# Welcome Adventurous Fellow

Interested in our work and you would like to get involved in building and implementing permissionless
technology ? Then this guide will help you get started.

You can explore the currently [Open Issues](https://github.com/cloqprotocol/go-cloq/issues) in-the works for the different releases.

You are also welcome to fork our repo and start creating PR’s after assigning yourself to an issue of interest. We are usually on [Discord](https://discord.gg) or [Gitter](https://gitter.im/cloqprotocol/go-cloq) join us to
meet other people and ask questions about project !

## Contribution Steps

**1. Set up go-cloq following the instructions in README.md.**

**2. Fork the go-cloq repo.**

Sign in to your Github account or create a new account if you do not have one already.
Next navigate your browser to <https://github.com/cloqprotocol/go-cloq/> and *fork* the project.

**3. Create a local clone to work on.**

```sh
 ~ mkdir -p $GOPATH/src/github.com/cloqporotocol
 ~ cd $GOPATH/src/github.com/cloqporotocol
 ~ git clone https://github.com/cloqporotocol/go-cloq.git
 ~ cd $GOPATH/src/github.com/cloqporotocol/go-cloq
```

**4. Link your local clone to the fork on your Github repo.**

```sh
~ git remote add mycopy https://github.com/<your_github_user_name>/go-cloq.git
```

**5. Link your local clone to the Cloq official repo so that you can easily fetch future changes to the official one.**

```sh
~ git remote add cloq https://github.com/cloqporotocol/cloq.git
~ git remote -v
```

**6. Find an issue to work on.**

Check out open issues at the [official repo](https://github.com/cloqporotocol/go-cloq/issues) and pick one.
Leave a comment to let the development team know that you would like to work on it.
Or examine the code for areas that can be improved and leave a comment to the development
team to ask if they would like you to work on it.

**7. Create a local branch with a name that clearly identifies what you will be working on.**

```sh
~ git checkout -b adding-cool-feature-branch
```

**8. Make improvements to the code.**

Each time you work on the code be sure that you are working on the branch that you have created as opposed to your local copy of the Cloq official repo. Keeping your changes segregated in this branch will make it easier to merge your changes into the repo later.

```sh
~ git checkout adding-cool-feature-branch
```

**9. Test your changes.**

Changes that only affect a single file can be tested with

```sh
~ go test <filename_test.go>
```

Changes that affect multiple files can be tested with ...

```sh
~ go test -v ./..
```

**10. Stage the file or files that you want to commit.**

```sh
~ git add --all
```

This command stages all of the files that you have changed. You can add individual files by specifying the file name or names and eliminating the “-- all”.

**11. Commit the file or files.**

```sh
~ git commit  -m “Message to explain what the commit covers”
```

You can use the –amend flag to include previous commits that have not yet been pushed to an upstream repo to the current commit.

**12. Fetch any changes that have occurred in the Cloq official repo since you started work.**

```sh
~ git fetch cloq
```

**13. Pull latest version of Cloq.**

```sh
~ git pull origin master
```

If there are conflicts between your edits and those made by others since you started work Git will ask you to resolve them. To find out which files have conflicts run ...

```sh
~ git status
```

Open those files one at a time and you
will see lines inserted by Git that identify the conflicts:

```sh
<<<<<< HEAD
Other developers’ version of the conflicting code
======
Your version of the conflicting code
'>>>>> Your Commit
```

The code from the Cloq repo is inserted between <<< and === while the change you have made is inserted between === and >>>>. Remove everything between <<<< and >>> and replace it with code that resolves the conflict. Repeat the process for all files listed by git status that have conflicts.

**14. Push your changes to your fork of the Cloq repo.**

Use git push to move your changes to your fork of the repo.

```sh
~ git push mycloqrepo adding-cool-feature-branch
```

**15. Check to be sure your fork of the Cloq repo contains your feature branch with the latest edits.**

Navigate to your fork of the repo on Github. On the upper left where the current branch is listed, change the branch to your adding-cool-feature-branch. Open the files that you have worked on and check to make sure they include your changes.

**16. Create a pull request.**

Navigate your browser to the [official repo](https://github.com/cloqprotocol/go-cloq) and click on the new pull request button. In the “base” box on the left, leave the default selection “base master”, the branch that you want your changes to be applied to. In the “compare” box on the right, select adding-cool-feature-branch, the branch containing the changes you want to apply. You will then be asked to answer a few questions about your pull request. After you complete the questionnaire, the pull request will appear in the list of pull requests [here](https://github.com/cloqprotocol/go-cloq/pulls).

**17. Respond to comments by Core Contributors.**

Core Contributors may ask questions and request that you make edits. If you set notifications at the top of the page to “not watching,” you will still be notified by email whenever someone comments on the page of a pull request you have created. If you are asked to modify your pull request, repeat steps 8 through 15, then leave a comment to notify the Core Contributors that the pull request is ready for further review.

**18. If the number of commits becomes excessive, you may be asked to squash your commits.**

 You can do this with an interactive rebase. Start by running the following command to determine the commit that is the base of your branch...

```sh
~ git mergse-base adding-cool-feature-branch cloq/master
```

**19. The previous command will return a commit-hash that you should use in the following command.**

```sh
~ git rebase -i commit-hash
```

Your text editor will open with a file that lists the commits in your branch with the word pick in front of each branch such as the following …

```sh
pick    hash    cleanup and some work
pick    hash    fix a bug
pick    hash    add a feature
```

Replace the word pick with the word “squash” for every line but the first so you end with ….

```sh
pick    hash    do some work
squash  hash    fix a bug
squash  hash    add a feature
```

Save and close the file, then a commit command will appear in the terminal that squashes the smaller commits into one. Check to be sure the commit message accurately reflects your changes and then hit enter to execute it.

**20. Update your pull request with the following command.**

```sh
~ git push mycloqrepo adding-cool-feature-branch -f
```

**21.  Finally, again leave a comment to the Core Contributors on the pull request to let them know that the pull request has been updated.**

## Part-Time Contributors

Anyone can become a part-time contributor and help out on fixing issues or implement current accepted [CLIPS].
A grant program will be open that allocates 50% of the current pre-mine reserved soly for developers and contributors
to the project.

As a part-time contributor an example work-flow would ressemble the following:

- Engaging in Discord and/or Gitter conversations, talk to people and ask questions about what you can work on.
- Opening up github issues to express interest in code to implement or a bug you found and would like to fix.
- Opening up PRs referencing an open issue in the PRs should include:
  - Detailed context of what would be required for a successful merge.
  - Consistent and extensive tests.
- The usual labels, milestones, and sub-projects see [GRANTS] for more details.
- Follow up on open PRs
  - Have an estimated timeframe to completion and let the core contributors know if a PR will take longer than expected

We do not expect all part-time contributors to be experts on all the latest reserach and technology, we are here to help.A good starting point to familiriaze yourself with our design and architecture would be the design document [DESIGN.md](https://github.com/cloqprotocol/docs/blob/master/DESIGN.md) and the current protocol specification [SPECS.md](https://github.com/cloqprotocol/docs/blob/master/PROTOCOL.md).
