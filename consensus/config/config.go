// Package config implements general configurations for consensus and a working
// node.
package config

import (
	"math/big"
	mathrand "math/rand"
	"time"

	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/protocol"
)

// ChainConfig represents blockchain consensus related configuration
// such as the chainid, the branchid, primitive versioning...
type ChainConfig struct {

	// Name defines a human readable identifier for the network.
	Name string
	// NetMagic defines the magic bytes used to identify network messages.
	NetMagic uint32
	// BranchID for verifying transactions
	BranchID uint32
	// GenesisHash defines the header hash of the genesis block
	GenesisHash [32]byte
	// StartingTarget is the maximum target we start working on
	StartingTarget *big.Int
	// ConsensusVersion is the version number for transactions and blocks
	ConsensusVersion uint32
}

// MinerConfig represents a list of addresses to receive miner rewards
type MinerConfig struct {
	Active    bool
	Addresses []string
}

// GetAddress returns a random address
func (mC *MinerConfig) GetAddress() string {
	mathrand.Seed(time.Now().Unix())
	idx := mathrand.Intn(len(mC.Addresses))

	return mC.Addresses[idx]

}

// NodeConfig is the config of the node
type NodeConfig struct {
	RPCAddress         string
	DataDirectory      string
	NetworkConfig      ChainConfig
	Resync             bool
	ListeningAddress   string
	BootstrapPeers     []string
	MinPeerCountToWait int
	HeartBeatInterval  time.Duration
	TimeOutInterval    time.Duration
	MaxPeers           int
	Pilot              bool
	Miner              bool
}

// NewConfig creates a default Config
// TODO : read config from YAML file
func NewConfig(chainConfig ChainConfig, pilot bool, miner bool, bootstrapPeers []string) NodeConfig {

	var bootstrap = make([]string, 0)
	if len(bootstrapPeers) != 0 {
		for _, p := range bootstrapPeers {
			bootstrap = append(bootstrap, p)
		}
	}

	return NodeConfig{
		RPCAddress:         "127.0.0.1`",
		DataDirectory:      "data",
		NetworkConfig:      chainConfig,
		Resync:             false,
		ListeningAddress:   "127.0.0.1",
		BootstrapPeers:     bootstrap,
		MinPeerCountToWait: 1,
		MaxPeers:           16,
		Pilot:              pilot,
		Miner:              miner,
	}
}

// ChainParameters describes parameters specific to a chain.
type ChainParameters interface {
	NetworkID() string
	NetworkMagic() uint32
	Version() uint32
	GetBranchID() uint32
	GenesisID() []byte
	GetPoWLimit() crypto.Hash
}

// WhirlpoolTestnet generates a WhirlpoolTestnet configuration
func WhirlpoolTestnet(genesisHash crypto.Hash) ChainConfig {

	name := "whirlpooltestv0"
	netmagic := protocol.TestnetMagic
	branchid := protocol.WhirpoolBranchID
	// decode genesisBlock := decode from disk ? or hardcode ?
	startingTarget := new(big.Int).Set(protocol.RegressionPoWLimit)

	return ChainConfig{
		Name:             name,
		NetMagic:         netmagic,
		BranchID:         branchid,
		GenesisHash:      genesisHash,
		StartingTarget:   startingTarget,
		ConsensusVersion: protocol.WhirlpoolConsensusVer,
	}
}

// WhirlpoolRegnet generates a WhirlpoolRegnet configuration
func WhirlpoolRegnet(genesisHash crypto.Hash) ChainConfig {

	name := "whirlpoolregv0"
	netmagic := protocol.TestnetMagic
	branchid := protocol.WhirpoolBranchID
	// decode genesisBlock := decode from disk ? or hardcode ?
	startingTarget := protocol.RegressionPoWLimit
	consensusVer := uint32(protocol.WhirlpoolConsensusVer)

	return ChainConfig{
		Name:             name,
		NetMagic:         netmagic,
		BranchID:         branchid,
		GenesisHash:      genesisHash,
		StartingTarget:   startingTarget,
		ConsensusVersion: consensusVer,
	}
}

// NetworkID is the parameter used to determine the bech32 hrp for address decoding.
func (chaincfg ChainConfig) NetworkID() string {
	return chaincfg.Name
}

// GetBranchID returns the current branchid  used to verify transactions
func (chaincfg ChainConfig) GetBranchID() uint32 {
	return chaincfg.BranchID
}

// GetPoWLimit is the maximum target to verify proof of work
func (chaincfg ChainConfig) GetPoWLimit() crypto.Hash {
	if chaincfg.StartingTarget == nil {
		return crypto.SetBig(protocol.WhirlpoolPoWLimit)
	}
	return crypto.SetBig(chaincfg.StartingTarget)
}

// GenesisID is the headerhash of the genesis block for this chain
func (chaincfg ChainConfig) GenesisID() []byte {
	return chaincfg.GenesisHash[:]
}

// NetworkMagic returns the network magic bytes
func (chaincfg ChainConfig) NetworkMagic() uint32 {
	return chaincfg.NetMagic
}

// Version returns the current consensus version
func (chaincfg ChainConfig) Version() uint32 {
	return chaincfg.ConsensusVersion
}
