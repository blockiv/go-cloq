# CasperFFG

```go casperffg``` implements the [Casper FFG](https://arxiv.org/abs/1710.09437)
overlay for a Proof-Of-Work block proposition algorithm.
