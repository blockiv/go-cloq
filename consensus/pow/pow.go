// Package pow provides a Proof Of Work based engine for consensus.
// Leader election is a lottery based on compute power.
package pow

import (
	"math/big"

	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/protocol"
)

var (
	// bigOne is 1 represented as a big.Int.  It is defined here to avoid
	// the overhead of creating it multiple times.
	one = big.NewInt(1)
)

// CheckWork checks that a proof of work is valid
func CheckWork(hash crypto.Hash, target *big.Int) bool {
	return hash.Big().Cmp(target) <= 0
}

// AmountOfWork returns the amount of work required to solve for a target.
func AmountOfWork(target *big.Int) *big.Int {

	blkWork := new(big.Int).SetInt64(0)
	targetCopy := new(big.Int).SetBytes(target.Bytes())
	// block work formula
	// (credits D.Hopwood : https://bitcoin.stackexchange.com/questions/5540/what-does-the-term-longest-chain-mean#comment59423_5542)
	// work(target) = (2**256 // (target + 1))
	if targetCopy.Cmp(blkWork) <= 0 {
		return blkWork
	}
	targetCopy = new(big.Int).Add(targetCopy, one)
	blkWork.Div(protocol.MaxAmountOfWork, targetCopy)

	return blkWork
}

// Difficulty computes a target difficulty.
func Difficulty(target *big.Int) *big.Int {

	targetCopy := new(big.Int).Set(target)

	if targetCopy.Cmp(protocol.WhirlpoolPoWLimit) >= 0 {
		return protocol.StartDiff
	}
	diff := new(big.Int).Div(protocol.WhirlpoolPoWLimit, targetCopy)
	return diff

}

// CumulativeChainWork computes the total chain work up to this target.
func CumulativeChainWork(target, chainWork *big.Int) (cumChainWork *big.Int) {
	blockWork := AmountOfWork(target)
	return new(big.Int).Add(chainWork, blockWork)
}
