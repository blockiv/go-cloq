package pow

import (
	"errors"
	"math/big"

	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/types"
)

func SolveBlock(blk *types.Block) (*types.Block, error) {

	h, _ := blk.Hash()
	blk.ChainWork = crypto.SetBig(new(big.Int).Add(blk.ChainWork.Big(), AmountOfWork(blk.Target.Big())))

	for !CheckWork(h, blk.Target.Big()) {
		blk.Nonce++

		h, _ = blk.Hash()
	}
	if !CheckWork(h, blk.Target.Big()) {
		return blk, errors.New("failed to do enough work")
	}

	return blk, nil
}
