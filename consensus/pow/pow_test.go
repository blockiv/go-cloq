package pow

import (
	"math/big"
	"testing"

	"gitlab.com/cloq/go-cloq/protocol"
)

func TestAmountOfWork(t *testing.T) {

	lowTarget, _ := new(big.Int).SetString("0x00ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff", 0)
	avgTarget := protocol.RegressionPoWLimit
	highTarget, _ := new(big.Int).SetString("0x00000000000000000015a35c0000000000000000000000000000000000000000", 0)

	t.Log("Amount of work (", lowTarget, ") : ", AmountOfWork(lowTarget))
	t.Log("Amount of work (", avgTarget, ") : ", AmountOfWork(avgTarget))
	t.Log("Amount of work (", highTarget, ") : ", AmountOfWork(highTarget))

	t.Log("Difficutlty ", lowTarget, ") : ", Difficulty(lowTarget))
	t.Log("Difficulty ", avgTarget, ") : ", Difficulty(avgTarget))
	t.Log("Difficulty ", highTarget, ") : ", Difficulty(highTarget))
}
