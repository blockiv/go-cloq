package wire

import (
	"bytes"
)

var (
	// MaxHeadersPerMessage specifies a soft limit on how many headers we send
	// at once 300 bytes * 2000
	MaxHeadersPerMessage uint32 = 2000
)

// HeadersMessage is a response to getheaders it is a list of rlp
// encoded blockheaders.
type HeadersMessage struct {
	BlockHeaders [][]byte `cbor:"blockHeaders"`
}

// Marshal implements the serialize interface.
func (hdrsMsg HeadersMessage) Marshal() []byte {
	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, hdrsMsg, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalHeadersMessage implements the deserialize interface
func UnmarshalHeadersMessage(buf []byte) (HeadersMessage, error) {
	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return NewHeadersMessage(), err
	}
	headersMsg := msg.(HeadersMessage)
	return headersMsg, nil
}

// NewHeadersMessage creates an instance of HeadersMessage
func NewHeadersMessage() HeadersMessage {
	return HeadersMessage{}
}

// Topic returns message topic
func (hdrsMsg HeadersMessage) Topic() uint32 {
	return Headers
}

// Checksum computes headers message checksum
func (hdrsMsg HeadersMessage) Checksum() []byte {
	return checksum(hdrsMsg)
}
