package wire

import (
	"bytes"
)

// InvType represents a data type for NodeData messages
type InvType uint8

const (
	_ InvType = iota
	// Contract represents a contract
	Contract
)

// NodeDataMessage represents a list of serialized data items
type NodeDataMessage struct {
	Type InvType  `cbor:"invType"`
	Data [][]byte `cbor:"data"`
}

// NewNodeDataMessage creates a new instance of NodeDataMessage
func NewNodeDataMessage() NodeDataMessage {
	return NodeDataMessage{}
}

// Marshal implements the serialize interface
func (nodeData NodeDataMessage) Marshal() []byte {

	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, nodeData, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalNodeDataMessage implements the deserialize interface
func UnmarshalNodeDataMessage(buf []byte) (NodeDataMessage, error) {

	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return NewNodeDataMessage(), err
	}
	nodeDataMsg := msg.(NodeDataMessage)
	return nodeDataMsg, nil
}

// Topic returns NodeDataMessage response topic
func (nodeData NodeDataMessage) Topic() uint32 {
	return NodeData
}

// Checksum computes a bundle's checksum
func (nodeData NodeDataMessage) Checksum() []byte {
	return checksum(nodeData)
}
