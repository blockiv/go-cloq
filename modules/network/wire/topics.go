package wire

const (
	// Status is the topic of the initial peer handshake payload it
	// advertises our node version and protocol.
	Status uint32 = 0x00
	//StatusOK is an acknowledgment message it confirms that two nodes
	// can talk to each other (they have the same protocol version).
	StatusOK = 0x01
	// GetPeers is an empty message to request few peers.
	GetPeers = 0x02
	// Peers is the topic used to broadcast peer addresses .
	Peers = 0x03
	// GetData represents a response to an inventory message.
	// it's used to request blocks, their headers mainly and transactions
	// in the mempool.
	GetData = 0x04
	// NodeData represents the response containing node data.
	NodeData = 0x06
	// Unknown represents a response to GetData it's a filtered inventory
	// message of data that we don't have.
	Unknown = 0xff

	// GetHeaders represent a request for a list of header hashes given a
	// starting and ending hash. (if the ending hash is all zeroes then count up to 500.)
	GetHeaders = 0x07
	// Headers represents a list of block headers.
	Headers = 0x08
	// GetBlocks represents a request for a list of blocks given a starting
	// and ending hash (if the ending hash is all zeroes then count up to 500).
	GetBlocks = 0x09
	// Blocks represents a list of blocks in response to GetBlocks .
	Blocks = 0x10

	// NewTransaction represents a new tx (broadcasted or in response to getdata).
	NewTransaction = 0x11
	// NewBlock represents a new block (mined or received)
	NewBlock = 0x12
	// Reject is sent when we reject a request or transaction
	Reject = 0x13

	// Ping is sent on a timer basis to refresh peer state
	Ping = 0x9a
	// Pong is a response to a ping message
	Pong = 0xa9
)

// GetTopic returns a string readable representation of the message topic
func GetTopic(x uint32) string {

	switch x {
	case Status:
		return "status"
	case StatusOK:
		return "statusOK"
	case Peers:
		return "peers"
	case GetPeers:
		return "getPeers"
	case GetData:
		return "getData"
	case GetHeaders:
		return "getHeaders"
	case Headers:
		return "headers"
	case GetBlocks:
		return "getBlocks"
	case Blocks:
		return "blocks"
	case NewTransaction:
		return "newTx"
	case NewBlock:
		return "newBlock"
	case Reject:
		return "reject"
	case Ping:
		return "ping"
	case Pong:
		return "pong"
	default:
		return "unknown"
	}
}
