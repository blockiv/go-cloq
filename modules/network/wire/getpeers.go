package wire

import (
	"bytes"
)

// GetPeersMessage is a request submitted to get a fresh list of peers to connect to
// once the status handshake is complete getpeers is the next step to expand
// our network.
// GetPeers is sent as part of the PEX protocol used to discover new peers.
type GetPeersMessage struct {
	PeerCount uint16
}

// Marshal implements the serialize interface
func (getPeersMsg GetPeersMessage) Marshal() []byte {
	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, getPeersMsg, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalGetPeersMessage implements the deserialize interface
func UnmarshalGetPeersMessage(buf []byte) (GetPeersMessage, error) {
	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return NewGetPeersMessage(), err
	}
	getPeersMsg := msg.(GetPeersMessage)
	return getPeersMsg, nil
}

// NewGetPeersMessage creates a new instance of GetPeersMessage it's an empty message.
func NewGetPeersMessage() GetPeersMessage {
	return GetPeersMessage{}
}

// Topic returns getpeers topic 0x03 the payload is empty
func (getPeersMsg GetPeersMessage) Topic() uint32 {
	return GetPeers
}

// Checksum returns an empty checksum
func (getPeersMsg GetPeersMessage) Checksum() []byte {
	return checksum(getPeersMsg)
}

// PeersMessage represents a response to getpeers it bundles a list of peer
// addresses and proposes them to the requester.
type PeersMessage struct {
	Peers []NodeAddress
}

// NewPeersMessage creates an empty peers message
func NewPeersMessage() PeersMessage {
	return PeersMessage{}
}

// Marshal implements the serialize interface
func (peersMsg PeersMessage) Marshal() []byte {
	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, peersMsg, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalPeersMessage implements the deserialize interface
func UnmarshalPeersMessage(buf []byte) (PeersMessage, error) {
	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return NewPeersMessage(), err
	}
	peersMsg := msg.(PeersMessage)
	return peersMsg, nil
}

// NodeAddress represents all the information we need to make a successful handshake
type NodeAddress struct {
	PeerInfo []byte
}

// Topic returns peers topic
func (peersMsg PeersMessage) Topic() uint32 {
	return Peers
}

// Checksum computes payload checksum
func (peersMsg PeersMessage) Checksum() []byte {
	return checksum(peersMsg)
}
