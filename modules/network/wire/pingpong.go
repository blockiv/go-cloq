package wire

import (
	"bytes"
	"fmt"
)

// PingMessage is sent to establish the first handshake and check peer status
type PingMessage struct {
	Ping string
}

// NewPingMessage creates a new instance of PingMessage
func NewPingMessage() PingMessage {
	return PingMessage{Ping: "ping"}
}

// Marshal implements the serialize interface
func (ping PingMessage) Marshal() []byte {

	return MarshalWireMessage(ping)
}

// UnmarshalPingMessage implements the deserialize interface
func UnmarshalPingMessage(buf []byte) (PingMessage, error) {

	msg, err := UnmarshalWireMessage(buf)
	pingMessage, ok := msg.(PingMessage)
	if !ok {
		return PingMessage{}, fmt.Errorf("wrong message received expected PingMessage got %T (+%v)", msg.Topic(), msg)
	}
	return pingMessage, err
}

// Topic returns PingMessage response topic
func (ping PingMessage) Topic() uint32 {
	return Ping
}

// Checksum implements checksum calculation over status messages.
func (ping PingMessage) Checksum() []byte {

	return checksum(ping)
}

// PongMessage is a response to a ping message
type PongMessage struct {
	Pong string
}

// NewPongMessage creates a new instance of PongMessage
func NewPongMessage() PongMessage {
	return PongMessage{Pong: "pong"}
}

// Marshal implements the serialize interface
func (pong PongMessage) Marshal() []byte {
	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, pong, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalPongMessage implements the deserialize interface
func UnmarshalPongMessage(buf []byte) (PongMessage, error) {
	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return NewPongMessage(), err
	}
	pongMessage := msg.(PongMessage)
	return pongMessage, nil
}

// Topic returns PongMessage response topic
func (pong PongMessage) Topic() uint32 {
	return Pong
}

// Checksum implements checksum calculation over status messages.
func (pong PongMessage) Checksum() []byte {

	return checksum(pong)
}
