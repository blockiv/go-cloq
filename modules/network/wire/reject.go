package wire

import (
	"bytes"
)

// RejectMessage is an empty message where we reject a getdata message.
type RejectMessage struct {
	rejected []byte
}

// NewRejectMessage creates an instance of ack message
func NewRejectMessage() RejectMessage {
	return RejectMessage{}
}

// Topic implements the Message interface
func (rejectMsg RejectMessage) Topic() uint32 {
	return Reject
}

// Checksum implements checksum calculation over status messages.
func (rejectMsg RejectMessage) Checksum() []byte {

	return checksum(rejectMsg)
}

// Marshal implements the serialize interface.
func (rejectMsg RejectMessage) Marshal() []byte {

	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, rejectMsg, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalRejectMessage implements the deserialize interface.
func UnmarshalRejectMessage(buf []byte) (RejectMessage, error) {

	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return NewRejectMessage(), err
	}
	rejectMsg := msg.(RejectMessage)
	return rejectMsg, nil
}
