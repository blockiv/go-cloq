package wire

import (
	"bytes"
	"crypto/rand"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

type wireTestCase struct {
	message Message
}

func TestWireMessage(t *testing.T) {

	t.Run("TestBlocksMessage", func(t *testing.T) {
		tests := []wireTestCase{
			{
				message: &BlocksMessage{
					BlockBodies: rand2DBytes(40),
				},
			}, {
				message: &BlocksMessage{
					BlockBodies: rand2DBytes(127),
				},
			},
		}

		runWireTests(t, tests)
	})
	t.Run("TestGetBlocksMessage", func(t *testing.T) {
		tests := []wireTestCase{
			{
				message: &GetBlocksMessage{
					BlockHashes: rand2DBytes(32),
					Reverse:     true,
				},
			}, {
				message: &GetBlocksMessage{
					BlockHashes: rand2DBytes(256),
					Reverse:     false,
				},
			},
		}
		runWireTests(t, tests)
	})
	t.Run("TestGetDataMessage", func(t *testing.T) {
		tests := []wireTestCase{
			{
				message: &GetDataMessage{
					InvType: Contract,
					Hashes:  rand2DBytes(32),
				},
			}, {
				message: &GetDataMessage{
					InvType: Contract,
					Hashes:  rand2DBytes(0),
				},
			},
		}
		runWireTests(t, tests)
	})
	t.Run("TestGetHeadersMessage", func(t *testing.T) {
		tests := []wireTestCase{
			{
				message: &GetHeadersMessage{
					StartingPoint: randBytes(32),
					Reverse:       false,
					MaxHeaders:    MaxHeadersPerMessage,
				},
			}, {
				message: &GetHeadersMessage{
					StartingPoint: []uint8{},
					Reverse:       true,
					MaxHeaders:    MaxHeadersPerMessage,
				},
			},
		}
		runWireTests(t, tests)
	})
	t.Run("TestGetPeersMessage", func(t *testing.T) {
		tests := []wireTestCase{
			{
				message: &GetPeersMessage{
					PeerCount: 10,
				},
			},
		}
		runWireTests(t, tests)
	})
	t.Run("TestPeersMessage", func(t *testing.T) {
		tests := []wireTestCase{
			{
				message: &PeersMessage{
					Peers: []NodeAddress{{PeerInfo: randBytes(16)}, {PeerInfo: randBytes(16)}, {PeerInfo: randBytes(16)}},
				},
			}, {
				message: &PeersMessage{
					Peers: []NodeAddress{{}},
				},
			},
		}
		runWireTests(t, tests)
	})
	t.Run("TestHeadersMessage", func(t *testing.T) {
		tests := []wireTestCase{
			{
				message: &HeadersMessage{
					BlockHeaders: rand2DBytes(34),
				},
			}, {
				message: &HeadersMessage{
					BlockHeaders: rand2DBytes(1),
				},
			},
		}
		runWireTests(t, tests)
	})
	t.Run("TestNewBlockMessage", func(t *testing.T) {
		var newBlockMessage = &NewBlockMessage{}
		newBlockMessage.Header = BlockSummary{
			Difficulty: randBytes(16),
			Hash:       randBytes(32),
			TXCount:    1000,
		}
		newBlockMessage.Block = randBytes(128)
		tests := []wireTestCase{
			{
				message: &NewBlockMessage{
					Header: BlockSummary{
						Difficulty: randBytes(24),
						Hash:       randBytes(32),
						TXCount:    1220,
					},
					Block: randBytes(257),
				},
			}, {
				message: &NewBlockMessage{
					Header: BlockSummary{
						Difficulty: randBytes(30),
						Hash:       randBytes(32),
						TXCount:    12,
					},
					Block: randBytes(254),
				},
			},
		}
		runWireTests(t, tests)
	})
	t.Run("TestNewTransactionMessage", func(t *testing.T) {
		tests := []wireTestCase{
			{
				message: &TransactionMessage{
					Body: randBytes(1255),
				},
			}, {
				message: &TransactionMessage{
					Body: randBytes(120),
				},
			},
		}
		runWireTests(t, tests)
	})
	t.Run("NodeDataMessage", func(t *testing.T) {
		tests := []wireTestCase{
			{
				message: &NodeDataMessage{
					Type: Contract,
					Data: rand2DBytes(1454),
				},
			}, {
				message: &NodeDataMessage{
					Type: Contract,
					Data: rand2DBytes(123),
				},
			},
		}
		runWireTests(t, tests)
	})
	t.Run("TestPingPongMessage", func(t *testing.T) {
		tests := []wireTestCase{
			{
				message: &PingMessage{
					Ping: "ping",
				},
			}, {
				message: &PongMessage{
					Pong: "pong",
				},
			},
		}
		runWireTests(t, tests)
	})
	t.Run("TestRejectMessage", func(t *testing.T) {
		tests := []wireTestCase{
			{
				message: &RejectMessage{},
			},
		}
		runWireTests(t, tests)
	})
	t.Run("TestStatusMessage", func(t *testing.T) {
		tests := []wireTestCase{
			{
				message: &StatusMessage{
					BranchID:        0xdeadbabe,
					ProtocolVersion: 0xcafebeef,
					GenesisHash:     randBytes(32),
					ChainWork:       randBytes(23),
					TipHash:         randBytes(32),
					TipHeight:       348434,
					TipRoot:         randBytes(32),
				},
			},
		}
		runWireTests(t, tests)
	})
	t.Run("TestStatusOKMessage", func(t *testing.T) {
		tests := []wireTestCase{
			{
				message: &StatusOKMessage{
					ACK: []byte("ACK"),
				},
			},
			{
				message: &StatusOKMessage{[]byte{}},
			}, {
				message: &StatusOKMessage{
					ACK: []byte("NACK"),
				},
			},
		}
		runWireTests(t, tests)
	})
}

func rand2DBytes(n int) [][]byte {

	var buf [][]byte

	for i := 0; i < 32; i++ {
		buf = append(buf, randBytes(n))
	}

	return buf
}

func randBytes(n int) []byte {
	var b = make([]byte, n)
	_, err := rand.Read(b[:])
	// failure of this is caused by the underlying operating system.
	if err != nil {
		panic(err)
	}
	return b
}

var errInconsistentEnc = errors.New("inconsistent encoding")

func assertConsistentEncoding(msg Message) error {

	buf := MarshalWireMessage(msg)

	msgOut, err := UnmarshalWireMessage(buf)
	if err != nil {
		return errInconsistentEnc
	}

	if msg.Topic() != msgOut.Topic() {
		return errInconsistentEnc
	}
	if !bytes.Equal(checksum(msg), msgOut.Checksum()) {
		return errInconsistentEnc
	}

	return nil

}
func runWireTests(t *testing.T, tests []wireTestCase) error {

	for i, tt := range tests {
		serializedMessage := tt.message.Marshal()
		if serializedMessage == nil {
			t.Fatalf("failed to serialize message [%d]", i)
		}
		deserMessage, err := UnmarshalWireMessage(serializedMessage)
		if err != nil {
			t.Fatalf("failed to deserialize message[%d] with error %s", i, err)
		}
		if assertConsistentEncoding(tt.message) != nil {
			t.Fatalf("failed to assert consistent encoding for message[%d]", i)
		}
		if !assert.Equal(t, tt.message, deserMessage) {
			t.Errorf("failed to validate message[%d] contents", i)
		}
	}
	return nil
}
