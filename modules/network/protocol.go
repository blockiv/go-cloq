package network

import (
	"fmt"

	"github.com/perlin-network/noise"
	"gitlab.com/cloq/go-cloq/modules/network/wire"
)

// ProtocolHandler is a set of callback used to register kademlia and gossip
// event callbacks.
type ProtocolHandler struct {
	OnPeerConnected  func(cli *noise.Client)
	OnPeerEvicted    func(id noise.ID)
	OnPeerAdmitted   func(id noise.ID)
	OnPeerActivity   func(id noise.ID)
	OnGossipReceived func(sender noise.ID, data []byte) error
}

func NewProtocolHandler(OnPeerConn func(cli *noise.Client), OnPeerEvicted, OnPeerAdmitted, OnPeerActivity func(id noise.ID), OnGossipReceived func(sender noise.ID, data []byte) error) ProtocolHandler {
	return ProtocolHandler{
		OnPeerConnected:  OnPeerConn,
		OnPeerEvicted:    OnPeerEvicted,
		OnPeerAdmitted:   OnPeerAdmitted,
		OnPeerActivity:   OnPeerActivity,
		OnGossipReceived: OnGossipReceived,
	}
}

// Events represents callbacks that run when new events happen.
// Each Event has a specific callback that execute, such as
// broadcasting gossip messages, registering peers into storage...

// handleGossipMessage is called each time a new gossip message is received
// Gossip implements the protocol interface, it's method Handle automatically
// pushes received messages to other connected peers the underlying binded Noise
// node is connected to.
func handleGossipMessage(sender noise.ID, data []byte) error {
	message, err := wire.UnmarshalWireMessage(data)
	if err != nil {
		return err

	}
	fmt.Printf("Received New Gossip Message %s From Peer %s :", wire.GetTopic(message.Topic()), ComputePeerID(sender))
	return nil
}

// handleAdmittedPeer is called each time a new peer is added to the kademlia routing
// table.
func (ps *PeerStore) handleAdmittedPeer(id noise.ID) {
	ps.mu.Lock()
	defer ps.mu.Unlock()

	ps.store[ComputePeerID(id)] = &Peer{ID: id.String()}
}

// handleEvictedPeer is called each time a peer is evicted
func (ps *PeerStore) handleEvictedPeer(id noise.ID) {
	ps.RemovePeer(id)
}

// handleConnectedPeer is called each time a new peer is connected (inbound)
func (ps *PeerStore) handleConnectedPeer(cli *noise.Client) {
	newPeer := NewPeer(cli)
	ps.AddNewPeer(newPeer)
}
