package chain

import (
	"fmt"
	"math/big"
	"sort"
	"time"

	"gitlab.com/cloq/go-cloq/pkg/bytesutil"

	"gitlab.com/cloq/go-cloq/pkg/safemath"

	"gitlab.com/cloq/go-cloq/consensus/config"
	"gitlab.com/cloq/go-cloq/consensus/pow"
	"gitlab.com/cloq/go-cloq/modules/storage/state"

	"gitlab.com/cloq/go-cloq/core/types"

	hashset "github.com/deckarep/golang-set"
	"github.com/pkg/errors"
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/protocol"
)

var (
	errOlderBlockTime    = errors.New("timestamp older than parent")
	errFutureBlockTime   = errors.New("timestamp too far in the future")
	errMedianBlockTime   = errors.New("timestamp older than median of past blocks")
	errTooManyUncles     = errors.New("too many uncles")
	errDuplicateUncle    = errors.New("duplicate uncle")
	errDuplicateCoinbase = errors.New("duplicate coinbase")
	errUncleIsAncestor   = errors.New("uncle is ancestor")
	errDanglingUncle     = errors.New("uncle's parent is not ancestor")
	errUncleIsMain       = errors.New("uncle is on main branch")
	errInvalidDifficulty = errors.New("invalid difficulty for expected work")
	errInvalidHeight     = errors.New("invalid height")
	errInvalidPoW        = errors.New("invalid proof-of-work")
	errInvalidChainWork  = errors.New("invalid chain work")
	errInvalidBlockVer   = errors.New("invalid block consensus version")
	errInvalidTXOrder    = errors.New("block transactions don't respect canonical transaction ordering")
)

// VerifyHeaderWithoutContext verifies that a header has valid work, valid timestamps.
func VerifyHeaderWithoutContext(header *types.Header, tipWork *big.Int, config config.ChainParameters, medianTimestamp uint64) (bool, error) {

	headerHash, err := header.Hash()
	if err != nil {
		return false, err
	}

	if uint64(config.Version()) != header.GetConsensusVersion() {
		return false, errInvalidBlockVer
	}
	if header.GetTimestamp() > uint64(time.Now().Add(protocol.AllowedFutureTimestamp).Unix()) {
		return false, errFutureBlockTime
	}
	if header.GetChainWork().Big().Cmp(tipWork) < 0 {
		return false, errInvalidChainWork
	}
	if pow.Difficulty(header.GetTarget().Big()).Cmp(pow.Difficulty(header.GetTarget().Big())) != 0 {
		return false, errInvalidDifficulty
	}
	if !pow.CheckWork(headerHash, header.GetTarget().Big()) {
		return false, errInvalidPoW
	}
	return true, nil
}

// VerifyBlockWithoutContext verifies that a block has a valid transaction list
func VerifyBlockWithoutContext(blk *types.Block) (bool, error) {
	// must have at least one tx
	if len(blk.Transactions()) == 0 {
		return false, errInvalidTXOrder
	}
	// coinbase is the first tx in block
	if !IsCoinbase(blk.Transactions()[0]) {
		return false, errInvalidTXOrder
	}
	ok, err := VerifyMerkleRoots(blk)
	if !ok || err != nil {
		return false, err
	}
	txids, err := transactionIDs(blk.Transactions())
	if err != nil {
		return false, err
	}
	if !sort.IsSorted(bytesutil.Sortable(txids)) {
		return false, errInvalidTXOrder
	}

	return true, nil

}

// VerifyHeader verifies that a header is valid according to the consensus rule.
func VerifyHeader(header *types.Header, parent *types.Header, blockchain BlockchainView, isUncle bool) (bool, error) {

	currentTip := blockchain.GetChainTip()

	if header.GetTimestamp() < parent.GetTimestamp() || header.GetTimestamp() < currentTip.MedianTimestamp {
		return false, errMedianBlockTime
	}
	headerHash, err := header.Hash()
	if err != nil {
		return false, err
	}

	if header.GetHeight() != parent.GetHeight()+1 {
		return false, errInvalidHeight
	}
	if header.GetChainWork().Big().Cmp(currentTip.TotalChainWork.Big()) < 0 {
		return false, errInvalidChainWork
	}

	target, err := blockchain.ComputeTargetThreshold(header.GetHeight())
	if err != nil {
		return false, err
	}
	if pow.Difficulty(target).Cmp(pow.Difficulty(header.GetTarget().Big())) != 0 {
		return false, errInvalidDifficulty
	}
	if !pow.CheckWork(headerHash, target) {
		return false, errInvalidPoW
	}

	if isUncle {
		if header.GetTimestamp() > uint64(time.Now().Add(protocol.AllowedFutureTimestamp).Unix()) {
			return false, errFutureBlockTime
		}
	}
	unclesWork := new(big.Int).SetInt64(0)
	if len(header.GetUncles()) > 0 {
		// Verify that there are at most 2 uncles included in this block
		if len(header.GetUncles()) > protocol.MaxUncles {
			return false, errTooManyUncles
		}
		ok, sumUnclesWork, err := verifyUncles(header, blockchain)
		if !ok || err != nil {
			return ok, err
		}
		unclesWork.Set(sumUnclesWork)
	}

	expectedChainWork := pow.CumulativeChainWork(target, parent.GetChainWork().Big())
	expectedChainWork = new(big.Int).Add(expectedChainWork, unclesWork)

	if expectedChainWork.Cmp(header.GetChainWork().Big()) != 0 {
		return false, errInvalidChainWork
	}
	return true, nil

}

// VerifyBlockTransactions : processes and validates all the transactions in a given block
func VerifyBlockTransactions(blk *types.Block, currentTip uint64, stateView state.StateView, chainConfig config.ChainConfig) (bool, error) {

	var blockFees uint64 = 0
	ok, err := VerifyMerkleRoots(blk)
	if !ok || err != nil {
		return false, err
	}
	if len(blk.Txs) > 1 {
		blockTxs := blk.Body.Txs[1:]
		txids, err := transactionIDs(blockTxs)
		if err != nil {
			return false, err
		}
		if !sort.IsSorted(bytesutil.Sortable(txids)) {
			return false, errInvalidTXOrder
		}
		for idx, transaction := range blockTxs {
			if IsCoinbase(transaction) {
				return false, errors.Wrap(err, fmt.Sprintf("invalid transaction at index : %d", idx))
			}
			ok, err := VerifyTransaction(transaction, currentTip, stateView, chainConfig)
			if !ok || err != nil {
				return false, errors.Wrap(err, fmt.Sprintf("invalid transaction at index : %d", idx))
			}
			blockFees, err = safemath.AddUint64(blockFees, transaction.GetFee())
			if err != nil {
				return false, errors.Wrap(err, fmt.Sprintf("invalid transaction at index : %d", idx))
			}
		}
	}
	coinbaseTx := blk.Body.Txs[0]
	coinbaseOK, err := VerifyCoinbase(coinbaseTx, blockFees, blk.GetHeight())
	if !coinbaseOK || err != nil {
		return false, errors.Wrap(err, "invalid coinbase transaction")
	}

	return true, nil

}

// verifyUncles checks that any block uncles are valid headers
func verifyUncles(header *types.Header, blockchain BlockchainView) (bool, *big.Int, error) {
	uncles, ancestors := hashset.NewSet(), make(map[crypto.Hash]*types.Header)

	height, parent := header.GetHeight()-1, header.GetParent()
	headerHash, err := header.Hash()
	unclesWork := new(big.Int).SetInt64(0)
	zeroWork := new(big.Int).SetInt64(0)

	if err != nil {
		return false, zeroWork, err
	}
	for i := 0; i < 7; i++ {
		ancestor, err := blockchain.GetHeader(parent, height)
		if ancestor == nil || err != nil {
			return false, zeroWork, errors.Wrap(err, "unavailable ancestor")
		}
		ancestorHash, err := ancestor.Hash()
		if err != nil {
			return false, zeroWork, err
		}
		ancestors[ancestorHash] = ancestor
		for _, uncleHash := range ancestor.GetUncles() {
			uncles.Add(uncleHash)
		}
		parent, height = ancestor.GetParent(), height-1
	}

	ancestors[headerHash] = header
	uncles.Add(headerHash)

	// Verify each of the uncles that it's recent, but not an ancestor
	for _, uncleHash := range header.GetUncles() {
		// Make sure every uncle is counted only once
		if uncles.Contains(uncleHash) {
			return false, zeroWork, errDuplicateUncle
		}
		uncles.Add(uncleHash)

		// Make sure the uncle has a valid ancestry
		if ancestors[uncleHash] != nil {
			return false, zeroWork, errUncleIsAncestor
		}
		uncleBranch := blockchain.GetBranch(uncleHash)
		if uncleBranch == MAIN {
			return false, zeroWork, errUncleIsMain
		}
		uncleHeader, err := blockchain.GetHeaderByHash(uncleHash)
		if err != nil {
			return false, zeroWork, err
		}
		if ancestors[uncleHeader.GetParent()] == nil || uncleHeader.GetParent() == header.GetParent() {
			return false, zeroWork, errDanglingUncle
		}
		if ok, err := VerifyHeader(uncleHeader, ancestors[uncleHeader.GetParent()], blockchain, true); !ok || err != nil {
			return false, zeroWork, err
		}
		uncleWork := pow.AmountOfWork(uncleHeader.GetTarget().Big())
		unclesWork = unclesWork.Add(unclesWork, uncleWork)
	}

	return true, unclesWork, err
}

// transactionIDs returns the list of transactionIDs in a block
// coinbase not included in this list.
func transactionIDs(transactions []*types.Transaction) ([][]byte, error) {

	var txids = make([][]byte, 0, len(transactions))

	for _, tx := range transactions {
		txid, err := tx.TXID()
		if err != nil {
			return nil, err
		}
		txids = append(txids, txid.Bytes())
	}
	return txids, nil
}
