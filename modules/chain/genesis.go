package chain

import (
	"math/big"

	"gitlab.com/cloq/go-cloq/consensus/pow"
	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/modules/storage/state"
	"gitlab.com/cloq/go-cloq/protocol"

	"gitlab.com/cloq/go-cloq/pkg/bytesutil"

	"gitlab.com/cloq/go-cloq/core/crypto"
)

// CreateGenesisBlock creates a new genesisBlock
func CreateGenesisBlock(genesisRecord []byte, genesisTimestamp int64, genesisOutputAddress string, value uint64, target *big.Int) (*types.Block, crypto.Hash, error) {

	genesisState := state.NewCachedSnapshot(nil)

	genesisInput := make([]types.Input, 1)
	genesisOutput := make([]types.Output, 1)

	genesisInput[0].Origin = crypto.EmptyHash
	genesisInput[0].Validator = crypto.SHA3(genesisRecord)

	genesisOutput[0].Address = genesisOutputAddress
	genesisOutput[0].Value = value
	genesisOutput[0].Locktime = protocol.CoinbaseMaturity

	genesisCommitment := types.NewCommitment(genesisInput, genesisOutput)

	genesisTX := types.NewTransaction(protocol.WhirlpoolConsensusVer, protocol.WhirpoolBranchID, 0, 0, genesisCommitment, types.NewWitness())

	genesisTXID, genesisWTXID, genesisTransition, err := ComputeTransactionsRoots([]*types.Transaction{genesisTX})
	if err != nil {
		return nil, crypto.EmptyHash, err
	}
	genesisHeader := &types.Header{
		Version:        1,
		Height:         0,
		Time:           uint64(genesisTimestamp),
		Nonce:          0,
		Parent:         crypto.Hash{},
		Uncles:         nil,
		Root:           crypto.Hash{},
		CommitmentRoot: bytesutil.ToBytes32(genesisTXID),
		WitnessRoot:    bytesutil.ToBytes32(genesisWTXID),
		TransitionRoot: bytesutil.ToBytes32(genesisTransition),
		Target:         crypto.SetBig(target),
		ChainWork:      crypto.SetBig(pow.AmountOfWork(protocol.WhirlpoolPoWLimit)),
	}
	genesisBlock := types.NewBlock(genesisHeader, []*types.Transaction{genesisTX}, nil)

	stateDelta, err := generateStateDelta(genesisBlock, genesisState)
	if err != nil {
		panic(err)
	}
	ok, root, err := applyStateDelta(genesisState, stateDelta)
	if !ok || err != nil {
		panic(err)
	}
	genesisBlock.Root = bytesutil.ToBytes32(root)

	genesisBlock, err = pow.SolveBlock(genesisBlock)
	if err != nil {
		panic(err)
	}
	genesisBlockHash, _ := genesisBlock.Hash()

	return genesisBlock, genesisBlockHash, nil
}
