package chain

import (
	"gitlab.com/cloq/go-cloq/core/types"

	lru "github.com/hashicorp/golang-lru"
	"github.com/pkg/errors"
	"gitlab.com/cloq/go-cloq/modules/storage/state"
)

var (
	errStateRootMismatch = errors.New("state root mismatch")
)

// transitionEntry are represented by a list of contractHashes used to query
// the trie, and their respective txos.
type transitionEntry struct {
	contractHashes  [][]byte // H(txid||index||output)
	openedContracts []*types.OpenContract
}

// transitionCache is where we cache resent applied transitions
type transitionCache struct {
	cache lru.Cache
}

func (tC *transitionCache) put(hash []byte, entry *transitionEntry) {
	tC.cache.Add(hash, entry)
}
func (tC *transitionCache) get(hash []byte) (*transitionEntry, bool) {
	val, ok := tC.cache.Get(hash)
	if !ok {
		return nil, false
	}
	return val.(*transitionEntry), true
}
func (tC *transitionCache) flush() {
	tC.cache.Purge()
}

// stateDelta is two lists of tuples (H(Contract),H(OpenContract)) that is generated
// from a block.
// S -> T -> S'
// The first transition list consists of tuples that will be removed.
// The second list consists of tuples that will be added.
type stateDelta struct {
	executed *transitionEntry
	created  *transitionEntry
}

// unpackCreatedContracts returns a sorted list of the new created opencontracts
func unpackCreatedContracts(blk *types.Block) ([]*types.OpenContract, error) {

	height := blk.Height

	createdContracts, err := blk.CreatedContracts()
	if err != nil {
		return nil, err
	}

	coinbaseContract, err := blk.CoinbaseContract()
	if err != nil {
		return nil, err
	}

	coinbaseOpenContract := types.NewOpenContract(coinbaseContract, height, true)

	openContractsCreated := make([]*types.OpenContract, 0, len(createdContracts))
	openContractsCreated = append(openContractsCreated, coinbaseOpenContract)

	for _, out := range createdContracts {
		newOC := types.NewOpenContract(out, height, false)
		openContractsCreated = append(openContractsCreated, newOC)
	}

	return openContractsCreated, nil
}

// ProcessBlockTransition will try to execute the block transition
// on the state trie.
func ProcessBlockTransition(blk *types.Block, state state.OpenContractSet, revertAfter bool) ([]byte, error) {

	preTransitionRoot := state.Root()

	delta, err := generateStateDelta(blk, state)
	if err != nil {
		return nil, err
	}
	ok, _, err := applyStateDelta(state, delta)
	if !ok || err != nil {
		return nil, err
	}

	if revertAfter {
		return state.Root(), state.Rollback(preTransitionRoot)
	}
	return state.Root(), nil
}

// generateStateDelta processes a block and returns a state transition
func generateStateDelta(blk *types.Block, state state.OpenContractSet) (*stateDelta, error) {

	var deltaSpent = new(transitionEntry)
	var deltaCreated = new(transitionEntry)
	var transition = new(stateDelta)

	executedInputsHash, err := blk.ExecutedContracts()
	if err != nil {
		return nil, err
	}
	executedOC, err := state.GetBatch(executedInputsHash)
	if err != nil {
		return nil, err
	}

	deltaSpent.contractHashes = executedInputsHash
	deltaSpent.openedContracts = executedOC

	createdContracts, err := unpackCreatedContracts(blk)
	if err != nil {
		return nil, err
	}

	deltaCreated.contractHashes, err = blk.CreatedContractsHashes()
	if err != nil {
		return nil, err
	}
	deltaCreated.openedContracts = createdContracts

	transition.created = deltaCreated
	transition.executed = deltaSpent

	return transition, nil
}

// applyStateDelta to state
func applyStateDelta(state state.OpenContractSet, transition *stateDelta) (bool, []byte, error) {

	if len(transition.executed.contractHashes) == 0 {
		// batch add created txos
		err := state.Put(transition.created.openedContracts)
		if err != nil {
			return false, nil, err
		}
		return true, state.Root(), nil
	}
	// batch sweep executed txos
	err := state.Delete(transition.executed.openedContracts)
	if err != nil {
		return false, nil, err
	}
	// batch add created txos
	err = state.Put(transition.created.openedContracts)
	if err != nil {
		return false, nil, err
	}

	return true, state.Root(), nil
}

// revertStateDelta applies a transition in reverse (removing created txos and adding executed ones)
func revertStateDelta(state state.OpenContractSet, transition *stateDelta) error {

	// batch sweep created txos
	err := state.Delete(transition.created.openedContracts)
	if err != nil {
		return err
	}

	// batch add executed txos
	err = state.Put(transition.executed.openedContracts)
	if err != nil {
		return err
	}

	return nil
}
