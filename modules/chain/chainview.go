package chain

import (
	"math/big"

	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/modules/storage/ledger"
)

// BlockchainView interface offers a read-only view of the current blockchain
type BlockchainView interface {
	GetBlockByHash(crypto.Hash) (*types.Block, error)
	GetHeaderByHash(crypto.Hash) (*types.Header, error)
	GetHeaderByHeight(uint64, ledger.BlockMarker) (*types.Header, error)
	GetChainTip() Tip
	GetBranch(crypto.Hash) ledger.BlockMarker
	GetPoWLimit() *big.Int
	ComputeMedianTimeSpan(height uint64) (int64, error)
	ComputeTargetThreshold(height uint64) (*big.Int, error)
	GetHeader(blkHash crypto.Hash, height uint64) (*types.Header, error)

	// range queries
	GetBlocksInRange(start, end crypto.Hash) ([]*types.Block, error)
	GetHeadersInRange(start, end crypto.Hash) ([]*types.Header, error)
}
