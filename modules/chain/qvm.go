package chain

import (
	"fmt"
	"io"

	"gitlab.com/cloq/go-cloq/core/types"

	"github.com/pkg/errors"
	"gitlab.com/cloq/go-cloq/core/qvm/vm"
	"gitlab.com/cloq/go-cloq/core/script"
	"gitlab.com/cloq/go-cloq/protocol"
)

var (
	// VMVersion flag is set for the current VM version
	VMVersion uint32 = 1
)

// Result wraps the VM execution model
type result struct {
	success   bool  // was our execution successful
	progIndex int   // which program errored
	err       error // what error did the program throw
}

// QVM implements a wrapper around VM for concurrent use.
// Each VM runs a script and has access to transaction data for introspection.
// Transaction data is the computed txid and a list of outputs.
type QVM struct {
	version  int
	cpuCount int
	engines  []*vm.VirtualMachine
}

// InitQVM initiate a qvm instance
func InitQVM(transaction *types.Transaction, prevHeights []uint64, currHeight uint64, maxMemory int) (*QVM, error) {

	txInputs, err := transaction.Inputs()
	if err != nil {
		return nil, err
	}

	inputCount := len(txInputs)
	if inputCount >= protocol.MaxPathwayThreshold {
		return nil, errors.New("input count larget than pathway threshold")
	}
	version := transaction.GetVersion()

	qvm := new(QVM)
	qvm.cpuCount = inputCount
	qvm.version = int(version)

	var vms = make([]*vm.VirtualMachine, 0, len(txInputs))

	txid, err := transaction.TXID()
	if err != nil {
		return nil, err
	}

	outputCount := len(transaction.Commitment.Outputs)

	outputHashes, err := transaction.OutputHashes()
	if err != nil {
		return nil, err
	}

	for idx, input := range txInputs {

		prog, err := script.NewScript(input.Validator)
		if err != nil {
			return nil, err
		}
		args := transaction.Witness.Redeemers[idx]

		testVM := vm.New(maxMemory, prog, args)
		executionCtx := vm.NewVMContext(VMVersion, version, txid, prevHeights[idx], currHeight, txid)
		executionCtx.LoadTransactionData(outputCount, outputHashes)
		err = testVM.LoadContext(executionCtx)
		if err != nil {
			return nil, err
		}

		vms = append(vms, testVM)

	}

	qvm.engines = vms

	return qvm, nil

}

// Run the virtual machine and report back results
func (qvm *QVM) Run(tracer io.Writer) (bool, error) {

	// This part need extensive care to avoid
	// any bad script to verify successfully
	var executionError error

	results := make(chan result, qvm.cpuCount)

	for idx, vmToRun := range qvm.engines {
		vmToRun := vmToRun
		go func(out chan result, index int, traceout io.Writer) {
			var res result
			res.progIndex = index
			res.success, res.err = vmToRun.Run(traceout)
			out <- res
		}(results, idx, tracer)
	}

	for res := range results {
		if !res.success || res.err != nil {
			executionError = errors.Wrap(res.err, fmt.Sprintf(" failed to eval validator at index : %d", res.progIndex))
			return false, executionError
		}
	}
	// if everything is good to go we return
	return true, executionError
}
