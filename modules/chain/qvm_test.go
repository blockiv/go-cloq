package chain

import (
	"testing"

	"gitlab.com/cloq/go-cloq/core/types"

	"gitlab.com/cloq/go-cloq/core/addr"
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/script"
	"gitlab.com/cloq/go-cloq/pkg/leb128"
	"golang.org/x/crypto/sha3"
)

var testScripts = []string{
	"sha3 0x92dad9443e4dd6d70a7f11872101ebff87e21798e4fbb26fa4bf590eb440e71b equal",
	"dup blake2 0xb966a57f3010512d1ebe8629864c71c909aeb4752e93ffa525f00976c93e33c0 equal jumpif:@end sha256 0x6cd8a81bea00783c3fcfca07eef8c80adc1fb1ba46dc611d5554983e6ae3a7ea equal @end true",
	"dup blake2 0xb966a57f3010512d1ebe8629864c71c909aeb4752e93ffa525f00976c93e33c0",
}

var testWitnesses = []types.Redeemer{
	[][]byte{[]byte("helloworld")},
	[][]byte{[]byte("hellobob")},
	[][]byte{[]byte("hi")},
}

func TestTxExecution(t *testing.T) {

	inputs := make([]types.Input, 0, 8)
	witnesses := make([]types.Redeemer, 0, 8)
	prevHeights := make([]uint64, 0, 8)
	for i := 0; i < 28; i++ {
		prevHeights = append(prevHeights, uint64((i+3)*7))
		b := leb128.FromInt64(int64(i))
		hash := sha3.Sum256(b)
		testScript := testScripts[i%len(testScripts)]
		testScriptB, _ := script.Compile(testScript)
		inputs = append(inputs, types.Input{Origin: hash, Validator: testScriptB})
		witnesses = append(witnesses, testWitnesses[i%2])
	}

	outputs := make([]types.Output, 1)
	hash := crypto.SHA3Hash([]byte("a very weird script"))
	address, _ := addr.EncodeHash(hash, "testnet")
	outputs[0] = types.Output{
		Address:  address,
		Value:    5600,
		Locktime: 100,
	}
	commitment := types.NewCommitment(inputs, outputs)
	witness := types.NewWitness()
	witness.Redeemers = witnesses

	transaction := types.NewTransaction(0, 0, 0, 0, commitment, witness)
	execution, err := InitQVM(transaction, prevHeights, 3545, 5000)
	if err != nil {
		t.Log("failed to init QVM with error : ", err)
	}
	success, err := execution.Run(nil)

	t.Logf("successful Execution : %v | error : %v", success, err)
}
