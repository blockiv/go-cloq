// Package chain implements a blockchain interface .
package chain

import (
	"bytes"
	"errors"
	"math/big"
	"sync"
	"time"

	hashset "github.com/deckarep/golang-set"
	lru "github.com/hashicorp/golang-lru"
	"gitlab.com/cloq/go-cloq/consensus/config"
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/proof"
	"gitlab.com/cloq/go-cloq/pkg/bytesutil"
	"gitlab.com/cloq/go-cloq/protocol"

	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/modules/storage"
	"gitlab.com/cloq/go-cloq/modules/storage/ledger"
	"gitlab.com/cloq/go-cloq/modules/storage/state"
)

// Interface assertions
var _ BlockchainView = (*Blockchain)(nil)

const (
	// UNKNOWN means the block status is not known.
	UNKNOWN ledger.BlockMarker = iota
	// MAIN means the block is currently on the main branch.
	MAIN
	// SIDE means the block is currently extending a different branch.
	SIDE
	// ORPHAN means the block is orphaned no block extends it.
	ORPHAN
	// INVALID means the block is invalid
	INVALID
)

var (
	// ErrMaxChainReorg is returned if a reorg trigger the maximum client limit
	ErrMaxChainReorg = errors.New("fork is longer than soft reorg limit")
	// ErrUnknownParent is returned if a parent is unknown to us
	ErrUnknownParent = errors.New("unknown parent for block")
	// ErrBlockNotFound is returned if we don't have a particular block
	ErrBlockNotFound = errors.New("block not found")
	// ErrHeaderNotFound is returned if we don't have a particular header
	ErrHeaderNotFound = errors.New("header not found")
)

const blocksCacheSize = 128

// Blockchain implements the chain provider interface, exposes a self-contained
// API to query blocks and process transactions according to the last view
// of the state (up to the current tip).
type Blockchain struct {
	mu     sync.RWMutex
	config config.ChainParameters

	currentTip   Tip
	blocksCache  *lru.Cache
	headersCache *lru.Cache

	blockIndex map[uint64]crypto.Hash      // Store an index of height -> blockhashe
	blockTree  map[crypto.Hash]crypto.Hash // Store an index of hash A -> hash B

	orphans hashset.Set // Stores the hash of orphan blocks
	side    hashset.Set // Stores the hash of side blocks

	ledger *ledger.LedgerDB // implementation of the block store interface
	state  *state.Snapshot  // implementation of the state interface
}

// New creates a new blockchain instance
func New(config config.ChainParameters, storage *storage.Storage) (*Blockchain, error) {

	blocksCache, err := lru.New(blocksCacheSize)
	if err != nil {
		return nil, err
	}

	headersCache, err := lru.New(blocksCacheSize)
	if err != nil {
		return nil, err
	}
	index := make(map[uint64]crypto.Hash, blocksCacheSize)

	tree := make(map[crypto.Hash]crypto.Hash, blocksCacheSize)

	orphans := hashset.NewSet()
	side := hashset.NewSet()

	return &Blockchain{
		blocksCache:  blocksCache,
		headersCache: headersCache,
		blockIndex:   index,
		blockTree:    tree,
		orphans:      orphans,
		config:       config,
		side:         side,
		ledger:       storage.LedgerDB,
		state:        storage.StateDB,
	}, nil
}

// GetBlockByHeight fetches all blocks at a certain height, otherwise the only commited block.
func (chain *Blockchain) GetBlockByHeight(height uint64, status ledger.BlockMarker) (*types.Block, error) {

	chain.mu.RLock()
	defer chain.mu.RUnlock()

	blkHash, ok := chain.blockIndex[height]

	if !ok {
		candidate, err := chain.ledger.GetBlockByHeight(height, status)
		if err != nil {
			return nil, err
		}
		return candidate, nil
	}
	candidate, ok := chain.blocksCache.Get(blkHash)
	if !ok {
		candidate, err := chain.ledger.GetBlockByHeight(height, status)
		if err != nil {
			return nil, err
		}
		return candidate, nil
	}
	return candidate.(*types.Block), nil
}

// GetHeaderByHeight fetches all headers at a certain height, otherwise the only commited header.
func (chain *Blockchain) GetHeaderByHeight(height uint64, status ledger.BlockMarker) (*types.Header, error) {

	chain.mu.RLock()
	defer chain.mu.RUnlock()

	blkHash, ok := chain.blockIndex[height]

	if !ok {
		candidate, err := chain.ledger.GetHeaderByHeight(height, status)
		if err != nil {
			return nil, err
		}
		return candidate, nil
	}
	candidate, ok := chain.headersCache.Get(blkHash)
	if !ok {
		candidate, err := chain.ledger.GetHeaderByHeight(height, status)
		if err != nil {
			return nil, err
		}
		return candidate, nil
	}
	return candidate.(*types.Header), nil
}

// GetBlockByHash fetches a block by it's hash
func (chain *Blockchain) GetBlockByHash(h crypto.Hash) (*types.Block, error) {

	chain.mu.RLock()
	defer chain.mu.RUnlock()

	blk, ok := chain.blocksCache.Get(h)
	if !ok {
		return chain.ledger.GetBlockByHash(h.Bytes())
	}
	return blk.(*types.Block), nil
}

// GetHeader returns a header with the same hash and height
func (chain *Blockchain) GetHeader(blkHash crypto.Hash, height uint64) (*types.Header, error) {
	return chain.GetHeaderByHash(blkHash)
}

// GetHeaderByHash fetches a header by it's hash
func (chain *Blockchain) GetHeaderByHash(h crypto.Hash) (*types.Header, error) {

	chain.mu.RLock()
	defer chain.mu.RUnlock()

	blk, ok := chain.headersCache.Get(h)
	if !ok {
		return chain.ledger.GetHeaderByHash(h.Bytes())
	}
	return blk.(*types.Header), nil
}

// GetChainTip returns the current most work tip.
func (chain *Blockchain) GetChainTip() Tip {
	chain.mu.RLock()
	defer chain.mu.RUnlock()
	return chain.currentTip
}

// updateTip updates the Blockchain blocksCache
func (chain *Blockchain) updateTip(hash crypto.Hash, height uint64, root crypto.Hash, work *big.Int) error {

	medianTimestamp, err := chain.ComputeMedianTimeSpan(height)
	if err != nil {
		return err
	}

	err = chain.ledger.UpdateTip(hash, height, root, crypto.SetBig(work))
	if err != nil {
		return err
	}
	chain.currentTip = Tip{
		MostWorkHash:    hash,
		MostWorkHeight:  height,
		MostWorkRoot:    root,
		TotalChainWork:  crypto.SetBig(work),
		MedianTimestamp: uint64(medianTimestamp),
	}

	return nil
}

// UpdateTip updates the blockchain tip
func (chain *Blockchain) UpdateTip(header *types.Header) error {

	hash, _ := header.Hash()
	height := header.GetHeight()
	root := header.GetStateRoot()
	work := header.GetChainWork().Big()

	err := chain.updateTip(hash, height, root, work)
	return err
}

// GetPoWLimit returns the proof of work limit for the chain .
func (chain *Blockchain) GetPoWLimit() *big.Int {
	return chain.config.GetPoWLimit().Big()
}

// ApplyBlock is the main transition function, it validates a block
// and applies to the new state, there are several cases to handle :
// 1. The block extends the current tip, in this case we validate block
// apply transition to the state if the block is valid and the transition
// accepts the block becomes the new tip.
// 2. The block forks the chain at height-k, unless the block has enough
// difficulty to overcome the current chain it's stored in the blocksCache no transitions
// are applied.
// 3. The block forks the chain and has enough difficulty to become the new one
// in this case we disconnect blocks in between (reversting state transitions)
// and marking blocks in between as side branches.
// 4. The block is more recent than our tip or it's parent is not present
// in this case the block is blocksCached until we receive it's parent and is orphaned.
// There's a soft-limit on how old we can go back currently limited at coinbase
// maturity.
func (chain *Blockchain) ApplyBlock(newBlock *types.Block, parent *types.Header) (Tip, bool, error) {

	currentTip := chain.GetChainTip()

	currentTipBlock, err := chain.GetHeaderByHash(currentTip.MostWorkHash)
	if err != nil {
		return currentTip, false, err
	}
	blkHash, err := newBlock.Hash()
	if err != nil {
		return currentTip, false, err
	}
	parentHash, err := parent.Hash()
	if err != nil {
		return currentTip, false, err
	}

	if parentHash.Equal(currentTip.MostWorkHash) {
		err = chain.AppendBlock(newBlock, MAIN)
		if err != nil {
			return currentTip, false, err
		}

		err := chain.connectBlock(newBlock)
		if err != nil {
			return currentTip, false, err
		}

		err = chain.updateTip(blkHash, newBlock.GetHeight(), newBlock.GetStateRoot(), newBlock.ChainWork.Big())
		if err != nil {
			return currentTip, false, err
		}
		return chain.GetChainTip(), true, nil
	} else if chain.isSide(parentHash) || currentTip.MostWorkHeight == newBlock.GetHeight() {
		// If the parent is a sidebranch and this block is extending it, or if we're creating
		// a fork we use the fork policy to check if we need to reorganize or not.
		if IsBetter(newBlock.Header, currentTipBlock) {
			blocksToDisconnect, blocksToConnect, err := chain.resolveReorg(currentTipBlock, newBlock.Header)
			if err != nil {
				return currentTip, false, err
			}

			ok, err := chain.reorganizeChain(blocksToDisconnect, blocksToConnect)

			if !ok || err != nil {
				return currentTip, false, err
			}

			err = chain.AppendBlock(newBlock, MAIN)
			if err != nil {
				return currentTip, false, err
			}
			err = chain.updateTip(blkHash, newBlock.GetHeight(), newBlock.GetStateRoot(), newBlock.ChainWork.Big())
			if err != nil {
				return currentTip, false, err
			}
			return chain.currentTip, true, nil
		}
		// if the block doesn't overtake the main branch we mark it as a side block
		ok := chain.markSide(blkHash)
		if !ok {
			return currentTip, false, err
		}
		err := chain.AppendBlock(newBlock, SIDE)
		if err != nil {
			return currentTip, false, err
		}
		return currentTip, true, nil
	}

	// if the block doesn't extend the tip or a sidebranch
	// the block is marked as orphan
	ok := chain.markOrphan(blkHash)
	err = chain.AppendBlock(newBlock, ORPHAN)
	if !ok || err != nil {
		return currentTip, false, err
	}
	return currentTip, true, nil
}

// GetStateView returns a read only copy of the contract's state at root-k, while the object
// is a state copy it holds the same pointer to the low level storage.
func (chain *Blockchain) GetStateView(root []byte) state.StateView {
	chain.mu.RLock()
	defer chain.mu.RUnlock()

	return state.NewStateView(chain.state)
}

// GetChainView returns a read only copy of the blockchain.
func (chain *Blockchain) GetChainView() BlockchainView {
	return chain
}

// AppendHeader blocksCaches the header and stores it on disk
func (chain *Blockchain) AppendHeader(hdr *types.Header, status ledger.BlockMarker) error {

	chain.mu.Lock()
	defer chain.mu.Unlock()

	hdrHash, err := hdr.Hash()
	if err != nil {
		return err
	}
	if !chain.headersCache.Contains(hdrHash) {
		chain.headersCache.Add(hdrHash, hdr)
	}
	switch status {
	case ORPHAN:
		chain.orphans.Add(hdrHash)
	case SIDE:
		chain.side.Add(hdrHash)
	case MAIN:
		chain.blockIndex[hdr.GetHeight()] = hdrHash
	}

	return chain.ledger.PutNewHeader(hdr, status)
}

// AppendBlock blocksCaches the block and stores it on disk
func (chain *Blockchain) AppendBlock(blk *types.Block, status ledger.BlockMarker) error {

	chain.mu.Lock()
	defer chain.mu.Unlock()

	blkHash, err := blk.Hash()
	if err != nil {
		return err
	}
	if !chain.blocksCache.Contains(blkHash) {
		chain.blocksCache.Add(blkHash, blk)
	}
	if !chain.headersCache.Contains(blkHash) {
		chain.headersCache.Add(blkHash, blk.Header)
	}

	switch status {
	case ORPHAN:
		chain.orphans.Add(blkHash)
	case SIDE:
		chain.side.Add(blkHash)
	case MAIN:
		chain.blockIndex[blk.GetHeight()] = blkHash
	}

	return chain.ledger.PutNewBlock(blk, status)
}

// isInCache checks whether a given block is already in our blocksCache
func (chain *Blockchain) isInCache(blkHash crypto.Hash) bool {

	return chain.blocksCache.Contains(blkHash)
}

// markOrphan marks a given block as orphan
func (chain *Blockchain) markOrphan(blkHash crypto.Hash) bool {
	if ok := chain.orphans.Add(blkHash); !ok {
		return false
	}

	err := chain.ledger.MarkBranch(blkHash.Bytes(), ORPHAN)
	return err == nil
}

// markSide marks a given block as a side branch
func (chain *Blockchain) markSide(blkHash crypto.Hash) bool {

	if ok := chain.side.Add(blkHash); !ok {
		return false
	}

	err := chain.ledger.MarkBranch(blkHash.Bytes(), SIDE)
	return err == nil
}

// markMain marks a given block as part of main branch
func (chain *Blockchain) markMain(blkHash crypto.Hash, height uint64) bool {
	err := chain.ledger.MarkBranch(blkHash.Bytes(), MAIN)
	if err != nil {
		return false
	}
	chain.blockIndex[height] = blkHash
	return true
}

// isSide checks whether a given block is on a side branch
func (chain *Blockchain) isSide(blkHash crypto.Hash) bool {
	return chain.side.Contains(blkHash)
}

// isOrphan checks whether a given block is orphaned
func (chain *Blockchain) isOrphan(blkHash crypto.Hash) bool {
	return chain.orphans.Contains(blkHash)
}

// GetBranch returns the branch on which a block is part of.
func (chain *Blockchain) GetBranch(blkHash crypto.Hash) ledger.BlockMarker {

	chain.mu.RLock()
	defer chain.mu.RUnlock()

	if chain.isInCache(blkHash) {

		if chain.isSide(blkHash) {
			return SIDE
		} else if chain.isOrphan(blkHash) {
			return ORPHAN
		}
		return MAIN
	}

	return chain.ledger.GetBranch(blkHash.Bytes())
}

// resolveReorg tracks the chain history for the common ancestor
// and returns a list of blocks to disconnect and connect
// this is triggered when a side branch overcomes the new branch
// and we need to re-Org the chain.
func (chain *Blockchain) resolveReorg(currentTip, newTip *types.Header) ([]crypto.Hash, []crypto.Hash, error) {

	currentTipHash, _ := currentTip.Hash()
	newTipHash, _ := newTip.Hash()

	commonAncestor, err := chain.findCommonAncestor(currentTip, newTip)
	if err != nil {
		return nil, nil, err
	}
	commonAncestorHash, _ := commonAncestor.Hash()
	blocksToDisconnect, err := chain.locateHistory(commonAncestorHash, currentTipHash)
	if err != nil {
		return nil, nil, err
	}

	blocksToConnect, err := chain.locateHistory(commonAncestorHash, newTipHash)
	if err != nil {
		return nil, nil, err
	}

	return blocksToDisconnect, blocksToConnect, nil
}

// GetBlocksInRange walks the chain from start to end hash
// and returns all blocks in between
func (chain *Blockchain) GetBlocksInRange(start, end crypto.Hash) ([]*types.Block, error) {

	blocks := make([]*types.Block, 0, 16)

	for iter := end; iter != start; {

		block, err := chain.GetBlockByHash(iter)
		if err != nil {
			return nil, err
		}
		blocks = append(blocks, block)
		iter = block.GetParent()

	}
	return blocks, nil
}

// GetHeadersInRange walks the chain from start to end hash
// and returns all blocks in between in reverse order (end first)
func (chain *Blockchain) GetHeadersInRange(start, end crypto.Hash) ([]*types.Header, error) {

	headers := make([]*types.Header, 0, 16)

	for iter := end; iter != start; {

		header, err := chain.GetHeaderByHash(iter)
		if err != nil {
			return nil, err
		}
		headers = append(headers, header)
		iter = header.GetParent()

	}
	return headers, nil
}

// ComputeMedianTimeSpan calculates the median timestamp of the past PoWAveragingWindow
// blocks
func (chain *Blockchain) ComputeMedianTimeSpan(height uint64) (int64, error) {

	if height < uint64(PoWAveragingWindow) {
		return time.Now().Unix(), nil
	}
	header, err := chain.GetHeaderByHeight(height, MAIN)
	if err != nil {
		return 0, err
	}
	return medianTime(header.GetHeight(), chain, MAIN)

}

// ComputeTargetThreshold calculates the expected target threshold at height k
func (chain *Blockchain) ComputeTargetThreshold(height uint64) (*big.Int, error) {
	return targetThreshold(height, chain, MAIN)
}

// disconnectBlock disconnects a block from the main chain by reverting it's state
// transition.
func (chain *Blockchain) disconnectBlock(blk *types.Block) error {
	if bytes.Equal(chain.state.Root(), blk.GetStateRoot().Bytes()) {

		delta, err := generateStateDelta(blk, chain.state)
		if err != nil {
			return err
		}
		err = revertStateDelta(chain.state, delta)
		if err != nil {
			return err
		}
		return nil
	}

	return errStateRootMismatch

}

// connectBlock connects a new block to the chain updating the tip and actively
// update the state.
func (chain *Blockchain) connectBlock(blk *types.Block) error {
	if blk.Height == 0 {
		root, err := ProcessBlockTransition(blk, chain.state, false)
		if err != nil {
			return err

		}
		if !bytes.Equal(blk.GetStateRoot().Bytes(), root) {
			return errStateRootMismatch
		}
		return nil
	}
	parent, err := chain.GetHeaderByHash(blk.GetParent())
	if err != nil {
		return err

	}
	if bytes.Equal(chain.GetChainTip().MostWorkRoot.Bytes(), parent.GetStateRoot().Bytes()) {
		root, err := ProcessBlockTransition(blk, chain.state, false)
		if err != nil {
			return err

		}
		if !bytes.Equal(blk.GetStateRoot().Bytes(), root) {
			return errStateRootMismatch
		}
		return nil
	}

	return errors.New("invalid parent state root can't apply block")

}

// reorganizeChain takes a list of blocks to disconnect and blocks to connect
// and reorganizes the chain accordingly.
func (chain *Blockchain) reorganizeChain(blocksToDisconnect, blocksToConnect []crypto.Hash) (bool, error) {

	for _, blockHash := range blocksToDisconnect {

		block, err := chain.GetBlockByHash(blockHash)
		if err != nil {
			return false, err
		}
		err = chain.disconnectBlock(block)
		if err != nil {
			return false, err
		}
		ok := chain.markSide(blockHash)
		if !ok {
			return false, errors.New("failed to mark block as side branch")
		}
	}

	for _, blockHash := range blocksToConnect {
		block, err := chain.GetBlockByHash(blockHash)
		if err != nil {
			return false, err
		}
		err = chain.connectBlock(block)
		if err != nil {
			return false, err
		}
		ok := chain.markMain(blockHash, block.Height)
		if !ok {
			return false, errors.New("failed to mark block as side branch")
		}
	}

	return true, nil
}

// LoadLastSavedState is used to read the last saved state from disk
// in memory.
func (chain *Blockchain) LoadLastSavedState() ([]byte, error) {
	tipRoot, err := chain.ledger.GetTipRoot()
	if err != nil {
		return nil, err
	}
	err = chain.state.LoadLastSavedState(tipRoot.Bytes())
	if err != nil {
		return nil, err
	}
	return tipRoot.Bytes(), nil
}

// LoadGenesis block into blockchain state.
func (chain *Blockchain) LoadGenesis(genesis *types.Block) error {

	err := chain.AppendBlock(genesis, MAIN)
	if err != nil {
		return err
	}
	genesisHash, _ := genesis.Hash()
	_, err = ProcessBlockTransition(genesis, chain.state, false)
	if err != nil {
		return err
	}
	chain.currentTip = Tip{
		MostWorkHash:    genesisHash,
		MostWorkHeight:  genesis.GetHeight(),
		MostWorkRoot:    genesis.GetStateRoot(),
		TotalChainWork:  crypto.SetBig(genesis.GetChainWork().Big()),
		MedianTimestamp: genesis.GetTimestamp(),
	}
	return nil
}

// CreateNextBlock generates a valid block given a set of transaction.
func (chain *Blockchain) CreateNextBlock(txs []*types.Transaction) (*types.Block, error) {

	currentTip := chain.currentTip
	txid, wtxid, transitions, err := ComputeTransactionsRoots(txs)
	if err != nil {
		return nil, err
	}
	header := types.NewHeader(currentTip.MostWorkHash, currentTip.MostWorkRoot, crypto.SetBytes(txid),
		crypto.SetBytes(wtxid), crypto.SetBytes(transitions),
		[]crypto.Hash{}, currentTip.MostWorkHeight+1, 0, uint64(time.Now().Add(30*time.Second).Unix()),
		crypto.SetBig(protocol.RegressionPoWLimit), currentTip.TotalChainWork)
	block := types.NewBlock(header, txs, []proof.CompactTrieAuditPath{})

	postRoot, err := ProcessBlockTransition(block, chain.state, true)
	if err != nil {
		return nil, err
	}

	block.Version = uint64(chain.config.Version())
	block.Root = bytesutil.ToBytes32(postRoot)
	return block, nil
}
