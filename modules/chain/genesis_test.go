package chain

import (
	"io/ioutil"
	"os"
	"testing"
	"time"

	"gitlab.com/cloq/go-cloq/consensus/config"
	"gitlab.com/cloq/go-cloq/modules/storage"
	"gitlab.com/cloq/go-cloq/modules/storage/state"

	"gitlab.com/cloq/go-cloq/protocol"
)

func TestCreateGenesisBlock(t *testing.T) {

	cachedState := state.NewCachedSnapshot(nil)

	genesisBlk, genesisHash, err := CreateGenesisBlock([]byte("Test Genesis Record"), time.Now().Unix(), "1cqtestaddress", protocol.GetBlockIssuance(0), protocol.RegressionPoWLimit)

	if err != nil {
		t.Fatal("failed to create genesis block with error :", err)
	}

	t.Log("Genesis Block Root : ", genesisBlk.GetStateRoot().String())
	t.Log("New Genesis Block Hash :", genesisHash.String())

	ok, err := VerifyMerkleRoots(genesisBlk)
	if !ok || err != nil {
		t.Fatal("failed to process genesis block with error :", err)
	}
	_, err = ProcessBlockTransition(genesisBlk, cachedState, false)
	if err != nil {
		t.Fatal("failed to process genesis block with error :", err)
	}
	conf := config.WhirlpoolRegnet(genesisHash)
	dir, err := ioutil.TempDir(os.TempDir(), "test_data")
	if err != nil {
		t.Fatal("failed to create temp dir", err)
	}
	store, err := storage.NewStorage(dir, false)
	if err != nil {
		t.Fatal("failed to create temp storage", err)
	}
	blockchain, err := New(conf, store)
	if err != nil {
		t.Fatal("failed to create temp chain", err)
	}
	blockchain.LoadGenesis(genesisBlk)
}
