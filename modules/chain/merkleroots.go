package chain

import (
	"bytes"
	"errors"

	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/pkg/merkle"
)

// VerifyMerkleRoots verifies that the header merkle roots are valid
func VerifyMerkleRoots(block *types.Block) (bool, error) {

	txidRoot, wtxidRoot, transitionRoot, err := ComputeTransactionsRoots(block.Transactions())

	if err != nil {
		return false, err
	}
	if !bytes.Equal(block.GetCommitmentRoot().Bytes(), txidRoot) {
		return false, errors.New("invalid commitment root")
	}
	if !bytes.Equal(block.GetWitnessRoot().Bytes(), wtxidRoot) {
		return false, errors.New("invalid witness root")
	}
	if !bytes.Equal(block.GetTransitionRoot().Bytes(), transitionRoot) {
		return false, errors.New("invalid inputs root")
	}
	return true, nil
}

// ComputeTransactionsRoots calculates the merkle roots of block data in the block
// this includes the root of transactions (by their commitment hash), witnesses, contracts
// executed and created.
func ComputeTransactionsRoots(transactions []*types.Transaction) (txidRoot []byte, wtxidRoot []byte, transitionRoot []byte, err error) {

	var txidLeaves = make([][]byte, 0, len(transactions))
	var wtxidLeaves = make([][]byte, 0, len(transactions))
	var inputLeaves = make([][]byte, 0, len(transactions))
	var outpointLeaves = make([][]byte, 0, len(transactions))
	var transitionLeaves = make([][]byte, 0, len(transactions))

	for _, transaction := range transactions {

		txid, err := transaction.Hash()
		if err != nil {
			return nil, nil, nil, err
		}
		wtxid, err := transaction.WTXID()
		if err != nil {
			return nil, nil, nil, err
		}
		inputs, err := transaction.ExecutedContracts()
		if err != nil {
			return nil, nil, nil, err
		}
		outputs, err := transaction.CreatedContractsHash()
		if err != nil {
			return nil, nil, nil, err
		}

		inputLeaves = append(inputLeaves, inputs...)
		outpointLeaves = append(outpointLeaves, outputs...)
		txidLeaves = append(txidLeaves, txid[:])
		wtxidLeaves = append(wtxidLeaves, wtxid[:])
	}

	transitionLeaves = append(transitionLeaves, inputLeaves...)
	transitionLeaves = append(transitionLeaves, outpointLeaves...)

	txidRoot = merkle.Root(txidLeaves)
	wtxidRoot = merkle.Root(wtxidLeaves)
	transitionRoot = merkle.Root(transitionLeaves)

	return txidRoot, wtxidRoot, transitionRoot, nil
}
