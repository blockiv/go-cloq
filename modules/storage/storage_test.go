package storage

import (
	"io/ioutil"
	"os"
	"testing"
)

func TestStorage(t *testing.T) {
	dir, err := ioutil.TempDir(os.TempDir(), "test_data")

	_, err = NewStorage(dir, false)
	if err != nil {
		t.Fatal("failed to create storage with error :", err)
	}
	if err := os.RemoveAll(dir); err != nil {
		t.Log("removal of temp directory test_data failed")
	}
}
