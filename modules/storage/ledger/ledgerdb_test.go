package ledger

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cloq/go-cloq/internal/testgen"
)

func TestNewDB(t *testing.T) {

	_, err := NewLedgerDB("/tmp/ecb.db")
	if err != nil {
		t.Error("failed with error : ", err)
	}

}

func TestBlockStream(t *testing.T) {

	bunchOfBlocks := testgen.GenBlocks()
	t.Log("Done generating Blocks ....")
	nBlocks := len(bunchOfBlocks)
	db, err := NewLedgerDB("/tmp/blockdb.db")
	if err != nil {
		t.Error("failed db initialization with error : ", err)
	}
	t.Log("Starting blockToProcess processing...")
	for idx, blockToProcess := range bunchOfBlocks {
		err := db.PutNewBlock(blockToProcess, MAIN)
		if err != nil {
			t.Error("failed to put new blockToProcess with error : ", err)
		}
		t.Log("Processed Block ", idx, " of ", nBlocks)
	}

	for _, blockToProcess := range bunchOfBlocks {

		blkHash, _ := blockToProcess.Hash()
		blkHeader, err := db.GetHeaderByHash(blkHash.Bytes())
		if err != nil {
			t.Error("failed to get header with error : ", err)
		}
		blkBody, err := db.GetBlockByHash(blkHash.Bytes())
		if err != nil {
			t.Error("failed to get block with error : ", err)
		}
		blk, err := db.GetBlockByHeight(blkHeader.GetHeight(), MAIN)
		if err != nil {
			t.Errorf("failed to get block by height %d with %s expected height %d", blkHeader.GetHeight(), err, blockToProcess.GetHeight())
		}
		branch := db.GetBranch(blkHash.Bytes())

		t.Log("blockToProcess hash : ", blkHash)
		assert.EqualValues(t, blockToProcess.Header, blkHeader)
		assert.EqualValues(t, blockToProcess.Body, blkBody.Body)
		assert.EqualValues(t, blockToProcess.Body, blk.Body)
		assert.EqualValues(t, MAIN, branch)
	}
	db.Close()
}
