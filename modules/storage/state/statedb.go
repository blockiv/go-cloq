package state

import (
	"sync"

	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/pkg/db/memdb"

	"github.com/pkg/errors"
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/pkg/bytesutil"
	"gitlab.com/cloq/go-cloq/pkg/db"
	"gitlab.com/cloq/go-cloq/pkg/pair"
	"gitlab.com/cloq/go-cloq/pkg/trie"
)

// Interface assertions
var _ StateView = (*Snapshot)(nil)

// The following implementation uses a sparse merkle tree with default value
// set to 0 as an optimization.
// The keys in the trie are the created contracts hashes H(TXID||Index||Output).
// The values are their validated state which includes the inclusion height and
// optional membership proofs.

// EXECUTED is the default value of trie open contracts (all closed)
// since the tree is sparse we consider all contract's to be executed,
// when a transaction creates a new contract it gets updated with it's validated
// data in the trie.
var EXECUTED = []byte{0}

// rootKey is used to store the last root of the state
var rootKey = "stateRoot"

// Snapshot represents an underlying OpenContractSet and a state trie.
// The state trie is used for storing hash values used to query the OCS.
// Each valid block is considered a valid state transition,
// represented as a list of executed contracts and newly created contracts..
// The trie keys are H(Contract) essentially the value of input references,
// the values are H(OpenContract)
// A Contract is only considered open after it's block is confirmed by atleast 1-block.
type Snapshot struct {
	root   []byte     // root represents the smt-root
	state  db.DB      // stateDB is used to store H(OpenContract),OpenContract.
	triedb db.DB      // triedb is the leveldb database where we persist the sparse merkle tree
	trie   *trie.Trie // trie is used to store H(Contract),H(OpenContract).
	lock   sync.Mutex
}

// NewStateView creates a new state view copy of the snapshot
func NewStateView(s *Snapshot) *Snapshot {

	return &Snapshot{
		root:  s.Root(),
		state: s.state,
		trie:  trie.NewTrie(s.Root(), crypto.SHA3, s.triedb),
	}
}

// NewCachedSnapshot creates a new in memory Snapshot.
func NewCachedSnapshot(root []byte) *Snapshot {

	stateCache := memdb.New(128)
	trieCache := memdb.New(128)

	return &Snapshot{
		root:   nil,
		state:  stateCache,
		triedb: trieCache,
		trie:   trie.NewTrie(root, crypto.SHA3, trieCache),
	}
}

// NewCachedTrie creates a new in memory sparse merkle tree.
func NewCachedTrie(root []byte, db db.DB) *trie.Trie {
	trieCache := memdb.New(128)
	return trie.NewTrie(nil, crypto.SHA3, trieCache)
}

// NewSnapshot instantiate a new snapshot store.
// Chainstate and TrieDB can either be in memory or on-disk.
// The trie resides largely in memory and is commited to disk once ever few blocks.
// Chainstate is updated at each block thus it's Write/Read heavy.
func NewSnapshot(root []byte, chainstate db.DB, triedb db.DB) *Snapshot {

	return &Snapshot{
		triedb: triedb,
		state:  chainstate,
		trie:   trie.NewTrie(root, crypto.SHA3, triedb),
	}
}

// LoadLastSavedState is used to load the tree with the last
func (s *Snapshot) LoadLastSavedState(lastSavedRoot []byte) error {

	s.root = bytesutil.SafeCopyBytes(lastSavedRoot)
	// load trie cache from the database
	return s.trie.LoadCache(lastSavedRoot)
}

// Close the underlying snapshot
func (s *Snapshot) Close() error {
	err := s.state.Close()
	if err != nil {
		return err
	}
	return s.triedb.Close()
}

// Root returns a copy ! of the state root
func (s *Snapshot) Root() []byte {

	var h = make([]byte, 32)
	copy(h, s.trie.Root)
	return h

}

// Put stores new oclist into the chainstate.db and updates the state trie
func (s *Snapshot) Put(oclist []*types.OpenContract) error {
	// Give the contract's created in a block we update the underlying database
	// by storing them as (H(OpenContract),RLP(OpenContract)) while the trie is updated as
	// (H(Contract),H(OpenContract)) this creates an authentication
	// bridge for the validation data and makes updates and state revert
	// operations atomic.

	var contractHashes = make([][]byte, 0, len(oclist))
	var ocHashes = make([][]byte, 0, len(oclist))
	// We now create a batch put operation for the chainstate db
	var updateBatch = s.state.NewBulk()

	for _, openContract := range oclist {

		// compute the contract hash
		contractHash, err := openContract.ContractHash()
		if err != nil {
			return err
		}
		// compute the opencontract hash
		ocHash, err := openContract.Hash()
		if err != nil {
			return err
		}
		// serialize the contract
		ocBytes, err := openContract.Marshal()

		if err != nil {
			return err
		}
		updateBatch.Put(ocHash[:], ocBytes)

		contractHashes = append(contractHashes, contractHash[:])
		ocHashes = append(ocHashes, ocHash[:])

	}

	// pair package implements sorting pairs the key values by keeping
	// the indices intact
	contractHashes, ocHashes = pair.SortPairs(contractHashes, ocHashes)

	// update the trie
	// atomic update will store the past trie so it's possible
	// to not-commit until a certain interval of blocks has been buried.
	newRoot, err := s.trie.AtomicUpdate(contractHashes, ocHashes)
	if err != nil {
		return err
	}
	updateBatch.Flush()
	err = s.trie.Commit()
	if err != nil {
		return err
	}
	// update snapshot root
	s.root = newRoot

	return nil

}

// getOpenContractHash fetches the OpenContractHash Hash from the trie
func (s *Snapshot) getOpenContractHash(contractHash []byte) ([]byte, error) {
	return s.trie.Get(contractHash)
}

// Get fetches a single OpenContract from the database by its contract hash
func (s *Snapshot) Get(contractHash []byte) (*types.OpenContract, error) {

	var newOC = new(types.OpenContract)

	ocHash, err := s.getOpenContractHash(contractHash)

	if ocHash == nil || err != nil {
		return newOC, errors.Wrap(err, "contract has been executed")
	}

	ocBytes := s.state.Get(ocHash)

	if ocBytes == nil {
		return newOC, errors.New("malformed contract")
	}
	err = newOC.Unmarshal(ocBytes)
	return newOC, errors.WithMessage(err, " UnmarshalBinary failed")
}

// GetBatch fetches a batch of Contracts
func (s *Snapshot) GetBatch(contractHashes [][]byte) ([]*types.OpenContract, error) {

	if len(contractHashes) == 0 {
		return []*types.OpenContract{}, nil
	}
	var oclist = make([]*types.OpenContract, 0, len(contractHashes))
	for _, h := range contractHashes {
		contract, err := s.Get(h)
		if err != nil {
			return nil, err
		}
		oclist = append(oclist, contract)
	}
	return oclist, nil
}

// IsOpen returns whether a contract is open.
// A Contract is Open if it's value in the trie is a valid hash different from nil
// and it's available in the OCS.
func (s *Snapshot) IsOpen(contractHash []byte) bool {

	status, err := s.trie.Get(contractHash)
	if err != nil {
		return false
	}
	if status == nil {
		return false
	}
	return true

}

// Delete removes the outpoint hashes from the trie by marking them Spent
// and deletes their oclist from the database
func (s *Snapshot) Delete(oclist []*types.OpenContract) error {

	batchUpdate := s.state.NewBulk()

	var executedContracts = make([][]byte, 0, len(oclist))
	var marker = make([][]byte, 0, len(oclist))

	for _, contract := range oclist {

		contractHash, err := contract.ContractHash()
		if err != nil {
			return err
		}
		executedContracts = append(executedContracts, contractHash[:])
		marker = append(marker, EXECUTED)

		ocHash, err := contract.Hash()
		if err != nil {
			return err
		}

		batchUpdate.Delete(ocHash[:])
	}

	executedContracts, marker = pair.SortPairs(executedContracts, marker)

	newRoot, err := s.trie.AtomicUpdate(executedContracts, marker)

	if err != nil {
		return err
	}
	s.root = newRoot

	err = s.trie.Commit()
	if err != nil {
		return err
	}

	batchUpdate.Flush()

	return nil
}

// GetMerkleProof returns a trie proof of inclusion/non inclusion of an outpoint
// it's the audit path ,a boolean status of inclusion and an error.
// It also returns a proof key and proof value used to for non inclusion proofs.
// proofkey and proof value can be (nil,default)
func (s *Snapshot) GetMerkleProof(key []byte) ([][]byte, bool, []byte, []byte, error) {
	return s.trie.MerkleProof(key)
}

// GetMerkleProofCompact returns a trie proof of inclusion/non inclusion of an outpoint
// it's the audit path ,a boolean status of inclusion and an error.
// It also returns a proof key and proof value used to for non inclusion proofs.
// proofkey and proof value can be (nil,default)
func (s *Snapshot) GetMerkleProofCompact(key []byte) ([]byte, [][]byte, int, bool, []byte, []byte, error) {
	return s.trie.MerkleProofCompact(key)
}

// GetMerkleProofPast returns a trie proof of inclusion/non inclusion of an outpoint
// it's the audit path ,a boolean status of inclusion and an error.
// It also returns a proof key and proof value used to for non inclusion proofs.
// proofkey and proof value can be (nil,default)
func (s *Snapshot) GetMerkleProofPast(key []byte, root []byte) ([][]byte, bool, []byte, []byte, error) {
	return s.trie.MerkleProofPast(key, root)
}

// GetMerkleProofCompactPast returns a trie proof of inclusion/non inclusion of an outpoint
// it's the audit path ,a boolean status of inclusion and an error.
// It also returns a proof key and proof value used to for non inclusion proofs.
// proofkey and proof value can be (nil,default)
func (s *Snapshot) GetMerkleProofCompactPast(key []byte, root []byte) ([]byte, [][]byte, int, bool, []byte, []byte, error) {
	return s.trie.MerkleProofCompactPast(key, root)
}

// VerifyInclusionProof verifies an inclusion proof
func (s *Snapshot) VerifyInclusionProof(proof [][]byte, key, proofVal []byte) bool {
	return s.trie.VerifyInclusion(proof, key, proofVal)
}

// VerifyCompactInclusionProof verifies an inclusion proof
func (s *Snapshot) VerifyCompactInclusionProof(bitmap []byte, index int, proof [][]byte, proofKey, proofVal []byte) bool {
	return s.trie.VerifyInclusionC(bitmap, proofKey, proofVal, proof, index)
}

// VerifyNonInclusionProof verifies an non-inclusion proof
func (s *Snapshot) VerifyNonInclusionProof(proof [][]byte, key []byte, value []byte, proofKey []byte) bool {
	return s.trie.VerifyNonInclusion(proof, key, value, proofKey)
}

// Rollback to previous root
func (s *Snapshot) Rollback(prev []byte) error {

	return s.trie.Revert(prev)

}
