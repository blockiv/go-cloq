package leveldb

import (
	"bytes"
	"fmt"
	"path/filepath"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/errors"
	"github.com/syndtr/goleveldb/leveldb/iterator"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"gitlab.com/cloq/go-cloq/pkg/db"
)

// NewLevelDB instance
func NewLevelDB(dir string) (*LevelDB, error) {
	dbPath := filepath.Join(dir, "data")

	kv, err := leveldb.OpenFile(dbPath, nil)
	if err != nil {
		return nil, err
	}

	database := &LevelDB{
		db: kv,
	}
	return database, nil
}

// LevelDB wraps an internal leveldb backend
type LevelDB struct {
	db *leveldb.DB
}

// Put stores a key value pair
func (db *LevelDB) Put(key, value []byte) {
	key = nilKey(key)
	value = nilKey(value)

	err := db.db.Put(key, value, &opt.WriteOptions{Sync: true})

	if err != nil {
		panic(fmt.Sprintf("Database Error: %v", err))
	}
}

// Delete removes a value given a key
func (db *LevelDB) Delete(key []byte) {
	key = nilKey(key)

	err := db.db.Delete(key, &opt.WriteOptions{Sync: true})

	if err != nil {
		panic(fmt.Sprintf("Database Error: %v", err))
	}
}

// Get fetches a value by key returns nil if not found
func (db *LevelDB) Get(key []byte) []byte {
	key = nilKey(key)
	res, err := db.db.Get(key, nil)
	if err != nil {
		if err == errors.ErrNotFound {
			return []byte{}
		}
		panic(fmt.Sprintf("Database Error: %v", err))
	}
	return res
}

// Has checks whether a value with key exists
func (db *LevelDB) Has(key []byte) bool {
	res, _ := db.db.Has(key, nil)
	return res
}

// Close the backend leveldb file
func (db *LevelDB) Close() error {
	return db.db.Close()
}

// NewTx returns a new bach transaction
func (db *LevelDB) NewTx() db.Tx {
	batch := new(leveldb.Batch)

	return &levelTransaction{db, batch, false, false}
}

// NewBulk returns a new bulk struct
func (db *LevelDB) NewBulk() db.Bulk {
	batch := new(leveldb.Batch)

	return &levelBulk{db, batch, false, false}
}

type levelTransaction struct {
	db        *LevelDB
	tx        *leveldb.Batch
	isDiscard bool
	isCommit  bool
}

func (transaction *levelTransaction) Put(key, value []byte) {
	transaction.tx.Put(key, value)
}

func (transaction *levelTransaction) Delete(key []byte) {
	transaction.tx.Delete(key)
}

func (transaction *levelTransaction) Commit() {
	if transaction.isDiscard {
		panic("Commit after dicard tx is not allowed")
	} else if transaction.isCommit {
		panic("Commit occures two times")
	}
	err := transaction.db.db.Write(transaction.tx, &opt.WriteOptions{Sync: true})
	if err != nil {
		panic(fmt.Sprintf("Database Error: %v", err))
	}
	transaction.isCommit = true
}

func (transaction *levelTransaction) Discard() {
	transaction.isDiscard = true
}

type levelBulk struct {
	db        *LevelDB
	tx        *leveldb.Batch
	isDiscard bool
	isCommit  bool
}

func (bulk *levelBulk) Put(key, value []byte) {
	bulk.tx.Put(key, value)
}

func (bulk *levelBulk) Delete(key []byte) {
	bulk.tx.Delete(key)
}

func (bulk *levelBulk) Flush() {
	if bulk.isDiscard {
		panic("Commit after dicard tx is not allowed")
	} else if bulk.isCommit {
		panic("Commit occures two times")
	}

	err := bulk.db.db.Write(bulk.tx, &opt.WriteOptions{Sync: true})
	if err != nil {
		panic(fmt.Sprintf("Database Error: %v", err))
	}
	bulk.isCommit = true
}

func (bulk *levelBulk) DiscardLast() {
	bulk.isDiscard = true
}

type levelIterator struct {
	start     []byte
	end       []byte
	reverse   bool
	iter      iterator.Iterator
	isInvalid bool
}

// Iterator returns a start-end range iterator
func (db *LevelDB) Iterator(start, end []byte) db.Iterator {
	var reverse bool

	// if end is bigger then start, then reverse order
	if bytes.Compare(start, end) == 1 {
		reverse = true
	} else {
		reverse = false
	}

	iter := db.db.NewIterator(nil, nil)

	if reverse {
		if start == nil {
			iter.Last()
		} else {
			valid := iter.Seek(start)
			if valid {
				soakey := iter.Key()
				if bytes.Compare(start, soakey) < 0 {
					iter.Prev()
				}
			} else {
				iter.Last()
			}
		}
	} else {
		if start == nil {
			iter.First()
		} else {
			iter.Seek(start)
		}
	}
	return &levelIterator{
		iter:      iter,
		start:     start,
		end:       end,
		reverse:   reverse,
		isInvalid: false,
	}
}

func (iter *levelIterator) Next() {
	if iter.Valid() {
		if iter.reverse {
			iter.iter.Prev()
		} else {
			iter.iter.Next()
		}
	} else {
		panic("Iterator is Invalid")
	}
}

func (iter *levelIterator) Valid() bool {

	// Once invalid, forever invalid.
	if iter.isInvalid {
		return false
	}

	// Panic on DB error.  No way to recover.
	if err := iter.iter.Error(); err != nil {
		panic(err)
	}

	// If source is invalid, invalid.
	if !iter.iter.Valid() {
		iter.isInvalid = true
		return false
	}

	// If key is end or past it, invalid.
	var end = iter.end
	var key = iter.iter.Key()

	if iter.reverse {
		if end != nil && bytes.Compare(key, end) <= 0 {
			iter.isInvalid = true
			return false
		}
	} else {
		if end != nil && bytes.Compare(end, key) <= 0 {
			iter.isInvalid = true
			return false
		}
	}

	// Valid
	return true
}

func (iter *levelIterator) Key() (key []byte) {
	if !iter.Valid() {
		panic("Iterator is invalid")
	} else if err := iter.iter.Error(); err != nil {
		panic(err)
	}

	originalKey := iter.iter.Key()

	key = make([]byte, len(originalKey))
	copy(key, originalKey)

	return key
}

func (iter *levelIterator) Value() (value []byte) {
	if !iter.Valid() {
		panic("Iterator is invalid")
	} else if err := iter.iter.Error(); err != nil {
		panic(err)
	}
	originalValue := iter.iter.Value()

	value = make([]byte, len(originalValue))
	copy(value, originalValue)

	return value
}

// Safeguard against nil keys
func nilKey(key []byte) []byte {
	if key == nil {
		return []byte{}
	}
	return key
}
