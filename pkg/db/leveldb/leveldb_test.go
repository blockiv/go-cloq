package leveldb

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	TestKey1 = "tempkey1"
	TestKey2 = "tempkey2"
	TestVal1 = "val1"
	TestVal2 = "val2"
	TestDict = map[string]string{
		"bitcoin":  "btc",
		"litecoin": "ltc",
		"cloq":     "clq",
		"bismuth":  "bis",
		"ethereum": "eth",
		"viacoin":  "via",
	}
)

func createTmpDB() (dir string, db *LevelDB) {
	dir, err := ioutil.TempDir("", "leveldb-tests")
	if err != nil {
		log.Fatal(err)
	}

	db, e := NewLevelDB(dir)
	if e != nil {
		log.Fatal(err)
	}

	return dir, db
}

func TestGetPutDeleteHas(t *testing.T) {

	dir, db := createTmpDB()

	// initial value of empty key must be empty byte
	assert.Empty(t, db.Get([]byte("bitcoin")))
	assert.False(t, db.Has([]byte("bitcoin")))
	for k, v := range TestDict {
		db.Put([]byte(k), []byte(v))
	}
	// check value set
	assert.Equal(t, "via", string(db.Get([]byte("viacoin"))))
	assert.True(t, db.Has([]byte("cloq")))

	// delete value
	db.Delete([]byte("litecoin"))

	// value must be erased
	assert.Empty(t, db.Get([]byte("litecoin")))
	assert.False(t, db.Has([]byte("litecoin")))

	db.Close()
	os.RemoveAll(dir)
}

func TestTransactionPut(t *testing.T) {

	dir, db := createTmpDB()

	// create a new writable tx
	tx := db.NewTx()

	for k, v := range TestDict {
		tx.Put([]byte(k), []byte(v))
	}

	// Value hasn't yet been commited to the database
	assert.Empty(t, db.Get([]byte("bitcoin")))
	assert.Empty(t, db.Get([]byte("ethereum")))
	assert.Empty(t, db.Get([]byte("bismuth")))
	assert.Empty(t, db.Get([]byte("cloq")))

	tx.Commit()

	// after commit, the value should now be in db
	assert.Equal(t, "btc", string(db.Get([]byte("bitcoin"))))
	assert.Equal(t, "via", string(db.Get([]byte("viacoin"))))
	assert.Equal(t, "clq", string(db.Get([]byte("cloq"))))
	assert.Equal(t, "eth", string(db.Get([]byte("ethereum"))))
	assert.Equal(t, "ltc", string(db.Get([]byte("litecoin"))))
	assert.Equal(t, "bis", string(db.Get([]byte("bismuth"))))

	db.Close()
	os.RemoveAll(dir)

}

func TestTransactionDiscard(t *testing.T) {

	dir, db := createTmpDB()

	// create a new writable tx
	tx := db.NewTx()
	// discard test
	tx = db.NewTx()
	// set the value in the tx
	tx.Put([]byte(TestKey1), []byte(TestVal1))

	// discard tx
	tx.Discard()

	assert.Panics(t, func() { tx.Commit() }, "commit after discard is not allowed")

	// after discard, the value must be reset at the db
	assert.False(t, db.Has([]byte(TestKey1)))

	db.Close()
	os.RemoveAll(dir)
}

func TestTransactionDelete(t *testing.T) {

	dir, db := createTmpDB()

	// create a new writable tx
	tx := db.NewTx()

	// set the values in the tx
	tx.Put([]byte(TestKey1), []byte(TestVal1))
	tx.Put([]byte(TestKey2), []byte(TestVal2))

	// delete the value in the tx
	tx.Delete([]byte(TestKey1))

	tx.Commit()

	// after commit, chekc the value from the db
	assert.Equal(t, "", string(db.Get([]byte(TestKey1))))

	db.Close()
	os.RemoveAll(dir)
}

func TestTransactionCommitTwice(t *testing.T) {

	dir, db := createTmpDB()

	// create a new writable tx
	tx := db.NewTx()

	// a first commit will success
	tx.Commit()

	// a second commit will cause panic
	assert.Panics(t, func() { tx.Commit() })

	db.Close()
	os.RemoveAll(dir)
}

func TestBulkPut(t *testing.T) {

	dir, db := createTmpDB()

	// create a new Bulk instance
	bulk := db.NewBulk()

	// set the huge number of value in the bulk
	for i := 0; i < 1000000; i++ {
		bulk.Put([]byte(fmt.Sprintf("key%d", i)),
			[]byte(TestVal2))
	}

	bulk.Flush()

	// after commit, the value visible from the db

	for i := 0; i < 1000000; i++ {
		assert.Equal(t, TestVal2, string(db.Get([]byte(fmt.Sprintf("key%d", i)))))
	}

	db.Close()
	os.RemoveAll(dir)
}

func TestBulkDelete(t *testing.T) {

	dir, db := createTmpDB()

	// create a new Bulk instance
	putBatch := db.NewBulk()
	delBatch := db.NewBulk()
	// set the huge number of value in the bulk
	for i := 0; i < 10; i++ {
		putBatch.Put([]byte(fmt.Sprintf("key%d", i)),
			[]byte(TestVal2))
	}

	putBatch.Flush()
	for i := 0; i < 10; i++ {
		assert.Equal(t, TestVal2, string(db.Get([]byte(fmt.Sprintf("key%d", i)))))
	}
	// after remove the keys
	for i := 0; i < 10; i++ {
		delBatch.Delete([]byte(fmt.Sprintf("key%d", i)))

	}
	// after commit, the value shouldn't be visible from the db
	delBatch.Flush()
	for i := 0; i < 10; i++ {
		assert.Equal(t, "", string(db.Get([]byte(fmt.Sprintf("key%d", i)))))
	}

	db.Close()
	os.RemoveAll(dir)
}
func TestIter(t *testing.T) {

	dir, db := createTmpDB()
	for i := 0; i < 8; i++ {
		db.Put([]byte(strconv.Itoa(i)), []byte(strconv.Itoa(i)))
	}

	i := 0
	for iter := db.Iterator(nil, nil); iter.Valid(); iter.Next() {
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Key()))
		i++
	}

	db.Close()
	os.RemoveAll(dir)
}

func TestRangeIter(t *testing.T) {

	dir, db := createTmpDB()

	for i := 1; i < 8; i++ {
		db.Put([]byte(strconv.Itoa(i)), []byte(strconv.Itoa(i)))
	}

	// test iteration 2 -> 5
	i := 2
	for iter := db.Iterator([]byte("2"), []byte("5")); iter.Valid(); iter.Next() {
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Key()))
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Value()))
		i++
	}
	assert.EqualValues(t, i, 5)

	// nil sames with []byte("0")
	// test iteration 0 -> 5
	i = 1
	for iter := db.Iterator(nil, []byte("5")); iter.Valid(); iter.Next() {
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Key()))
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Value()))
		i++
	}
	assert.EqualValues(t, i, 5)

	db.Close()
	os.RemoveAll(dir)
}

func TestReverseIter(t *testing.T) {

	dir, db := createTmpDB()

	for i := 1; i < 8; i++ {
		db.Put([]byte(strconv.Itoa(i)), []byte(strconv.Itoa(i)))
	}

	// test reverse iteration 5 <- 2
	i := 5
	for iter := db.Iterator([]byte("5"), []byte("2")); iter.Valid(); iter.Next() {
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Key()))
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Value()))
		i--
	}
	assert.EqualValues(t, i, 2)

	i = 5
	for iter := db.Iterator([]byte("5"), nil); iter.Valid(); iter.Next() {
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Key()))
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Value()))
		i--
	}
	assert.EqualValues(t, i, 0)

	db.Close()
	os.RemoveAll(dir)
}
