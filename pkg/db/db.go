// Copyright 2018 The cloq Authors
// This file is part of the cloq-core library .
//
// The cloq-core library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The cloq-core library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the cloq-core library. If not, see <http://www.gnu.org/licenses/>.

// Package db offers an interface to low level key value stores and storage in Generate
package db

// DB is an general interface to access at storage data
type DB interface {
	Put(key, value []byte)
	Delete(key []byte)
	Get(key []byte) []byte
	Has(key []byte) bool
	Iterator(start, end []byte) Iterator
	NewTx() Tx
	Close() error
	NewBulk() Bulk
}

// Tx is used to batch multiple operations
type Tx interface {
	//	Get(key []byte) []byte
	Put(key, value []byte)
	Delete(key []byte)
	Commit()
	Discard()
}

// Iterator is used to navigate specific key ranges
type Iterator interface {
	Next()
	Valid() bool
	Key() []byte
	Value() []byte
}

// Bulk is used to batch multiple transactions
// This will internally commit transactions when reach maximum tx size
type Bulk interface {
	Put(key, value []byte)
	Delete(key []byte)
	Flush()
	DiscardLast()
}
