// Package memdb implements db and iterator interfaces for a map
package memdb

import (
	"bytes"
	"container/list"
	"sort"
	"sync"

	"gitlab.com/cloq/go-cloq/pkg/db"
)

// MemDB implements an in memory database thread-safe over a map
type MemDB struct {
	lock sync.Mutex
	db   map[string][]byte
}

// New creates a new instance of MemDb
func New(size int) *MemDB {

	return &MemDB{db: make(map[string][]byte, size)}
}

// Put a key value pair
func (db *MemDB) Put(key, value []byte) {
	db.lock.Lock()
	defer db.lock.Unlock()

	key = nilKey(key)
	value = nilKey(value)

	db.db[string(key)] = value
}

// Delete a value and it's key of course
func (db *MemDB) Delete(key []byte) {
	db.lock.Lock()
	defer db.lock.Unlock()

	key = nilKey(key)

	delete(db.db, string(key))
}

// Get a value by key
func (db *MemDB) Get(key []byte) []byte {
	db.lock.Lock()
	defer db.lock.Unlock()

	key = nilKey(key)

	return db.db[string(key)]
}

// Has a key-value or not
func (db *MemDB) Has(key []byte) bool {
	db.lock.Lock()
	defer db.lock.Unlock()

	key = nilKey(key)

	_, ok := db.db[string(key)]

	return ok
}

// Close statisfies the interface
func (db *MemDB) Close() error {
	return nil
}

// NewTx on db
func (db *MemDB) NewTx() db.Tx {

	return &memoryTx{
		db:        db,
		opList:    list.New(),
		isDiscard: false,
		isCommit:  false,
	}
}

// Tx over memory db
type memoryTx struct {
	txLock    sync.Mutex
	db        *MemDB
	opList    *list.List
	isDiscard bool
	isCommit  bool
}

type txOp struct {
	isSet bool
	key   []byte
	value []byte
}

// Put Set a key-value and add it to transaction list
func (transaction *memoryTx) Put(key, value []byte) {
	transaction.txLock.Lock()
	defer transaction.txLock.Unlock()

	key = nilKey(key)
	value = nilKey(value)

	transaction.opList.PushBack(&txOp{true, key, value})
}

// Delete a key-value as an operation in a transaction
func (transaction *memoryTx) Delete(key []byte) {
	transaction.txLock.Lock()
	defer transaction.txLock.Unlock()

	key = nilKey(key)

	transaction.opList.PushBack(&txOp{false, key, nil})
}

// Commit will execute the transaction ops
func (transaction *memoryTx) Commit() {
	transaction.txLock.Lock()
	defer transaction.txLock.Unlock()

	if transaction.isDiscard {
		panic("Commit after dicard tx is not allowed")
	} else if transaction.isCommit {
		panic("Commit occures two times")
	}

	txdb := transaction.db

	txdb.lock.Lock()
	defer txdb.lock.Unlock()

	for e := transaction.opList.Front(); e != nil; e = e.Next() {
		op := e.Value.(*txOp)
		if op.isSet {
			txdb.db[string(op.key)] = op.value
		} else {
			delete(txdb.db, string(op.key))
		}
	}

	transaction.isCommit = true
}

// Discard a transaction
func (transaction *memoryTx) Discard() {
	transaction.txLock.Lock()
	defer transaction.txLock.Unlock()

	transaction.isDiscard = true
}

// NewBulk returns a batch tx struct
func (db *MemDB) NewBulk() db.Bulk {

	return &memBulk{
		db:        db,
		opList:    list.New(),
		isDiscard: false,
		isCommit:  false,
	}
}

type memBulk struct {
	txLock    sync.Mutex
	db        *MemDB
	opList    *list.List
	isDiscard bool
	isCommit  bool
}

func (bulk *memBulk) Put(key, value []byte) {
	bulk.txLock.Lock()
	defer bulk.txLock.Unlock()

	key = nilKey(key)
	value = nilKey(value)

	bulk.opList.PushBack(&txOp{true, key, value})
}

func (bulk *memBulk) Delete(key []byte) {
	bulk.txLock.Lock()
	defer bulk.txLock.Unlock()

	key = nilKey(key)

	bulk.opList.PushBack(&txOp{false, key, nil})
}

func (bulk *memBulk) Flush() {
	bulk.txLock.Lock()
	defer bulk.txLock.Unlock()

	if bulk.isDiscard {
		panic("Commit after dicard tx is not allowed")
	} else if bulk.isCommit {
		panic("Commit occures two times")
	}

	bulkdb := bulk.db

	bulkdb.lock.Lock()
	defer bulkdb.lock.Unlock()

	for e := bulk.opList.Front(); e != nil; e = e.Next() {
		op := e.Value.(*txOp)
		if op.isSet {
			bulkdb.db[string(op.key)] = op.value
		} else {
			delete(bulkdb.db, string(op.key))
		}
	}

	bulk.isCommit = true
}

func (bulk *memBulk) DiscardLast() {
	bulk.txLock.Lock()
	defer bulk.txLock.Unlock()

	bulk.isDiscard = true
}

// Iterator should provide range queries and reverse iteration
type memoryIterator struct {
	start     []byte
	end       []byte
	reverse   bool
	keys      []string
	isInvalid bool
	cursor    int
	db        *MemDB
}

// Range queries for a key
func isKeyInRange(key []byte, start []byte, end []byte, reverse bool) bool {
	if reverse {
		if start != nil && bytes.Compare(start, key) < 0 {
			return false
		}
		if end != nil && bytes.Compare(key, end) <= 0 {
			return false
		}
		return true
	}

	if bytes.Compare(key, start) < 0 {
		return false
	}
	if end != nil && bytes.Compare(end, key) <= 0 {
		return false
	}
	return true

}

// Iterator from a start key to an end key
func (db *MemDB) Iterator(start, end []byte) db.Iterator {
	db.lock.Lock()
	defer db.lock.Unlock()

	var reverse bool

	// if end is bigger then start, then reverse order
	if bytes.Compare(start, end) == 1 {
		reverse = true
	} else {
		reverse = false
	}

	var keys sort.StringSlice

	for key := range db.db {
		if isKeyInRange([]byte(key), start, end, reverse) {
			keys = append(keys, key)
		}
	}
	if reverse {
		sort.Sort(sort.Reverse(keys))
	} else {
		sort.Strings(keys)
	}

	return &memoryIterator{
		start:     start,
		end:       end,
		reverse:   reverse,
		isInvalid: false,
		keys:      keys,
		cursor:    0,
		db:        db,
	}
}

// Next key-value
func (iter *memoryIterator) Next() {
	if !iter.Valid() {
		panic("Iterator is Invalid")
	}

	iter.cursor++
}

// Valid on whether we are in range or not
func (iter *memoryIterator) Valid() bool {
	// Once invalid, forever invalid.
	if iter.isInvalid {
		return false
	}

	return 0 <= iter.cursor && iter.cursor < len(iter.keys)
}

// Key where cursor is pointing right now
func (iter *memoryIterator) Key() (key []byte) {
	if !iter.Valid() {
		panic("Iterator is Invalid")
	}

	return []byte(iter.keys[iter.cursor])
}

// Value of the key at the current cursor
func (iter *memoryIterator) Value() (value []byte) {
	if !iter.Valid() {
		panic("Iterator is Invalid")
	}

	key := []byte(iter.keys[iter.cursor])

	return iter.db.Get(key)
}

// Safeguard against nil keys
func nilKey(key []byte) []byte {
	if key == nil {
		return []byte{}
	}
	return key
}
