/**
 *  @file
 *  @copyright defined in aergo/LICENSE.txt
 */

package trie

import (
	"bytes"
)

var (
	// DefaultLeaf value : [byte(0)]
	DefaultLeaf = []byte{0}
)

const (
	// HashLength is the fixed size of hash function output
	HashLength = 32
	// max number of previous tries to cache for revert operation
	maxPastTries = 300
)

// Hash represents the output of a cryptographic hash function
type Hash [HashLength]byte

func bitIsSet(bits []byte, i int) bool {
	return bits[i/8]&(1<<uint(7-i%8)) != 0
}
func bitSet(bits []byte, i int) {
	bits[i/8] |= 1 << uint(7-i%8)
}

// Sortable is a wrapper for sorting test data
type Sortable [][]byte

func (d Sortable) Len() int {
	return len(d)
}
func (d Sortable) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
}
func (d Sortable) Less(i, j int) bool {
	return bytes.Compare(d[i], d[j]) == -1
}
