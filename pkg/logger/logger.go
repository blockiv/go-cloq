package logger

// Log Levels:
// 3: DebugLevel prints Panics, Fatals, Errors, Warnings, Infos and Debugs
// 2: InfoLevel  prints Panics, Fatals, Errors, Warnings and Info
// 1: WarnLevel  prints Panics, Fatals, Errors and Warnings
// 0: ErrorLevel prints Panics, Fatals and Errors
// Default is level 0
// Code for tagging logs:
// Debug -> Useful debugging information
// Info  -> Something noteworthy happened
// Warn  -> You should probably take a look at this
// Error -> Something failed but I'm not quitting
// Fatal -> Bye

import (
	"fmt"
	"io"
	"log"
	"os"
)

// LogLevel designates a logging level
type LogLevel int

const (
	// LogLevelError for error logs
	LogLevelError LogLevel = 0
	// LogLevelWarning for warning logs
	LogLevelWarning LogLevel = 1
	// LogLevelInfo for informational logs
	LogLevelInfo LogLevel = 2
	// LogLevelDebug for debug logs
	LogLevelDebug LogLevel = 3
)

var logLevel = LogLevelDebug // the default

// SetLogLevel specifies a level to log to
func SetLogLevel(newLevel int) {
	logLevel = LogLevel(newLevel)
}

// SetLogFile specify a file and write logs to
func SetLogFile(logFile io.Writer) {
	log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds)
	logOutput := io.MultiWriter(os.Stdout, logFile)
	log.SetOutput(logOutput)
}

func getPrefix(level string) string {
	return fmt.Sprintf("[%s]", level)
}

// Fatalln ...
func Fatalln(args ...interface{}) {
	log.Fatalln(args...)
}

// Fatalf ....
func Fatalf(format string, args ...interface{}) {
	log.Fatalf(format, args...)
}

// Fatal ...
func Fatal(args ...interface{}) {
	log.Fatal(args...)
}

// Debugf ...
func Debugf(format string, args ...interface{}) {
	if logLevel >= LogLevelDebug {
		log.Printf(fmt.Sprintf("%s %s", getPrefix("DEBUG"), format), args...)
	}
}

// Infof ...
func Infof(format string, args ...interface{}) {
	if logLevel >= LogLevelInfo {
		log.Printf(fmt.Sprintf("%s %s", getPrefix("INFO"), format), args...)
	}
}

// Warnf ...
func Warnf(format string, args ...interface{}) {
	if logLevel >= LogLevelWarning {
		log.Printf(fmt.Sprintf("%s %s", getPrefix("WARN"), format), args...)
	}
}

// Errorf ...
func Errorf(format string, args ...interface{}) {
	if logLevel >= LogLevelError {
		log.Printf(fmt.Sprintf("%s %s", getPrefix("ERROR"), format), args...)
	}
}

// Debugln ...
func Debugln(args ...interface{}) {
	if logLevel >= LogLevelDebug {
		args = append([]interface{}{getPrefix("DEBUG")}, args...)
		log.Println(args...)
	}
}

// Infoln ...
func Infoln(args ...interface{}) {
	if logLevel >= LogLevelInfo {
		args = append([]interface{}{getPrefix("INFO")}, args...)
		log.Println(args...)
	}
}

// Warnln ...
func Warnln(args ...interface{}) {
	if logLevel >= LogLevelWarning {
		args = append([]interface{}{getPrefix("WARN")}, args...)
		log.Println(args...)
	}
}

// Errorln ...
func Errorln(args ...interface{}) {
	if logLevel >= LogLevelError {
		args = append([]interface{}{getPrefix("ERROR")}, args...)
		log.Println(args...)
	}
}

// Debug ...
func Debug(args ...interface{}) {
	if logLevel >= LogLevelDebug {
		args = append([]interface{}{getPrefix("DEBUG")}, args...)
		log.Print(args...)
	}
}

// Info ....
func Info(args ...interface{}) {
	if logLevel >= LogLevelInfo {
		args = append([]interface{}{getPrefix("INFO")}, args...)
		log.Print(args...)
	}
}

// Warn ....
func Warn(args ...interface{}) {
	if logLevel >= LogLevelWarning {
		args = append([]interface{}{getPrefix("WARN")}, args...)
		log.Print(args...)
	}
}

// Error ....
func Error(args ...interface{}) {
	if logLevel >= LogLevelError {
		args = append([]interface{}{getPrefix("ERROR")}, args...)
		log.Print(args...)
	}
}
