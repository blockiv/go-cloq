package schnorrkel

import (
	"github.com/gtank/merlin"
	ristretto "github.com/gtank/ristretto255"
)

const (
	// SigSize is the size of an encoded signature.
	SigSize = 64
	// PublicKeySize is the size of an encoded public key
	PublicKeySize = 32
)

// Signature holds a schnorrkel signature
type Signature struct {
	R *ristretto.Element
	S *ristretto.Scalar
}

// NewSigningContext returns a new transcript initialized with the context for the signature
//.see: https://github.com/w3f/schnorrkel/blob/master/src/context.rs#L183
func NewSigningContext(context, msg []byte) *merlin.Transcript {
	t := merlin.NewTranscript("SigningContext")
	t.AppendMessage([]byte(""), context)
	t.AppendMessage([]byte("sign-bytes"), msg)
	return t
}

// Sign uses the schnorr signature algorithm to sign a message
// See the following for the transcript message
// https://github.com/w3f/schnorrkel/blob/master/src/sign.rs#L158
// Schnorr w/ transcript, secret key x:
// 1. r  $<- G
// 2. R = r*G
// 3. k = scalar(transcript)
// 4. s = kx + r
// signature: (R, s)
// public key used for verification: y = x*G
func Sign(t *merlin.Transcript, sk *PrivateKey) (*Signature, error) {
	t.AppendMessage([]byte("proto-name"), []byte("Schnorr-sig"))

	pub, err := sk.Public()
	if err != nil {
		return nil, err
	}
	pubc := pub.Compress()

	t.AppendMessage([]byte("sign:pk"), pubc[:])

	// sample random nonce
	r, err := RandomScalar()
	if err != nil {
		return nil, err
	}
	R := ristretto.NewElement().ScalarBaseMult(r)
	t.AppendMessage([]byte("sign:R"), R.Encode([]byte{}))

	// sample the challenge scalar c
	challengeScalar := t.ExtractBytes([]byte("sign:c"), 64)
	k := ristretto.NewScalar()
	k.FromUniformBytes(challengeScalar)

	// x is the private key
	var x = sk.key

	// s = kx + r
	s := x.Multiply(x, k).Add(x, r)

	return &Signature{R: R, S: s}, nil
}

// Verify a schnorr signature (R,s)
// 1. k = scalar(transcript)
// 2. R' = -kp + gs
// 3. return R' == R
func Verify(s *Signature, t *merlin.Transcript, p *PublicKey) bool {
	t.AppendMessage([]byte("proto-name"), []byte("Schnorr-sig"))
	pubc := p.Compress()
	t.AppendMessage([]byte("sign:pk"), pubc[:])
	t.AppendMessage([]byte("sign:R"), s.R.Encode([]byte{}))

	kb := t.ExtractBytes([]byte("sign:c"), 64)
	k := ristretto.NewScalar()
	k.FromUniformBytes(kb)

	Rp := ristretto.NewElement()
	Rp = Rp.ScalarBaseMult(s.S)
	ky := (p.key).ScalarMult(k, p.key)
	Rp = Rp.Subtract(Rp, ky)

	return Rp.Equal(s.R) == 1
}

// Decode sets a Signature from bytes
// see: https://github.com/w3f/schnorrkel/blob/master/src/sign.rs#L100
func (s *Signature) Decode(in [64]byte) error {
	s.R = ristretto.NewElement()
	err := s.R.Decode(in[:32])
	if err != nil {
		return err
	}
	in[63] &= 127
	s.S = ristretto.NewScalar()
	return s.S.Decode(in[32:])
}

// Encode turns a signature into a byte array
// see: https://github.com/w3f/schnorrkel/blob/master/src/sign.rs#L77
func (s *Signature) Encode() [64]byte {
	out := [64]byte{}
	renc := s.R.Encode([]byte{})
	copy(out[:32], renc)
	senc := s.S.Encode([]byte{})
	copy(out[32:], senc)
	out[63] |= 128
	return out
}
