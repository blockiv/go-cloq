package schnorrkel

import (
	"crypto/rand"
	"crypto/sha512"

	"github.com/gtank/merlin"
	ristretto "github.com/gtank/ristretto255"
)

// MiniSecretKey is a secret scalar value used to generate a secret key.
type MiniSecretKey struct {
	key [32]byte
}

// PrivateKey consists of a secret scalar and a signing nonce
type PrivateKey struct {
	key   *ristretto.Scalar
	nonce [32]byte
}

// PublicKey is a ristretto element
type PublicKey struct {
	key *ristretto.Element
}

// GenerateKeypair generates a new schnorrkel keypair
func GenerateKeypair() (*PrivateKey, *PublicKey, error) {
	msc, err := NewRandomMiniSecretKey()
	if err != nil {
		return nil, nil, err
	}
	return msc.ExpandEd25519(), msc.Public(), nil
}

// NewMiniSecretKey derives a mini secret key from a byte array
func NewMiniSecretKey(b [64]byte) *MiniSecretKey {
	s := ristretto.NewScalar()
	s.FromUniformBytes(b[:])
	enc := s.Encode([]byte{})
	sk := [32]byte{}
	copy(sk[:], enc)
	return &MiniSecretKey{key: sk}
}

// NewMiniSecretKeyFromRaw derives a mini secret key from little-endian encoded raw bytes.
func NewMiniSecretKeyFromRaw(b [32]byte) (*MiniSecretKey, error) {
	s := b
	return &MiniSecretKey{key: s}, nil
}

// NewRandomMiniSecretKey generates a mini secret key from random
func NewRandomMiniSecretKey() (*MiniSecretKey, error) {
	s := [32]byte{}
	_, err := rand.Read(s[:])
	if err != nil {
		return nil, err
	}

	return &MiniSecretKey{key: s}, nil
}

// NewPublicKey from bytes
func NewPublicKey(b [32]byte) *PublicKey {
	e := ristretto.NewElement()
	e.Decode(b[:])
	return &PublicKey{key: e}
}

// Decode minisecretkey from bytes
func (s *MiniSecretKey) Decode(in [32]byte) (err error) {
	newMiniSK, err := NewMiniSecretKeyFromRaw(in)
	if err != nil {
		return err
	}
	s.key = newMiniSK.key
	return nil
}

// ExpandUniform a mini secret-key to a privatekey
func (s *MiniSecretKey) ExpandUniform() *PrivateKey {

	var nonceBuf [32]byte

	t := merlin.NewTranscript("ExpandSecretKeys")
	t.AppendMessage([]byte("mini"), s.key[:])

	scalarBytes := t.ExtractBytes([]byte("sk"), 64)
	key := ristretto.NewScalar()
	key.FromUniformBytes(scalarBytes[:])

	nonce := t.ExtractBytes([]byte("no"), 32)

	copy(nonceBuf[:], nonce)
	return &PrivateKey{
		key:   key,
		nonce: nonceBuf,
	}
}

// ExpandEd25519 expands a mini secret key into a secret key
// https://github.com/w3f/schnorrkel/blob/43f7fc00724edd1ef53d5ae13d82d240ed6202d5/src/keys.rs#L196
func (s *MiniSecretKey) ExpandEd25519() *PrivateKey {

	var key [32]byte
	var nonce [32]byte

	h := sha512.Sum512(s.key[:])

	copy(key[:], h[:32])
	key[0] &= 248
	key[31] &= 63
	key[31] |= 64
	t := divideScalarByCofactor(key[:])
	copy(key[:], t)

	copy(nonce[:], h[32:])

	var scKey, err = ScalarFromBytes(key)

	if err != nil {
		return nil
	}
	sk := &PrivateKey{key: scKey, nonce: nonce}
	return sk
}

// Public gets the public key corresponding to this mini secret key
func (s *MiniSecretKey) Public() *PublicKey {
	e := ristretto.NewElement()
	sk := s.ExpandEd25519()

	return &PublicKey{key: e.ScalarBaseMult(sk.key)}
}

// Decode forms the secret key from the input bytes
func (s *PrivateKey) Decode(in [32]byte) error {
	var err error
	s.key, err = ScalarFromBytes(in)
	return err
}

// Encode returns the secret key as bytes
func (s *PrivateKey) Encode() [32]byte {
	var keyBytes [32]byte
	copy(keyBytes[:], s.key.Encode(nil))
	return keyBytes

}

// Public gets the public key corresponding to this secret key
func (s *PrivateKey) Public() (*PublicKey, error) {
	e := ristretto.NewElement()

	return &PublicKey{key: e.ScalarBaseMult(s.key)}, nil
}

// Decode Public key from bytes
func (p *PublicKey) Decode(in [32]byte) error {
	p.key = ristretto.NewElement()
	return p.key.Decode(in[:])
}

// Compress returns the encoding of the point underlying the public key
func (p *PublicKey) Compress() [32]byte {
	b := p.key.Encode([]byte{})
	enc := [32]byte{}
	copy(enc[:], b)
	return enc
}

// Encode is a wrapper around compress
func (p *PublicKey) Encode() [32]byte {
	return p.Compress()
}
