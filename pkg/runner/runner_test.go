package runner_test

import (
	"context"
	"testing"
	"time"

	"gitlab.com/cloq/go-cloq/pkg/runner"
	"go.uber.org/atomic"
	"go.uber.org/zap"
)

func TestRunner(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	logger, _ := zap.NewDevelopment()
	defer logger.Sync()
	i := atomic.NewInt32(0)

	runner.RunEvery(ctx, logger, 100*time.Millisecond, func() {
		i.Inc()
	})

	// Sleep for a bit and ensure the value has increased.
	time.Sleep(200 * time.Millisecond)

	if i.Load() == 0 {
		t.Error("Counter failed to increment with ticker")
	}

	cancel()

	// Sleep for a bit to let the cancel take place.
	time.Sleep(100 * time.Millisecond)

	last := i.Load()

	// Sleep for a bit and ensure the value has not increased.
	time.Sleep(200 * time.Millisecond)

	if i.Load() != last {
		t.Error("Counter incremented after stop")
	}
}
