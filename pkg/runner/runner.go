// Package runner provides scheduling for functions that run periodically.
package runner

import (
	"context"
	"reflect"
	"runtime"
	"time"

	"go.uber.org/zap"
)

// RunEvery runs the provided command periodically.
// It runs in a goroutine, and can be cancelled by finishing the supplied context.
func RunEvery(ctx context.Context, log *zap.Logger, period time.Duration, f func()) {

	funcName := runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name()
	ticker := time.NewTicker(period)
	go func() {
		for {
			select {
			case <-ticker.C:
				log.Info("function", zap.String("funcName", funcName), zap.String("status", "running"))
				f()
			case <-ctx.Done():
				log.Debug("function", zap.String("funcName", funcName), zap.String("status", "context is closed, exiting"))
				ticker.Stop()

			}
		}
	}()
}
