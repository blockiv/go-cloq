package types

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"math/big"
	"time"

	"gitlab.com/cloq/go-cloq/pkg/bech32"

	"gitlab.com/cloq/go-cloq/core/addr"
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/proof"
	"gitlab.com/cloq/go-cloq/core/script"
	"gitlab.com/cloq/go-cloq/pkg/leb128"
	"gitlab.com/cloq/go-cloq/protocol"
	"golang.org/x/crypto/sha3"
)

func generateRandomBytes() crypto.Hash {
	var b crypto.Hash
	_, err := rand.Read(b[:])
	// failure of this is caused by the underlying operating system.
	if err != nil {
		panic(err)
	}
	return b
}

var testScripts = []string{
	"sha3 0x92dad9443e4dd6d70a7f11872101ebff87e21798e4fbb26fa4bf590eb440e71b equal",
	"dup blake2 0xb966a57f3010512d1ebe8629864c71c909aeb4752e93ffa525f00976c93e33c0 equal jumpif:@end sha256 0x6cd8a81bea00783c3fcfca07eef8c80adc1fb1ba46dc611d5554983e6ae3a7ea equal @end true",
}

var testWitnesses = []Redeemer{
	[][]byte{[]byte("helloworld")},
	[][]byte{[]byte("hellobob")},
}

// genTransaction generates a dummy transaction.
func genTransaction() *Transaction {
	inputs := make([]Input, 0, 8)
	var wit Witness
	for i := 0; i < 8; i++ {
		b := leb128.FromInt64(int64(i))
		hash := sha3.Sum256(b)
		testScript := testScripts[i%2]
		testScriptB, _ := script.Compile(testScript)
		inputs = append(inputs, Input{Origin: hash, Validator: testScriptB})
		wit.Push(testWitnesses[i%2])
	}

	outputs := make([]Output, 1)
	hash := crypto.SHA3Hash([]byte("a very weird script"))
	address, _ := addr.EncodeHash(hash, "testnet")
	outputs[0] = Output{
		Address:  address,
		Value:    5600,
		Locktime: 100,
	}
	commitment := NewCommitment(inputs, outputs)
	transaction := NewTransaction(0, 0, 0, 0, commitment, wit)

	return transaction
}

// genBlocks generates a slice of dummy blocks
func genBlocks() []*Block {

	// let's generate some coinbase blocks
	hashes := make([]crypto.Hash, 10)

	blocks := make([]*Block, 10)

	var err error
	var i uint64
	for i = 0; i < 10; i++ {
		coinbaseTX := genCoinbase(i)
		hashes[i], err = coinbaseTX.Hash()
		if err != nil {
			panic("failure to calculate test hash")
		}
		hdr := NewHeader(hashes[i], crypto.EmptyHash, hashes[i], crypto.EmptyHash, crypto.EmptyHash, []crypto.Hash{}, 325465, uint64(time.Now().Unix()), 96580, crypto.SetBig(protocol.RegressionPoWLimit), crypto.SetBig(big.NewInt(171772)))
		blk := NewBlock(hdr, append([]*Transaction{}, coinbaseTX, genTransaction()), []proof.CompactTrieAuditPath{{}})
		blocks[i] = blk
	}

	return blocks

}

// genCoinbase generates a dummy coinbase transaction
func genCoinbase(height uint64) *Transaction {
	v := generateRandomBytes()
	nullInput := []Input{{Origin: crypto.EmptyHash, Validator: v[:16]}}
	address, _ := addr.EncodeHash(generateRandomBytes(), protocol.TestnetAddressPrefix)
	output := []Output{{Address: address, Value: protocol.GetBlockIssuance(height), Locktime: protocol.CoinbaseMaturity}}
	return NewTransaction(protocol.WhirlpoolConsensusVer, 0, 0, 10, NewCommitment(nullInput, output), NewWitness())
}

// genOpenContracts generates a list of dummy Open Contracts.
func genOpenContracts(size int) []*OpenContract {

	var txids [][32]byte
	indexes := make([]uint32, 0)
	values := make([]uint64, 0)
	addresses := make([]string, 0)
	var h crypto.Hash
	for i := 0; i < size; i++ {
		key := make([]byte, 32)
		_, err := rand.Read(key)
		if err != nil {
			panic(err)
		}
		h = crypto.SHA3Hash(key)
		txids = append(txids, h)
		fmt.Println("txids ", i, hex.EncodeToString(txids[i][:]))
		indexes = append(indexes, uint32(i*3))
		r := crypto.Blake256(txids[i][:])
		addr, _ := bech32.EncodeCloqAddress("cq", r[:])
		addresses = append(addresses, addr)
		values = append(values, uint64(i*100000/25))

	}
	ocss := make([]*OpenContract, 0)

	for i, txid := range txids {
		out := Output{Address: addresses[i], Value: values[i], Locktime: uint64(i)}
		ocs := &OpenContract{Contract: NewContract(txid, indexes[i], out), InclusionHeight: uint64(size % 375), Coinbase: false}
		ocss = append(ocss, ocs)
	}
	return ocss

}
