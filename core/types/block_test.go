package types

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBlock(t *testing.T) {
	t.Run("TestBlockSerialization", func(t *testing.T) {
		blocks := genBlocks()
		for _, block := range blocks {

			_, err := block.Hash()
			if err != nil {
				t.Error("Hash failed with error :", err)
			}
			blkHdrBytes, err := block.Header.Marshal()
			if err != nil {
				t.Error("Marshal failed with error :", err)
			}
			blkBytes, err := block.Marshal()
			if err != nil {
				t.Error("Marshal failed with error :", err)
			}
			fullBlkBytes, err := block.MarshalFull()
			if err != nil {
				t.Error("Marshal failed with error :", err)
			}

			blk := NewEmptyBlock()
			// Unmarshal the header
			err = blk.Header.Unmarshal(blkHdrBytes)

			if err != nil {
				t.Error("Unmarshal failed with error :", err)
			}
			// Unmarshal the body
			err = blk.Unmarshal(blkBytes)
			if err != nil {
				t.Error("Unmarshal failed with error :", err)
			}

			assert.Equal(t, block, blk)

			blk = NewEmptyBlock()
			err = blk.UnmarshalFull(fullBlkBytes)
			if err != nil {
				t.Error("Unmarshal failed with error :", err)
			}
			assert.Equal(t, block, blk)

			blockJSON, err := json.MarshalIndent(blk, "", " ")
			if err != nil {
				t.Error("MarshalJSON failed with error :", err)
			}
			err = ioutil.WriteFile("test_blk.json", blockJSON, os.ModePerm)
			if err != nil {
				t.Error("WriteFile failed with error :", err)
			}
			blockJSONBytes, err := ioutil.ReadFile("test_blk.json")
			if err != nil {
				t.Error("ReadFile failed with error :", err)
			}
			blk = NewEmptyBlock()
			err = json.Unmarshal(blockJSONBytes, blk)
			if err != nil {
				t.Error("UnmarshalJSON failed with error :", err)
			}
			assert.Equal(t, block, blk)
		}
	})
}
