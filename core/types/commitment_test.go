package types

import (
	"encoding/hex"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cloq/go-cloq/core/addr"
	"gitlab.com/cloq/go-cloq/core/crypto"

	"gitlab.com/cloq/go-cloq/pkg/leb128"
	"golang.org/x/crypto/sha3"
)

// commitmentTest represents a testcase
type commitmentTest struct {
	name string
	c    Commitment
	err  bool
}

var emptyHash [32]byte

func TestCommitment(t *testing.T) {

	t.Run("TestCommitmentConsistency", func(t *testing.T) {
		testCases := []commitmentTest{
			{
				name: "bad commitment : null inputs",
				c: Commitment{
					Inputs:  []Input{},
					Outputs: []Output{{Address: "clq123456!!!", Value: 99, Locktime: 1}},
				},

				err: true,
			}, {
				name: "bad commitment : empty validator null outputs",
				c: Commitment{
					Inputs:  []Input{{Origin: emptyHash}},
					Outputs: []Output{},
				},

				err: true,
			}, {
				name: "bad commitment : duplicate inputs",
				c: Commitment{
					Inputs:  []Input{{Origin: emptyHash, Validator: []byte{0x0, 0x1, 0x2, 0x3, 0x4}}, {Origin: emptyHash, Validator: []byte{0x0, 0x1, 0x2, 0x3, 0x4}}},
					Outputs: []Output{},
				},

				err: true,
			}, {
				name: "bad commitment : empty commitment",
				c: Commitment{
					Inputs:  []Input{},
					Outputs: []Output{},
				},

				err: true,
			},
		}

		for _, test := range testCases {

			ok, err := test.c.Validate(false)
			if ok || err == nil {
				t.Errorf("test[%s] want error : %t bad commitments should fail to validate", test.name, test.err)
			}
			_, err = test.c.Marshal()
			t.Log(err)
			if err != nil {
				t.Errorf("test[%s] :  failed to serialize commitment", test.name)
			}

		}
	})
	t.Run("TestCommitmentValidation", func(t *testing.T) {
		inputs := make([]Input, 0, 8)

		for i := 0; i < 8; i++ {
			b := leb128.FromInt64(int64(i))
			hash := sha3.Sum256(b)
			inputs = append(inputs, Input{Origin: hash, Validator: []byte{}, Sighash: 0x1})
		}

		outputs := make([]Output, 1)
		hash := crypto.SHA3Hash([]byte("a very weird script"))
		address, _ := addr.EncodeHash(hash, "testnet")
		outputs[0] = Output{
			Address:  address,
			Value:    5600,
			Locktime: 100,
		}
		t.Log(outputs[0].Hash())

		commitment := NewCommitment(inputs, outputs)

		if ok, err := commitment.Validate(false); !ok || err != nil {
			t.Error("valid commitment marked invalid")
		}

		txid, _ := commitment.Hash()

		t.Log("txid : ", hex.EncodeToString(txid[:]))

		for _, input := range inputs {
			val, err := json.Marshal(input)
			if err != nil {
				t.Error("failed to marshal commitment with err ", err)
			}
			t.Log(string(val))
		}
		val, _ := json.Marshal(commitment)
		t.Log(string(val))

		commBytes, err := commitment.Marshal()
		if err != nil {
			t.Error("failed to serialize commitment with err ", err)

		}
		com := Commitment{}
		err = com.Unmarshal(commBytes)
		if err != nil {
			t.Error("failed to deserialize commitment with err ", err)

		}
		assert.Equal(t, commitment, com)

	})
	t.Run("TestCoinbaseCommitmentConsistency", func(t *testing.T) {
		testCases := []commitmentTest{
			{
				name: "bad commitment : non null input",
				c: Commitment{
					Inputs:  []Input{{Origin: crypto.SHA3Hash([]byte("coinbase"))}},
					Outputs: []Output{{Address: "clq123456!!!", Value: 99, Locktime: 1}},
				},

				err: true,
			}, {
				name: "bad commitment : empty validator null outputs",
				c: Commitment{
					Inputs:  []Input{{Origin: emptyHash}},
					Outputs: []Output{{Address: "clq123456!!!", Value: 99, Locktime: 1}},
				},

				err: true,
			},
		}
		for _, test := range testCases {

			ok, err := test.c.Validate(true)
			if ok || err == nil {
				t.Error("bad commitments should fail to valdiate")
			}
			_, err = test.c.Marshal()
			t.Log(err)
			if err != nil {
				t.Error("failed to serialize commitment")
			}

		}
	})

}
