// Package oc provides an implementation of a portable open contract format.
// This abstracts a large portion of the underlying objects that are necessary
// to generate/store/verify transaction either client-side or node-side.
package types

import (
	"bytes"
	"fmt"

	"github.com/fxamacker/cbor/v2"
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/pkg/leb128"
	"gitlab.com/cloq/go-cloq/protocol"
)

// OpenContract represents an open contract in the OpenContractSet.
// An open contract is one that is available for execution by a transaction.
// This is similar to a UTXO but considered first-class.
// A Transaction will reference contracts by their hashes and create new ones.
// The referenced contracts are removed from the OpenContractSet and the created
// ones are added.
// A OpenContract is composed of :
// - Contract :
// 	- Address
//	- Value
//	- Locktime
// - Inclusion Height H_k (useful to know used for validation)
// - Coinbase property
type OpenContract struct {
	_               struct{} `cbor:"omitempty,toarray"`
	Contract        Contract
	InclusionHeight uint64
	Coinbase        bool
}

// String implements pretty printing.
func (oc *OpenContract) String() string {
	return fmt.Sprintf("Contract : %s\n Height : %d\n Coinbase : %t\n", oc.Contract.String(), oc.Height(), oc.IsCoinbase())
}

// NewOpenContract creates a new instance of OpenContract.
// Each block we can generate a list of inclusion proofs based on the receiptsroot
// and a list of contracts created all share the same height.
func NewOpenContract(out Contract, height uint64, coinbase bool) *OpenContract {

	return &OpenContract{
		Contract:        out,
		InclusionHeight: height,
		Coinbase:        coinbase,
	}
}

// Height returns the inclusion height of the contract
func (oc *OpenContract) Height() uint64 {
	return oc.InclusionHeight
}

// IsCoinbase returns whether this open contract was created in a coinbase TX.
func (oc *OpenContract) IsCoinbase() bool {
	return oc.Coinbase
}

// Marshal implements serializationsfor OpenContracts
func (oc *OpenContract) Marshal() ([]byte, error) {
	return cbor.Marshal(oc)
}

// Unmarshal implements deserialization for OpenContracts
func (oc *OpenContract) Unmarshal(b []byte) error {
	return cbor.Unmarshal(b, oc)
}

// Hash returns the sha3 digest of a serialized oc.
// The OpenContractSet is a trie where the key's are a contract hash and the value
// is the opencontract's hash.
func (oc *OpenContract) Hash() (crypto.Hash, error) {
	var h crypto.Hash

	contractHash, err := oc.ContractHash()
	if err != nil {
		return h, err
	}

	var b = new(bytes.Buffer)

	_, err = b.Write(contractHash.Bytes())
	if err != nil {
		return h, err
	}
	heightBytes := leb128.FromUint64(oc.InclusionHeight)
	if heightBytes == nil {
		return h, err
	}
	_, err = b.Write(heightBytes)
	if err != nil {
		return h, err
	}
	return crypto.SHA3Hash(protocol.TagOpenContract, b.Bytes()), nil
}

// GetContract returns the oc's contract
func (oc *OpenContract) GetContract() Contract {
	return oc.Contract
}

// ContractHash returns the contract's hash H(TXID||INDEX||ADDRESS||VALUE||LOCKTIME)
func (oc *OpenContract) ContractHash() (crypto.Hash, error) {
	return oc.Contract.Hash()
}

// Reference returns the txid,index of the contract we hold.
func (oc *OpenContract) Reference() (crypto.Hash, uint32) {
	return oc.GetContract().TXID, oc.GetContract().Index
}
