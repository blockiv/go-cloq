package types

import (
	"github.com/fxamacker/cbor/v2"
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/pkg/bytesutil"
	"gitlab.com/cloq/go-cloq/pkg/merkle"
)

// Redeemer represents a list of arguments used to satisfy a validator's predicate.
type Redeemer [][]byte

// Witness represents a sequence of lists of arguments to each input's validator
// script.
type Witness struct {
	_         struct{} `cbor:"omitempty,toarray"`
	Redeemers []Redeemer
}

// Marshal implements the serialization interface using CBOR.
func (wit *Witness) Marshal() ([]byte, error) {
	return cbor.Marshal(wit)
}

// Unmarshal implements the deserialization interface using CBOR.
func (wit *Witness) Unmarshal(b []byte) error {
	return cbor.Unmarshal(b, wit)
}

// NewWitness creates a new instance of transaction witness data.
func NewWitness() Witness {
	return Witness{
		Redeemers: make([]Redeemer, 0),
	}
}

// Push appends a new witness list
func (wit *Witness) Push(args [][]byte) {
	wit.Redeemers = append(wit.Redeemers, args)
}

// Hash computes the merkle root hash of witness redeemers.
func (wit *Witness) Hash() crypto.Hash {

	var leaves = make([][]byte, 0, len(wit.Redeemers))
	for _, redeemer := range wit.Redeemers {

		redeemerRoot := merkle.Root(redeemer)
		leaves = append(leaves, redeemerRoot)
	}

	return bytesutil.ToBytes32(merkle.Root(leaves))
}
