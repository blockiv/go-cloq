package types

import (
	"encoding/json"
	"os"
	"testing"

	"github.com/fxamacker/cbor/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cloq/go-cloq/core/qvm/vm"
)

type res struct {
	success bool
	err     error
}

func TestTransactions(t *testing.T) {

	t.Run("TestTransactionJSON", func(t *testing.T) {
		transaction := genTransaction()
		jsonIndentTx, err := json.MarshalIndent(transaction, "", "  ")
		if err != nil {
			t.Error("marshal json failed with error ", err)
		}
		t.Log("New Transaction : ", string(jsonIndentTx))
		jsonTx, err := json.Marshal(transaction)
		if err != nil {
			t.Error("marshal json failed with error ", err)
		}
		tx := &Transaction{}
		err = json.Unmarshal(jsonTx, tx)
		if err != nil {
			t.Error("unmarshal json failed with error ", err)
		}
		assert.Equal(t, transaction, tx)
	})
	t.Run("TestTransactionExecution", func(t *testing.T) {
		transaction := genTransaction()
		in, _ := transaction.Inputs()

		_, err := transaction.ExecutedContracts()
		if err != nil {
			t.Fatal("failed to fetch executed contracts with error :", err)
		}
		_, err = transaction.CreatedContracts()
		if err != nil {
			t.Fatal("failed to fetch created contracts with error :", err)
		}
		_, err = transaction.CreatedContractsHash()
		if err != nil {
			t.Fatal("failed to fetch executed contracts hashes with error :", err)
		}
		toProcessChan := make(chan *Transaction, 1)
		toProcessChan <- transaction

		doneChan := make(chan res)

		go func(in chan *Transaction, out chan res) {
			var result res
			x := <-in
			result.success, result.err = x.Validate(false)
			t.Log("Validation result : ", result)
			out <- result
		}(toProcessChan, doneChan)
		res := <-doneChan
		if !res.success || res.err != nil {
			t.Error("transaction execution failed with error :", res.err)
		}

		for idx, input := range in {

			txvm := vm.New(1000, input.Validator, transaction.Witness.Redeemers[idx])
			success, err := txvm.Run(os.Stdout)
			if success != true || err != nil {
				t.Errorf("valid tx failed to execute with status : %v, error : %v", success, err)
			}
			t.Log("successful validation of transaction : ", success, err)
		}
	})

	t.Run("TestTransactionSerialization", func(t *testing.T) {
		transaction := genTransaction()
		txBytes, err := transaction.Marshal()
		if err != nil {
			t.Error("transaction serialization failed with error :", err)
		}
		tx := &Transaction{}
		err = tx.Unmarshal(txBytes)
		if err != nil {
			t.Error("transaction deserialization failed with error :", err)
		}
		assert.Equal(t, transaction, tx)

		outputs, err := transaction.CreatedContracts()
		if err != nil {
			t.Error("failed to fetch new created contracts with error :", err)
		}
		for _, output := range outputs {
			outBytes, err := output.Marshal()
			if err != nil {
				t.Error("contract serialization failed with error :", err)
			}
			contract := Contract{}
			err = contract.Unmarshal(outBytes)
			if err != nil {
				t.Error("contract deserialization failed with error :", err)
			}
			if !assert.Equal(t, output, contract) {
				t.Error("transaction non consistent")
			}
		}
	})
	t.Run("TestCBOREncodingConsistency", func(t *testing.T) {
		transaction := genTransaction()
		txBytes, err := cbor.Marshal(transaction)
		if err != nil {
			t.Error("transaction serialization failed with error :", err)
		}
		tx := &Transaction{}
		err = cbor.Unmarshal(txBytes, tx)
		if err != nil {
			t.Error("transaction deserialization failed with error :", err)
		}
		assert.Equal(t, transaction, tx)

		outputs, err := transaction.CreatedContracts()
		if err != nil {
			t.Error("failed to fetch new created contracts with error :", err)
		}
		for _, output := range outputs {
			outBytes, err := output.Marshal()
			if err != nil {
				t.Error("contract serialization failed with error :", err)
			}
			contract := Contract{}
			err = contract.Unmarshal(outBytes)
			if err != nil {
				t.Error("contract deserialization failed with error :", err)
			}
			if !assert.Equal(t, output, contract) {
				t.Error("transaction non consistent")
			}
		}
	})
}
