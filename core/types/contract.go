package types

import (
	"fmt"

	"github.com/fxamacker/cbor/v2"
	"gitlab.com/cloq/go-cloq/protocol"

	"gitlab.com/cloq/go-cloq/core/crypto"
)

// An Contract is essentially the coins in the state.
// An Contract holds the necessary information to spend it and generate proofs
// of its creation and current state (SPENT/UNSPENT)

// Contract is an output with tx information
type Contract struct {
	_      struct{}    `cbor:"omitempty,toarray"`
	TXID   crypto.Hash `json:"txid"`
	Index  uint32      `json:"index"`
	Output Output      `json:"output"`
}

// String implements stringer
func (out *Contract) String() string {

	return fmt.Sprintf("txid : %s\nindex : %d\naddress : %s\nvalue : %d\nlocktime : %d\n",
		out.TXID, out.Index, out.Output.Address, out.Output.Value, out.Output.Locktime)
}

// NewContract creates a new contract
func NewContract(txid crypto.Hash, idx uint32, out Output) Contract {
	return Contract{
		TXID:   txid,
		Index:  idx,
		Output: out,
	}
}

// Marshal implements serialization interface over contract
func (out *Contract) Marshal() ([]byte, error) {
	return cbor.Marshal(out)
}

// Unmarshal implements deserialization interface over contracts
func (out *Contract) Unmarshal(b []byte) error {
	return cbor.Unmarshal(b, out)
}

// Hash returns SHA3 digest of an contract.
// This represents the reference used as input.
// You can notice that txid+index are unique for each transaction output.
// They add sufficient entropy so as not have collisions over inputs.
func (out *Contract) Hash() (crypto.Hash, error) {

	outBytes, err := out.Marshal()
	if err != nil {
		return crypto.EmptyHash, err
	}

	return crypto.SHA3Hash(protocol.TagContract, outBytes), nil
}

// Address returns an contract address (encoded)
func (out *Contract) Address() string {
	return out.Output.Address
}

// Value returns an contract's value
func (out *Contract) Value() uint64 {
	return out.Output.Value
}

// Locktime returns an contract locktime
func (out *Contract) Locktime() uint64 {
	return out.Output.Locktime
}
