package types

import (
	"bytes"
	"errors"

	"github.com/fxamacker/cbor/v2"
	"gitlab.com/cloq/go-cloq/protocol"

	"gitlab.com/cloq/go-cloq/core/crypto"
)

var (
	// soft limit on transaction sizes
	maxTxWeight = 4_000_000
)

var (
	// ErrMalformedTransaction is signaled whenever a validation check fails
	ErrMalformedTransaction = errors.New("bad transaction serialization")
)

// Transaction represents a single batch of state transitions.
// A sequence of inputs is consumed to create a new sequence of outputs.
// Each transaction may contain audit proofs that the inputs it's trying
// to spend are present in the state and marked UNSPENT.
// Proofs field is not necessary.
type Transaction struct {
	_          struct{}   `cbor:"omitempty,toarray"`
	Version    uint32     `json:"version"`
	BranchID   uint32     `json:"branchid"`
	Expiry     uint64     `json:"expiry"`
	Fees       uint64     `json:"fees"`
	Commitment Commitment `json:"commitment"`
	Witness    Witness    `json:"witness"`
}

// NewTransaction creates a new transaction instance
func NewTransaction(version, branchid uint32, expiry, fees uint64, commitment Commitment, wit Witness) *Transaction {

	return &Transaction{
		Version:    version,
		BranchID:   branchid,
		Expiry:     expiry,
		Fees:       fees,
		Commitment: commitment,
		Witness:    wit,
	}
}

// Marshal implements a serialization interface
func (tx *Transaction) Marshal() ([]byte, error) {
	return cbor.Marshal(tx)
}

// Unmarshal implements a deserialization interface
func (tx *Transaction) Unmarshal(b []byte) error {
	return cbor.Unmarshal(b, tx)
}

// Hash returns the whole transaction hash (wtxid)
func (tx *Transaction) Hash() (crypto.Hash, error) {
	witHash := tx.Witness.Hash()
	txid, err := tx.TXID()
	if err != nil {
		return crypto.EmptyHash, err
	}
	return crypto.SHA3Hash(protocol.TagWitness, txid.Bytes(), witHash.Bytes()), nil
}

// TXID returns the transaction identifier
func (tx *Transaction) TXID() (crypto.Hash, error) {
	return tx.Commitment.Hash()
}

// WTXID returns the transaction||witness hash.
func (tx *Transaction) WTXID() (crypto.Hash, error) {
	return tx.Hash()
}

// GetVersion returns the transaction version.
func (tx *Transaction) GetVersion() uint32 {
	return tx.Version
}

// GetExpiry returns the expiration block of the transaction.
func (tx *Transaction) GetExpiry() uint64 {
	return tx.Expiry
}

// GetBranchID returns the transaction's branch id.
func (tx *Transaction) GetBranchID() uint32 {
	return tx.BranchID
}

// GetFee returns the transaction fee
func (tx *Transaction) GetFee() uint64 {
	return tx.Fees
}

// Validate a transaction runs semantic checks.
// Commitment checks :
// No duplicate inputs.
// No null inputs or outputs.
// Coinbase rules.
func (tx *Transaction) Validate(isCoinbase bool) (bool, error) {

	ok, err := tx.Commitment.Validate(isCoinbase)

	if !ok || err != nil {
		return false, err
	}

	txid, err := tx.TXID()

	if err != nil {
		return false, err
	}
	wtxid, err := tx.WTXID()

	if err != nil {
		return false, err
	}

	if bytes.Equal(txid[:], wtxid[:]) {
		return false, errors.New("duplicate witness hash")
	}

	if tx.Weight() > maxTxWeight || tx.Weight() == -1 {
		return false, errors.New("transaction size is higher than protocol limit")
	}
	return true, nil
}

// Weight is the size of a serialized commitment
func (tx *Transaction) Weight() int {

	txBytes, err := tx.Commitment.Marshal()
	if err != nil {
		return -1
	}
	return len(txBytes)

}

// CreatedContracts returns the list of contracts ! NOT OUTPUTS ! created in a tx
func (tx *Transaction) CreatedContracts() ([]Contract, error) {

	txid, err := tx.TXID()

	if err != nil {
		return nil, err
	}
	contractsCreated := make([]Contract, 0, len(tx.Commitment.Outputs))

	for idx, output := range tx.Commitment.Outputs {
		contract := NewContract(txid, uint32(idx), output)
		contractsCreated = append(contractsCreated, contract)
	}

	return contractsCreated, nil
}

// CreatedContractsHash returns the list of hashes of the contracts created
func (tx *Transaction) CreatedContractsHash() ([][]byte, error) {

	created, err := tx.CreatedContracts()
	if err != nil {
		return nil, err
	}

	var createdHashes = make([][]byte, 0, len(created))

	for _, out := range created {

		outHash, err := out.Hash()
		if err != nil {
			return nil, err
		}

		createdHashes = append(createdHashes, outHash.Bytes())
	}
	return createdHashes, nil
}

// Inputs returns a copy list of spent inputs
func (tx *Transaction) Inputs() ([]Input, error) {
	spentInputs := make([]Input, len(tx.Commitment.Inputs))
	copy(spentInputs, tx.Commitment.Inputs)
	return spentInputs, nil
}

// Outputs returns a copy list of created outputs
func (tx *Transaction) Outputs() ([]Output, error) {
	createdOutputs := make([]Output, len(tx.Commitment.Inputs))
	copy(createdOutputs, tx.Commitment.Outputs)
	return createdOutputs, nil
}

// OutputHashes returns the list of hashes of the outputs.
func (tx *Transaction) OutputHashes() ([][]byte, error) {

	createdOutputs, err := tx.Outputs()
	if err != nil {
		return nil, err
	}

	var createdHashes = make([][]byte, 0, len(createdOutputs))

	for _, out := range createdOutputs {

		outHash, err := out.Hash()
		if err != nil {
			return nil, err
		}

		createdHashes = append(createdHashes, outHash.Bytes())
	}
	return createdHashes, nil
}

// ExecutedContracts returns a list of executed contracts hashes
func (tx *Transaction) ExecutedContracts() ([][]byte, error) {

	executedHashes := make([][]byte, 0, len(tx.Commitment.Inputs))

	for _, in := range tx.Commitment.Inputs {
		executedHashes = append(executedHashes, in.Origin.Bytes())
	}

	return executedHashes, nil
}

// Size returns the transaction size (without the witness) in bytes
func (tx *Transaction) Size() (int, error) {
	buf, err := tx.Commitment.Marshal()
	return len(buf), err
}
