# CloqVM

## TODO

- [x] Wrap errors within script compilation.
- [x] Drop the naked jmp opcode.
- [x] Correctly parse predicates.
- [ ] Correctly parse if/else conidtions to jumpif bytecode.
