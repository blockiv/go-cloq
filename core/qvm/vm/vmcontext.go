package vm

import "gitlab.com/cloq/go-cloq/core/crypto"

// Context represents the execution context for the virtual machine.
// It's loaded with the virtual machine at runtime and stores introspective
// values required for introspection opcodes and upgrades.
type Context struct {

	// VM data
	VMVersion uint32   // virtual machine version
	Code      []byte   // validator script
	Args      [][]byte // witness data

	// Transaction data
	TxVersion    uint32   // version of the transaction we're executing
	TXID         []byte   // transaction hash
	OutputCount  int      // number of outputs in tx
	OutputHashes [][]byte // outputhashes
	// Accessible chain data
	ContractHeight uint64 // inclusion height of the contract we're executing
	ChainHeight    uint64 // current chain-tip height
	ChainHash      []byte

	// Contextual data
	TxSigHash func() []byte // Compute a signature for this transaction
}

// NewVMContext creates a new instance of VM Context
func NewVMContext(version, txVer uint32, txid crypto.Hash, contractHeight, chainHeight uint64, chainHash crypto.Hash) *Context {
	return &Context{
		VMVersion:      version,
		TxVersion:      txVer,
		TXID:           txid[:],
		ContractHeight: contractHeight,
		ChainHeight:    chainHeight,
		ChainHash:      chainHash[:],
	}
}

// LoadTransactionData loads the transaction data
func (context *Context) LoadTransactionData(outputCount int, outputHashes [][]byte) {
	context.OutputCount = outputCount
	context.OutputHashes = outputHashes
}
