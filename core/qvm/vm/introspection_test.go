package vm

import (
	"testing"

	"gitlab.com/cloq/go-cloq/core/crypto"
	stack "gitlab.com/cloq/go-cloq/core/qvm/vm/internal"
	"gitlab.com/cloq/go-cloq/pkg/leb128"
)

func TestIntrospectionOPS(t *testing.T) {

	type testCase struct {
		op      OP
		startVM *VirtualMachine
		wantErr error
		wantVM  *VirtualMachine
	}
	testcases := []testCase{

		{
			op: OPMintime,
			startVM: &VirtualMachine{
				evalStack: stack.New([][]byte{leb128.FromInt64(130)}),
				context:   NewVMContext(0, 0, crypto.EmptyHash, 57604, 57790, crypto.EmptyHash),
				maxMemory: 256,
			},
			wantErr: nil,
			wantVM: &VirtualMachine{
				evalStack: stack.New([][]byte{{1}}),
				maxMemory: 256,
			},
		},

		{
			op: OPMintime,
			startVM: &VirtualMachine{
				evalStack: stack.New([][]byte{leb128.FromInt64(100)}),
				context:   NewVMContext(0, 0, crypto.EmptyHash, 57604, 57690, crypto.EmptyHash),
				maxMemory: 256,
			},
			wantErr: nil,
			wantVM: &VirtualMachine{
				evalStack: stack.New([][]byte{{0}}),
				maxMemory: 256,
			},
		},

		{
			op: OPCheckOutput,
			startVM: &VirtualMachine{
				evalStack: stack.New([][]byte{decodeHex("98ca18e86a3bfd179505678aed34b540e6ad34f78f824fe31adb3126c8f551b0"), leb128.FromInt64(0)}),
				context:   NewVMContext(0, 0, crypto.EmptyHash, 34, 50, crypto.EmptyHash),
				maxMemory: 512,
			},
			wantErr: nil,
			wantVM: &VirtualMachine{
				evalStack: stack.New([][]byte{{1}}),
				maxMemory: 512,
			},
		},
	}

	for idx, test := range testcases {

		test.startVM.context.OutputHashes = append(test.startVM.context.OutputHashes, decodeHex("98ca18e86a3bfd179505678aed34b540e6ad34f78f824fe31adb3126c8f551b0"))
		test.startVM.context.OutputCount = 1

		err := opTable[test.op].callback(test.startVM)
		t.Log("case ", idx, "callback error : ", err)
		if err != test.wantErr {
			t.Errorf("case %d, op %s: got err = %v want %v", idx, test.op.String(), err, test.wantErr)
			continue
		}
		if test.wantErr != nil {
			continue
		}

		if !vmEqual(test.startVM, test.wantVM) {
			t.Errorf("case %d, op %s: unexpected vm result\n\tgot:  %+v\n\twant: %+v\n", idx, test.op.String(), test.startVM.evalStack, test.wantVM.evalStack)
		}

	}

}
