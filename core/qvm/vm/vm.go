package vm

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"io"

	stack "gitlab.com/cloq/go-cloq/core/qvm/vm/internal"
	"gitlab.com/cloq/go-cloq/core/script"
)

// Result represents an execution result
type Result struct {
	success bool
	err     error
	name    string
}

const (
	// MaxProgramSize is limited at 4kb (opcodes and data elements)
	MaxProgramSize = 4096
	// MaxPushDataSize is the max number of bytes that can be pushed at once
	MaxPushDataSize = 256
	// MaxPubKeysPerMultiSig is the max number of co-signers in a multisig OP
	MaxPubKeysPerMultiSig = 128

	// MaxStackDepth is the limit on how many items can be pushed on the stack
	MaxStackDepth = 1024

	// DefaultCost is 1 unit per simple opcode
	DefaultCost = 1
	// StackCost is 8 per item pushed
	StackCost = 8
	// HashCost is 1*byte to be hashed
	HashCost = 1
	// JumpCost is 64 per jump instruction executed.
	JumpCost = 64
	// SigCost is 256 per signature to verify
	SigCost = 256
)

// VirtualMachine implements the VM0 from the spec
type VirtualMachine struct {
	program   []byte // a program is a sequence of bytes
	ip        uint32 // instruction pointer the current instruction
	nextIP    uint32 // pointer to the next instruction
	maxMemory int    // max memory size
	size      int    // vm size (number of elements in the evalStack + altStack)

	memory []byte // memory used to store parsed from opcodes

	evalStack stack.Stack // main execution stack
	altStack  stack.Stack // alternative execution stack

	// context of the transaction's execution
	context *Context
}

// New creates a new virtual machine
func New(mem int, program []byte, args [][]byte) *VirtualMachine {

	return &VirtualMachine{
		program:   program,
		maxMemory: mem,
		evalStack: stack.New(args),
	}
}

// LoadContext loads the current transaction context into the VM.
func (vm *VirtualMachine) LoadContext(context *Context) error {

	vm.context = context
	return nil
}

// Reset the virtual machine by clearing memory and execution stacks.
func (vm *VirtualMachine) Reset() {
	vm.program = nil
	vm.evalStack = stack.New(nil)
	vm.altStack = stack.New(nil)
	vm.SetLimit(5000)
}

// AddProgram adds a new program to the VM
func (vm *VirtualMachine) AddProgram(prog []byte) error {
	if len(prog) > MaxProgramSize {
		return ErrMaxProgramSizeExceeded
	}
	vm.program = prog
	return nil
}

// AddArguments adds arguments to the evalstack for execution
func (vm *VirtualMachine) AddArguments(args [][]byte) error {

	for _, arg := range args {
		if len(arg) > MaxPushDataSize {
			return ErrMaxDataSizeExceeded
		}
		vm.evalStack.PushBytes(arg)
	}
	return nil
}

// SetLimit sets the memory limit
func (vm *VirtualMachine) SetLimit(lim int) {
	vm.maxMemory = lim
}

// String outputs program opcodes
func (vm *VirtualMachine) String() string {
	return fmt.Sprintln(hex.EncodeToString(vm.program))
}

// Run the Virtual Machine and return an error and execution status.
// An idiomatic way to use it :
// ok,err := vm.Run()
func (vm *VirtualMachine) Run(tracer io.Writer) (bool, error) {

	var err error
	for vm.ip < uint32(len(vm.program)) { // handle vm.ip updates in step
		err = vm.execute(tracer)
		if err != nil {
			return false, err
		}
	}
	return !vm.failed(), nil
}

// Eval stack operations
func (vm *VirtualMachine) push(b []byte) error {

	if vm.evalStack.Depth() > MaxStackDepth {
		return ErrMaxStackDepthExceeded
	}
	if len(b) > MaxPushDataSize {
		return ErrMaxDataSizeExceeded
	}

	cost := len(b)

	err := vm.applyCost(cost)
	if err != nil {
		return err
	}
	vm.size++
	return vm.evalStack.PushBytes(b)

}
func (vm *VirtualMachine) pop() ([]byte, error) {
	top, err := vm.evalStack.PopBytes()
	if err != nil {
		return nil, err
	}
	cost := StackCost + len(top)
	vm.maxMemory += cost
	vm.size--
	return top, nil
}

func (vm *VirtualMachine) pushInt(val int64) error {
	return vm.push(FromInt64(val))
}

func (vm *VirtualMachine) popInt() (int64, error) {
	top, err := vm.pop()
	if err != nil {
		return -1, err
	}
	return ToInt64(top)
}

func (vm *VirtualMachine) pushBool(b bool) error {
	return vm.push(FromBool(b))
}

func (vm *VirtualMachine) popBool() (bool, error) {
	top, err := vm.pop()
	if err != nil {
		return false, err
	}
	return ToBool(top), nil
}

// Given a cost substract it from  memory
func (vm *VirtualMachine) applyCost(n int) error {
	if n > vm.maxMemory {
		return ErrMaxMemoryExceeded
	}
	vm.maxMemory -= n
	return nil
}

// failed returns true if the stack is empty or the top
// item is false
func (vm *VirtualMachine) failed() bool {

	if vm.evalStack.Depth() == 0 {
		return true
	}
	top, _ := vm.evalStack.PeekBool(0)
	if !top {
		return true
	}
	return false
}

func (vm *VirtualMachine) execute(tracer io.Writer) error {
	inst, err := script.ParseOP(vm.program, vm.ip)
	if err != nil {
		return err
	}

	vm.nextIP = vm.ip + inst.Length

	if tracer != nil {
		opname := inst.Opcode.String()
		fmt.Fprintf(tracer, "ip : %d | memory : %d | op : %s", vm.ip, vm.maxMemory, opname)
		if len(inst.Data) > 0 {
			fmt.Fprintf(tracer, " | data : %x", inst.Data)
		}
		fmt.Fprint(tracer, "\n")
	}

	vm.memory = inst.Data
	err = opTable[inst.Opcode].callback(vm)
	if err != nil {
		return err
	}
	vm.ip = vm.nextIP

	if tracer != nil {
		fmt.Fprintf(tracer, vm.evalStack.String())
	}

	return nil
}
func vmEqual(vm1 *VirtualMachine, vm2 *VirtualMachine) bool {

	if !bytes.Equal(vm1.memory, vm2.memory) {
		return false

	}

	if !vm1.evalStack.Equal(vm2.evalStack) {
		return false
	}

	if !vm1.evalStack.Equal(vm2.evalStack) {
		return false
	}

	if !bytes.Equal(vm1.program, vm2.program) {
		return false
	}

	return true
}
