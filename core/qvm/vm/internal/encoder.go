package stack

import (
	"gitlab.com/cloq/go-cloq/pkg/leb128"
)

// FromInt64 encodes a signed 64 bit integer to a bytearray using leb128 encoding
func FromInt64(n int64) []byte {

	return leb128.FromInt64(n)
}

// ToInt64 decodes a byte array to an int64 using leb128
func ToInt64(b []byte) (int64, error) {

	return leb128.ToInt64(b)
}

// FromUint64 : encodes a uint64 to a bytearray using leb128 encoding
func FromUint64(value uint64) []byte {

	return leb128.FromUint64(value)
}

// ToUint64 decodes a LEB128 Encoded Uint64
func ToUint64(b []byte) (uint64, error) {

	return leb128.ToUint64(b)
}

// ToBool : given a bytearray return true if they have non zero bytes
// Example :  -> false ; "000000" ->
func ToBool(buf []byte) bool {

	for _, b := range buf {
		if b != 0 {
			return true
		}

	}

	return false
}

// FromBool converts a boolean to a byte array
func FromBool(b bool) []byte {
	if b {
		return []byte{1}
	}
	return []byte{0}
}

func min(a int, b int) int32 {

	if a >= b {
		return int32(b)
	}

	return int32(a)
}
