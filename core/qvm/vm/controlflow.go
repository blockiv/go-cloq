package vm

import (
	"encoding/binary"
)

func opVerify(vm *VirtualMachine) error {
	err := vm.applyCost(DefaultCost)
	if err != nil {
		return err
	}
	b, err := vm.evalStack.PopBool()
	if err != nil {
		return err
	}
	if b {
		return nil
	}
	return ErrVerifyFailed
}

func opHalt(vm *VirtualMachine) error {
	err := vm.applyCost(DefaultCost)
	if err != nil {
		return err
	}
	return ErrHalt
}

func opJumpIf(vm *VirtualMachine) error {
	err := vm.applyCost(JumpCost)
	if err != nil {
		return err
	}
	b, err := vm.evalStack.PopBool()
	if err != nil {
		return err
	}
	if b {
		address := binary.LittleEndian.Uint32(vm.memory)
		vm.nextIP = address
	}

	return nil
}
