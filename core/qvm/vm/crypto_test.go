package vm

import (
	"crypto/ed25519"
	"crypto/rand"
	"encoding/hex"
	"testing"

	"gitlab.com/cloq/go-cloq/core/crypto"

	stack "gitlab.com/cloq/go-cloq/core/qvm/vm/internal"
	"golang.org/x/crypto/sha3"
)

func decodeHex(s string) []byte {
	b, _ := hex.DecodeString(s)
	return b
}

func asHash(s string) crypto.Hash {
	var h crypto.Hash
	b := decodeHex(s)
	copy(h[:], b)
	return h
}

func TestParamGen(t *testing.T) {
	pk, sk, _ := ed25519.GenerateKey(rand.Reader)
	testmsg := sha3.Sum256(pk)
	t.Log(hex.EncodeToString(pk), hex.EncodeToString(testmsg[:]), hex.EncodeToString(ed25519.Sign(sk, testmsg[:])))
}
func TestHashOP(t *testing.T) {

	type testCase struct {
		op      OP
		startVM *VirtualMachine
		wantErr error
		wantVM  *VirtualMachine
	}
	testcases := []testCase{
		{
			op: OPSHA3,
			startVM: &VirtualMachine{
				evalStack: stack.New([][]byte{[]byte("helloworld")}),
				maxMemory: 1000,
			},
			wantErr: nil,
			wantVM: &VirtualMachine{
				evalStack: stack.New([][]byte{decodeHex("92dad9443e4dd6d70a7f11872101ebff87e21798e4fbb26fa4bf590eb440e71b")}),
				maxMemory: 1000 - 64,
			},
		},
		{
			op: OPSHA256,
			startVM: &VirtualMachine{
				evalStack: stack.New([][]byte{[]byte("helloworld")}),
				maxMemory: 1000,
			},
			wantErr: nil,
			wantVM: &VirtualMachine{
				evalStack: stack.New([][]byte{decodeHex("936a185caaa266bb9cbe981e9e05cb78cd732b0b3280eb944412bb6f8f8f07af")}),
				maxMemory: 1000 - 64,
			},
		},
		{
			op: OPBlake2,
			startVM: &VirtualMachine{
				evalStack: stack.New([][]byte{[]byte("helloworld")}),
				maxMemory: 1000,
			},
			wantErr: nil,
			wantVM: &VirtualMachine{
				evalStack: stack.New([][]byte{decodeHex("3c228306552177f5a304cb12a5b5e60897f2f486b64671afdccf0f8dd9410cbd")}),
				maxMemory: 1000 - 64,
			},
		}, {
			op: OPSHA3,
			startVM: &VirtualMachine{
				evalStack: stack.New([][]byte{[]byte("helloworld")}),
				maxMemory: 0,
			},
			wantErr: ErrMaxMemoryExceeded,
			wantVM: &VirtualMachine{
				evalStack: stack.New([][]byte{decodeHex("92dad9443e4dd6d70a7f11872101ebff87e21798e4fbb26fa4bf590eb440e71b")}),
				maxMemory: 0,
			},
		},
	}

	for idx, test := range testcases {
		err := opTable[test.op].callback(test.startVM)

		if err != test.wantErr {
			t.Errorf("case %d, op %s: got err = %v want %v", idx, test.op.String(), err, test.wantErr)
			continue
		}
		if test.wantErr != nil {
			continue
		}

		if !vmEqual(test.startVM, test.wantVM) {
			t.Errorf("case %d, op %s: unexpected vm result\n\tgot:  %+v\n\twant: %+v\n", idx, test.op.String(), test.startVM.evalStack, test.wantVM.evalStack)
		}

	}
}
func TestSigOps(t *testing.T) {

	type testCase struct {
		op      OP
		startVM *VirtualMachine
		wantErr error
		wantVM  *VirtualMachine
	}
	testcases := []testCase{

		{
			op: OPCheckSig,
			startVM: &VirtualMachine{
				evalStack: stack.New([][]byte{decodeHex("ea76a0bcee1f285d1d53a566297fddf8c954d02e722ed6160022ec80e3a691fb56e3383b52256d65fd9d828d0d5da4deb56684152b3ab8b3948aaf71b1208f01"), decodeHex("3526218466de77a9ddbc35f7050d7a506c845508e7e1e5e13d44868cd326d002"), decodeHex("b0a20339bb05f8f734b2121a84f7672f0bccff4f20eadf48ff81b34a82ef010c"), {byte(SigSchemeED25519)}}),
				maxMemory: 512,
			},
			wantErr: nil,
			wantVM: &VirtualMachine{
				evalStack: stack.New([][]byte{{1}}),
				maxMemory: 256,
			},
		},

		{
			op: OPCheckSigTx,
			startVM: &VirtualMachine{
				evalStack: stack.New([][]byte{decodeHex("ea76a0bcee1f285d1d53a566297fddf8c954d02e722ed6160022ec80e3a691fb56e3383b52256d65fd9d828d0d5da4deb56684152b3ab8b3948aaf71b1208f01"), decodeHex("b0a20339bb05f8f734b2121a84f7672f0bccff4f20eadf48ff81b34a82ef010c"), {byte(SigSchemeED25519)}}),
				maxMemory: 512,
				context:   NewVMContext(1, 1, asHash("3526218466de77a9ddbc35f7050d7a506c845508e7e1e5e13d44868cd326d002"), 0, 0, crypto.EmptyHash),
			},
			wantErr: nil,
			wantVM: &VirtualMachine{
				evalStack: stack.New([][]byte{{1}}),
				maxMemory: 256,
			},
		},

		{
			op: OPCheckSig,
			startVM: &VirtualMachine{
				evalStack: stack.New([][]byte{decodeHex("ea76a0bcee1f285d1d53a566297fddf8c954d02e722ed6160022ec80e3a691fb56e3383b52256d65fd9d828d0d5da4deb56684152b3ab8b3948aaf71b1208f01"), decodeHex("3526218466de77a9ddbc35f7050d7a506c845508e7e1e5e13d44868cd326d002"), decodeHex("b0a20339bb05f8f734b2121a84f7672f0bccff4f20eadf48ff81b34a82ef010c"), {byte(SigSchemeED25519)}}),
				maxMemory: 512,
			},
			wantErr: nil,
			wantVM: &VirtualMachine{
				evalStack: stack.New([][]byte{{1}}),
				maxMemory: 256,
			},
		},

		{
			op: OPCheckSig,
			startVM: &VirtualMachine{
				evalStack: stack.New([][]byte{decodeHex("ea76a0bcee1f285d1d53a566297fddf8c954d02e722ed6160022ec80e3a691fb56e3383b52256d65fd9d828d0d5da4deb56684152b3ab8b3948aaf71b1208f01"), decodeHex("3526218466de77a9ddbc35f7050d7a506c845508e7e1e5e13d44868cd326d002"), decodeHex("b0a20339bb05f8f734b2121a84f7672f0bccff4f20eadf48ff81b34a82ef010c"), {byte(SigSchemeED25519)}}),
				maxMemory: 255,
			},
			wantErr: ErrMaxMemoryExceeded,
			wantVM: &VirtualMachine{
				evalStack: stack.New([][]byte{{1}}),
				maxMemory: 256,
			},
		},
	}

	for idx, test := range testcases {
		err := opTable[test.op].callback(test.startVM)
		t.Log("case ", idx, "callback error : ", err)
		if err != test.wantErr {
			t.Errorf("case %d, op %s: got err = %v want %v", idx, test.op.String(), err, test.wantErr)
			continue
		}
		if test.wantErr != nil {
			continue
		}

		if !vmEqual(test.startVM, test.wantVM) {
			t.Errorf("case %d, op %s: unexpected vm result\n\tgot:  %+v\n\twant: %+v\n", idx, test.op.String(), test.startVM.evalStack, test.wantVM.evalStack)
		}

	}

}
