package vm

import (
	"bytes"
	"errors"
	"fmt"
	"math"
)

// opMintime pops an integer from the stack representing minimum number
// of blocks before this contract can be executed.
func opMintime(vm *VirtualMachine) error {

	inclusionHeight := vm.context.ContractHeight
	currentHeight := vm.context.ChainHeight

	currentDepth := currentHeight - inclusionHeight

	depthThreshold, err := vm.popInt()
	if err != nil {
		return err
	}

	if depthThreshold <= 0 || depthThreshold >= math.MaxInt32 {
		return errors.New("contract threshold out of range ")
	}

	if currentDepth > uint64(depthThreshold) {
		return vm.pushBool(true)
	}
	return vm.pushBool(false)
}

// opMaxtime pops an integer from the stack representing maximum number
// of blocks before this contract is burned (can't be spent again).
func opMaxtime(vm *VirtualMachine) error {

	inclusionHeight := vm.context.ContractHeight
	currentHeight := vm.context.ChainHeight

	currentDepth := currentHeight - inclusionHeight

	depthThreshold, err := vm.popInt()
	if err != nil {
		return err
	}

	if depthThreshold <= 0 || depthThreshold >= math.MaxInt32 {
		return errors.New("contract threshold out of range ")
	}

	if currentDepth < uint64(depthThreshold) {
		return vm.pushBool(true)
	}
	return vm.pushBool(false)

}

// opCheckOutputHash
func opCheckOutputHash(vm *VirtualMachine) error {

	outputIndex, err := vm.popInt()
	if err != nil {
		return err
	}
	fmt.Println(outputIndex)

	if outputIndex < 0 || outputIndex >= int64(vm.context.OutputCount) {
		return errors.New("output index out of range")
	}

	outputHash := vm.context.OutputHashes[outputIndex]

	wantedOutputHash, err := vm.pop()

	if err != nil {
		return err
	}

	return vm.pushBool(bytes.Equal(wantedOutputHash, outputHash[:]))
}
