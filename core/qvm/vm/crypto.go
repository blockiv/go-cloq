package vm

import (
	"crypto/ed25519"
	"crypto/sha256"
	"errors"

	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/crypto/schnorr"
	"golang.org/x/crypto/blake2b"

	"golang.org/x/crypto/sha3"
)

var (
	// ErrBadSigScheme is returned when the sigscheme byte is unknown
	ErrBadSigScheme = errors.New("bad sigscheme detected")
)

// SigHash is a one byte enum representing the data covered by the signature
// when parsing signatures the last byte is dedicated to the SigHash flag.
// The behavior is different from Bitcoin.
type SigHash byte

// SigScheme represents one of the signature algorithms supported by the Virtual Machine.
type SigScheme byte

const (
	// SigHashUnknown is for signatures that end with a 0 byte (longer than their predefined lengths).
	SigHashUnknown SigHash = 1
	// SigHashAll the signature is checked against TXID (covers all inputs and outputs)
	SigHashAll SigHash = 2
	// SigHashSingle the signature, part of a witness field at index (i), covers H(Input[i]||Output[i])
	SigHashSingle SigHash = 3

	// SigSchemeED25519 is for the ed25519 signature algorithm.
	SigSchemeED25519 SigScheme = 0x81
	// SigSchemeSchnorr is for the schnorrkel signature algorithm.
	SigSchemeSchnorr SigScheme = 0x82
)

func opSHA3(vm *VirtualMachine) error {

	data, err := vm.pop()
	if err != nil {
		return err
	}
	err = vm.applyCost(HashCost * len(data))
	if err != nil {
		return err
	}
	x := sha3.Sum256(data)
	return vm.push(x[:])
}

func opSHA256(vm *VirtualMachine) error {

	data, err := vm.pop()
	if err != nil {
		return err
	}
	err = vm.applyCost(HashCost * len(data))
	if err != nil {
		return err
	}
	x := sha256.Sum256(data)
	return vm.push(x[:])
}

func opBLAKE2(vm *VirtualMachine) error {

	data, err := vm.pop()
	if err != nil {
		return err
	}
	err = vm.applyCost(HashCost * len(data))
	if err != nil {
		return err
	}
	x := blake2b.Sum256(data)
	return vm.push(x[:])
}

func opTXID(vm *VirtualMachine) error {

	return vm.push(vm.context.TXID[:])
}

func opCheckSigTx(vm *VirtualMachine) error {

	txid := vm.context.TXID
	err := vm.applyCost(SigCost)
	if err != nil {
		return err
	}
	scheme, err := vm.pop()
	if err != nil {
		return err
	}

	if len(scheme) != 1 {
		return ErrBadSigScheme
	}
	sigscheme := SigScheme(scheme[0])
	pubkey, err := vm.pop()
	if err != nil {
		return err
	}
	sig, err := vm.pop()
	if err != nil {
		return err
	}

	switch sigscheme {
	case SigSchemeED25519:
		success, err := checkSigEd25519(pubkey, txid[:], sig)
		if err != nil {
			return err
		}
		return vm.pushBool(success)
	case SigSchemeSchnorr:
		success, err := checkSigSchnorrkel(pubkey, txid[:], sig)
		if err != nil {
			return err
		}
		return vm.pushBool(success)
	default:
		return vm.pushBool(false)
	}

}

func opCheckSig(vm *VirtualMachine) error {
	err := vm.applyCost(SigCost)
	if err != nil {
		return err
	}
	scheme, err := vm.pop()
	if err != nil || len(scheme) != 1 {
		return err
	}
	sigscheme := SigScheme(scheme[0])

	pubkey, err := vm.pop()
	if err != nil {
		return err
	}
	msg, err := vm.pop()

	if len(msg) != 32 {
		return ErrBadMessageSize
	}
	if err != nil {
		return err
	}
	sig, err := vm.pop()
	if err != nil {
		return err
	}

	switch sigscheme {
	case SigSchemeED25519:
		success, err := checkSigEd25519(pubkey, msg, sig)
		if err != nil {
			return err
		}
		return vm.pushBool(success)
	case SigSchemeSchnorr:
		success, err := checkSigSchnorrkel(pubkey, msg, sig)
		if err != nil {
			return err
		}
		return vm.pushBool(success)
	default:
		return vm.pushBool(false)
	}

}

func checkSigEd25519(pubkey []byte, msg []byte, sig []byte) (bool, error) {

	if len(pubkey) != ed25519.PublicKeySize {
		return false, ErrBadPublicKey
	}
	if len(msg) != crypto.HashLength {
		return false, ErrBadMessageSize
	}
	if len(sig) != ed25519.SignatureSize {
		return false, ErrBadSignature
	}
	return ed25519.Verify(pubkey, msg, sig), nil
}

func checkSigSchnorrkel(pubkey []byte, msg []byte, sig []byte) (bool, error) {

	if len(pubkey) != schnorr.PublicKeySize {
		return false, ErrBadPublicKey
	}
	if len(msg) != crypto.HashLength {
		return false, ErrBadMessageSize
	}
	if len(sig) != schnorr.SignatureSize {
		return false, ErrBadSignature
	}

	pub, err := schnorr.NewPublicKey(pubkey)
	if err != nil {
		return false, err
	}

	return schnorr.Verify(pub, msg, sig)

}

func opCheckMultiSig(publicKeys [][]byte, msg []byte, signatures [][]byte) (bool, error) {
	return true, nil
}
