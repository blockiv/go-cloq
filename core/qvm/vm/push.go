package vm

func opPush(vm *VirtualMachine) error {
	err := vm.applyCost(1)
	if err != nil {
		return err
	}

	buf := make([]byte, len(vm.memory))
	copy(buf, vm.memory)
	return vm.push(buf)

}

func opNop(vm *VirtualMachine) error {
	return vm.applyCost(1)
}
