package addr

import (
	"bytes"
	"encoding/hex"
	"testing"

	"gitlab.com/cloq/go-cloq/core/crypto"

	"gitlab.com/cloq/go-cloq/protocol"
)

func TestGenAddress(t *testing.T) {

	testCases := []struct {
		script   string
		valid    bool
		expected string
	}{{
		script:   "718064a274a280",
		valid:    true,
		expected: "tq18ype9uhdhu5f0gquv449mlyaxp2jxfq4vrj5etk9jatytx0ha5fs0q2g5j",
	}, {
		script:   "a44a203c228306552177f5a304cb12a5b5e60897f2f486b64671afdccf0f8dd9410cbd80",
		valid:    true,
		expected: "tq1e7gf3uhk6la5xtl6anre4l9yfnh5ecam8p36lychvqg34yk04a2svy96qr",
	}, {
		script:   "a34a2092dad9443e4dd6d70a7f11872101ebff87e21798e4fbb26fa4bf590eb440e71b80",
		valid:    true,
		expected: "tq1yfrhr3qr5dp8hw7mxz0d7p4ee4ppx6m2f3vgwul0yn5ld87f0vjs5ye0cu",
	}, {
		script:   "6fa44a20b966a57f3010512d1ebe8629864c71c909aeb4752e93ffa525f00976c93e33c080624e000000a24a206cd8a81bea00783c3fcfca07eef8c80adc1fb1ba46dc611d5554983e6ae3a7ea8001",
		valid:    true,
		expected: "tq1wj2c0ga7fwrqqwg69507dej3083e7r665mcyr9gpw4ksfzr5jevqwjvqnv",
	}}

	for _, testcase := range testCases {
		s, _ := hex.DecodeString(testcase.script)

		p2c, err := Encode(s, "testnet")
		if err != nil || p2c != testcase.expected {
			t.Error("failed to encode script with error :", err, "got :", p2c, " expected :", testcase.expected)
		}

		hrp, bc, err := Decode(p2c)
		sh := crypto.SHA3(protocol.TagScript, s)
		if err != nil || hrp != protocol.TestnetAddressPrefix {
			t.Error("failed to decode address to script hash with error", err, "expected ", protocol.TestnetAddressPrefix, "got ", hrp)
		}
		if !bytes.Equal(bc, sh) {
			t.Error("failed to get consistent script hash got ", hex.EncodeToString(bc), " expected : ", hex.EncodeToString(sh[:]))
		}
	}
}
