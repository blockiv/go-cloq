package crypto

import (
	"bytes"
	"crypto/sha512"
	"math/big"

	"golang.org/x/crypto/sha3"

	"golang.org/x/crypto/blake2b"

	"encoding/hex"
	"encoding/json"
)

// HashLength is defaulted to 32 bytes
const HashLength = 32

// EmptyHash is defined as all zero 32 bytes string
var EmptyHash = [32]byte{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

// Hash represents the 32 byte digest of Blake2b
type Hash [HashLength]byte

// SetBytes sets b to hash.
func (h *Hash) SetBytes(b []byte) {
	h.setBytes(b)
}

// SetBig converts a big int to a Hash type
func SetBig(b *big.Int) Hash {
	intBytes := b.Bytes()
	var h Hash
	if len(intBytes) > 32 {
		panic("Too much work")
	}

	copy(h[32-len(intBytes):], intBytes)
	return h
}

// SetBytes converts a byte slice into  a hash type
func SetBytes(b []byte) Hash {
	var h Hash
	h.setBytes(b)
	return h
}

// Equal compares two hashes
func (h Hash) Equal(other Hash) bool {

	return bytes.Equal(h[:], other[:])
}

// String converts a Hash to it's hex representation
func (h Hash) String() string {

	return hex.EncodeToString(h[:])
}

// Big converts a Hash to a big int
func (h Hash) Big() *big.Int {
	return new(big.Int).SetBytes(h[:])
}

// Bytes convert a fixed 32 Hash to a byte slice
func (h Hash) Bytes() []byte {
	return h[:]
}

// setBytes fills the hash with the value of b
func (h *Hash) setBytes(b []byte) {

	copy(h[:], b)
}

// Clone returns a copy of Hash
func (h Hash) Clone() Hash {
	var hash = h
	return hash
}

// MarshalJSON implements hex encoding for hashes.
func (h *Hash) MarshalJSON() ([]byte, error) {
	return json.Marshal(h.String())
}

// UnmarshalJSON implements hex decoding for json hashes.
func (h *Hash) UnmarshalJSON(b []byte) error {
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}
	buf, err := hex.DecodeString(s)
	if err != nil || len(buf) != 32 {
		return err
	}
	copy(h[:], buf)
	return nil
}

// Blake256 is a short hand for Blake2b-256 sum as a slice
func Blake256(data []byte) Hash {
	hash := blake2b.Sum256(data)
	return hash
}

// SHA3Hash is a short hand for SHA3-256 sum
func SHA3Hash(data ...[]byte) Hash {
	hasher := sha3.New256()
	hasher.Reset()
	for i := 0; i < len(data); i++ {
		hasher.Write(data[i])
	}
	var h Hash

	copy(h[:], hasher.Sum(nil))

	return h
}

// HeaderHash computes the blockheader hash SHA512/256(hdrBytes)
func HeaderHash(data []byte) Hash {
	return sha512.Sum512_256(data)
}

// SHA3 is a generic sha3
func SHA3(data ...[]byte) []byte {
	h := SHA3Hash(data...)
	return h[:]
}

// Sortable is a wrapper for sorting hashes
type Sortable []Hash

func (d Sortable) Len() int {
	return len(d)
}
func (d Sortable) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
}
func (d Sortable) Less(i, j int) bool {
	return bytes.Compare(d[i][:], d[j][:]) == -1
}
