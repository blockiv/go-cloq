package schnorr

import (
	"bytes"
	"crypto/rand"
	"encoding/hex"
	"reflect"
	"testing"
)

func TestNewKeypairFromSeed(t *testing.T) {
	seed := make([]byte, 32)
	_, err := rand.Read(seed)
	if err != nil {
		t.Fatal(err)
	}

	kp, err := NewKeypairFromSeed(seed)
	if err != nil {
		t.Fatal(err)
	}

	if kp.public == nil || kp.private == nil {
		t.Fatal("key is nil")
	}
}

func TestSignAndVerify(t *testing.T) {
	kp, err := GenerateKeypair()
	if err != nil {
		t.Fatal(err)
	}

	msg, _ := hex.DecodeString("b0a20339bb05f8f734b2121a84f7672f0bccff4f20eadf48ff81b34a82ef010c")
	sig, err := kp.Sign(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("Sig :", hex.EncodeToString(sig))
	pub := kp.Public()
	t.Log("PublicKey :", pub.String())
	ok, err := Verify(pub, msg, sig)
	if err != nil {
		t.Fatal(err)
	}
	if !ok {
		t.Fatal("Fail: did not verify sr25519 sig")
	}
}

func TestPublicKeys(t *testing.T) {
	kp, err := GenerateKeypair()
	if err != nil {
		t.Fatal(err)
	}

	priv := kp.Private()
	kp2, err := NewKeypair(priv.key)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(kp.Public(), kp2.Public()) {
		t.Fatalf("Fail: pubkeys do not match got %x expected %x", kp2.Public(), kp.Public())
	}
}

func TestEncodeAndDecodePrivateKey(t *testing.T) {
	kp, err := GenerateKeypair()
	if err != nil {
		t.Fatal(err)
	}

	enc := kp.Private().Encode()
	res := new(PrivateKey)
	err = res.Decode(enc)
	if err != nil {
		t.Fatal(err)
	}
	resBuf := res.key.Encode()
	exp := kp.Private().Encode()
	if !bytes.Equal(resBuf[:], exp) {
		t.Fatalf("Fail: got %x expected %x", res.key.Encode(), exp)
	}
}

func TestEncodeAndDecodePublicKey(t *testing.T) {
	kp, err := GenerateKeypair()
	if err != nil {
		t.Fatal(err)
	}

	enc := kp.Public().Encode()
	res := new(PublicKey)
	err = res.Decode(enc)
	if err != nil {
		t.Fatal(err)
	}

	resBuf := res.key.Encode()
	exp := kp.Public().Encode()
	if !bytes.Equal(resBuf[:], exp) {
		t.Fatalf("Fail: got %v expected %v", res.key, exp)
	}
}
