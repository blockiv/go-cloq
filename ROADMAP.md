# Testnet Roadmap

- Just build a working node !
- Modules and Packages with explicit APIs
- Reduce cognitive load
- One centirfuge service to do the actual heavy lifting (talking to peers, validating data)
- Design for testability
- Tests ! Tests ! Tests !
- ~~Rename service to Node~~ :
  - ~~Remove P2P package (leave wire)~~
  - ~~Refactor ac to chain package remove service abstraction~~
  - ~~Refactor node to network module (wrap Noise + Peer Management)~~
  - Node package implements main node service (centrifuge)
    - Initiate : load last saved, start network stack
    - Connect to Bootstrap peers (establish Handshake Status)
    - Select best advertised peer
    - Send Request GetHeaders(genesis,nil,maxBlocks)
    - Parse response
    - Validate headerchain (establish tip) and insert into storage
    - Send Request GetBlocks
    - Parse and validate blocks
    - Start Listening for inbound connections

- Blockchain State (Fast Access):
  - State (UTXO SMT)
  - Headerchain
  - Network Parameters
- Ledger (Store)
  - LedgerDB
- Client Node :
  - P2P Node
  - Blockchain State
  - Ledger
- Stateless Execution :
  - H(TXID||Index||Output) Value : H(H(TXID | Index | Output ), Height)
  - Input : Hash(TXID||Index||Output) | Sighash | Code | Height | Value ??
  - Proof : Key = H(TXID||Index||Output) | Value = H(TXID||Index||Output||),Height)

  - Merkle Proof (SMT) : Audith Path || Included || Key || Value
  - Verify Tx => VerifyProof(Input Hash, Input Height,Audit Path,Pre-Transition Root)

- Case New Transaction  :
  - Last Received : Create New Address : xxxxx
  - On New Block : Transaction is output address mine => if yes add to mine cache
  - mine cache : H(TXID||Index||Output),(TXID,Index,Output,Height)
  - Hash (TXID||Index||Output,Height) => New Coin
  - New Transaction : Input = H(New Coin)
  - Get Balance for Address = Scan Address Balance => Get Coins
  - On New Block Commitment (Add Address Storage)

- Wallet :
  - Subscribe to AddressNotify
  - Cache (Transaction History, Outputs)
  - Cache Script

- Block Validation :
  - [x] Block Validation
  - [x] GHOST
    - [x] Include Uncle Calculations in Proof Of Work verification
  - [ ] Stateless Validation Included
- [x] Sighash Type as part of input :
  - [x] SIGHASH Single : Commit to one H(H(input)||H(output))
  - [x] SIGHASH All : Commit to TXID
  - [ ] Handle SIGHASH types in QVM
- Confidential Transactions :
  - [ ] Implement BulletProofs (Range Proofs for transacted values)
  - [ ] Implement Asset Issuance on Colored Coins / Confidential Transactions
  - [ ] Look at ways to Enforce new covenants
- Node :
  - [x] Implement a sync manager thread (Sync headers then blocks and rebuild Contract Set)
    - [x] Initial Sync (Mining/Mempool/Listening/Inbound Connections/Connecting to other peers)
    - [x] Serialize Service Start/Close and ressource races on main
    - [ ] Handle NodeData messages and rebuilding the state tree without blocks
  - [ ] Implement an RPC backend for querying the StateDB and BlockDB (Storage service) and Broadcasting
  - [x] Implement Node Exchange Protocol (Handshake/Sync/Broadcasting)
  - [x] ~~Refactor Storage interface~~
  - [ ] Signal Initial Block Download and Prevent Connecting To More Peers Until the sync is active

- Wallet :
  - [ ] Encrypted Wallet with Standard Script Generation

This is a list of preliminary [CLIPS] we wish to introduce in testnet.

- **CLIP-001** : Improve VM

  - [x] Wrap VM under QVM handle underlying VM runs concurrently
  - [x] Review VM Opcode (Arithmetic on bigInt + Checkoutputhash)
  - [x] ~~Eval instead of Run~~
  - [x] ~~Implement versioning scheme for VM programs~~
  - [x] ~~Rename inputs to ContractID and Output and Outpoint to Contract ? (just cleanup names and remove confusion).~~
  - [ ] Document VM operations and opcodes (add remaining opcodes), split any cyclic dependancy
  - [x] ~~Replace Checkoutput by checkoutputhash~~
  - [x] ~~Script versioning~~
  - [x] ~~Expand tagged hashing for txos.~~
  - [-] ~~Default to ed25519 for signatures add checksigalt~~
  - [ ] Add BLS Signatures
  - [ ] Add arithmetic opcodes
