package centrifuge

import (
	"encoding/hex"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/perlin-network/noise"
	"gitlab.com/cloq/go-cloq/consensus/config"
	"gitlab.com/cloq/go-cloq/consensus/pow"
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/internal/testgen"
	"gitlab.com/cloq/go-cloq/modules/chain"
	"gitlab.com/cloq/go-cloq/modules/network/wire"
	"gitlab.com/cloq/go-cloq/modules/storage"
	"gitlab.com/cloq/go-cloq/modules/storage/ledger"
	"gitlab.com/cloq/go-cloq/protocol"
	"gitlab.com/cloq/go-cloq/service/ac"
)

func TestMockNode(t *testing.T) {
	dir, err := ioutil.TempDir(os.TempDir(), "test_data")
	if err != nil {
		t.Fatal("failed to create temp dir")
	}
	_, _, node := createTestNode(t, dir)
	hdr, err := node.Blockchain.GetHeaderByHeight(1, ledger.MAIN)
	if err != nil {
		t.Fatalf("failed to populate mock store with error : %s", err)
	}
	hdrHash, _ := hdr.Hash()
	t.Log("Header hash :", hex.EncodeToString(hdrHash.Bytes()))
}

func TestMockNodeHandler(t *testing.T) {
	aliceDir, err := ioutil.TempDir(os.TempDir(), "test_alice")
	if err != nil {
		t.Fatal("failed to create temp dir")
	}
	bobDir, err := ioutil.TempDir(os.TempDir(), "test_bob")
	if err != nil {
		t.Fatal("failed to create temp dir")
	}
	_, _, alice := createTestNode(t, aliceDir)
	_, _, bob := createTestNode(t, bobDir)
	t.Log("Created Mock node !")

	hdr, err := alice.Blockchain.GetHeaderByHeight(1, ledger.MAIN)
	if err != nil {
		t.Fatalf("failed to populate mock store with error : %s", err)
	}
	startingHeaderHash, _ := hdr.Hash()
	t.Log("Populated store !")

	t.Logf("Requesting blocks starting from : %s\n", startingHeaderHash)

	if err != nil {
		t.Fatal("failed to fetch header with error : ", err)
	}

	alice.Start()
	bob.Start()

	t.Log("Listening")

	if _, err := bob.Network.Dial(alice.Network.Host.Addr()); err != nil {
		panic(err)
	}
	t.Log("Boostrapped to new peer")
	nodes := []*noise.Node{alice.Network.Host, bob.Network.Host}
	for _, node := range nodes {
		for _, client := range append(node.Inbound(), node.Outbound()...) {
			client.WaitUntilReady()
		}
	}

	bobStatus := bob.GetNodeStatus()

	resp, err := bob.Network.SendRequestToPeer(alice.Network.Host.ID(), bobStatus)
	if err != nil {
		t.Fatal("failed to send request to peer with error :", err)
	}
	t.Log("response received from remote peer")
	aliceStatus, err := wire.UnmarshalWireMessage(resp)
	if err != nil {
		t.Fatal("failed to unmarshal response from peer with error :", err)
	}
	if aliceStatus.Topic() != wire.Reject {
		t.Fatal("expected peer to be rejected due to incompatible genesis")
	}
	alice.Network.Host.Close()
	bob.Network.Host.Close()

}

// createTestNode creates a testing node
func createTestNode(t *testing.T, dir string) (*types.Block, crypto.Hash, *Node) {
	genesisBlk, genesisHash, err := chain.CreateGenesisBlock([]byte("Test Genesis Record"), time.Now().Unix(), "1cqtestaddress", protocol.GetBlockIssuance(0), protocol.RegressionPoWLimit)
	if err != nil {
		t.Fatal("failed to create genesis block")
	}
	cfg := config.WhirlpoolRegnet(genesisHash)
	store, err := storage.NewStorage(dir, true)
	if err != nil {
		t.Fatal("failed to create storage")
	}
	chain, err := chain.New(cfg, store)
	if err != nil {
		t.Fatal("failed to create blockchain")
	}
	node := NewNode(cfg, config.NodeConfig{}, nil, chain)
	err = chain.LoadGenesis(genesisBlk)
	if err != nil {
		t.Fatal("failed to load genesis with error :", err)
	}
	t.Log("Chain tip :", chain.GetChainTip())
	err = populateValidChain(t, chain)
	if err != nil {
		t.Fatalf("failed to populate mock store with error : %s", err)
	}

	return genesisBlk, genesisHash, node
}

// createGenesisNode creates a testing node
func createGenesisNode(t *testing.T, genesisBlk *types.Block, genesisHash []byte, dir string) *Node {

	cfg := config.WhirlpoolRegnet(crypto.SetBytes(genesisHash))
	store, err := storage.NewStorage(dir, true)
	chain, err := chain.New(cfg, store)

	if err != nil {
		t.Fatal("failed to create genesis with error")
	}

	err = chain.LoadGenesis(genesisBlk)
	if err != nil {
		t.Fatal("failed to create genesis with error")
	}

	return NewNode(cfg, config.NewConfig(cfg, false, false, []string{}), ac.NewService(cfg, chain, config.MinerConfig{}), chain)
}

// populateMockStore fills database with data for mock processing
func populateMockStore(genesis *types.Block, store *storage.Storage) (*types.Block, error) {
	err := store.StateDB.Put(testgen.GenOpenContracts(100))
	if err != nil {
		return nil, err
	}
	blocks := testgen.GenBlocks()

	for _, block := range blocks {
		err := store.LedgerDB.PutNewBlock(block, ledger.MAIN)
		if err != nil {
			return nil, err
		}
	}

	return blocks[len(blocks)-1], err
}

// populateValidChain generates a valid chain for service tests.
func populateValidChain(t *testing.T, blockchain *chain.Blockchain) error {

	for i := 1; i < 10; i++ {
		coinbase := testgen.GenCoinbase(uint64(i))
		nextBlock, err := blockchain.CreateNextBlock([]*types.Transaction{coinbase})

		if err != nil {
			t.Fatal("Failed to create next block with error :", err)
		}

		nextBlock, err = pow.SolveBlock(nextBlock)
		if err != nil {
			t.Fatal("Failed to create next block with error :", err)
		}
		err = blockchain.AppendBlock(nextBlock, chain.MAIN)
		if err != nil {
			t.Fatal("Failed to append block with error :", err)
		}
		err = blockchain.UpdateTip(nextBlock.Header)
		if err != nil {
			t.Fatal("Failed to update tip with error :", err)
		}
		t.Log("Created next block at height", nextBlock.Height, " block timestamp :", nextBlock.GetTimestamp())
		// TODO FIX: add sleep to process reg pow otherwise blocks will be invalid due
		// to erroneous timestamps
	}
	t.Log("Alice Tip :", blockchain.GetChainTip())

	return nil

}
