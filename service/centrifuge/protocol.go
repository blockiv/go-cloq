package centrifuge

import (
	"github.com/perlin-network/noise"
	"gitlab.com/cloq/go-cloq/modules/network"
	"gitlab.com/cloq/go-cloq/modules/network/wire"
	"go.uber.org/zap"
)

// HandleGossipMessage handles new gossip messages
func (n *Node) HandleGossipMessage(sender noise.ID, data []byte) error {

	message, err := wire.UnmarshalWireMessage(data)
	if err != nil {
		return err
	}

	if message.Topic() == wire.NewTransaction {
		newTransactionBytes, ok := message.(*wire.TransactionMessage)
		if !ok {
			return errMessageType
		}
		return n.OnMessageNewTransaction(sender, newTransactionBytes)
	} else if message.Topic() == wire.NewBlock {
		newBlockBytes, ok := message.(*wire.NewBlockMessage)
		if !ok {
			return errMessageType
		}
		return n.OnMessageNewBlock(sender, newBlockBytes)
	}

	return n.Network.SendMessageToPeer(sender, wire.NewRejectMessage())
}

// HandlePeerConnected handles new connected peers
func (n *Node) HandlePeerConnected(cli *noise.Client) {
	newPeer := network.NewPeer(cli)
	n.Logger.Info("Handling new connected peer :", zap.String("ID", cli.ID().String()))
	n.Network.Peers.AddNewPeer(newPeer)
}
