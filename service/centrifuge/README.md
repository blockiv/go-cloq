# Centrifuge

Centrifuge is the main node service, it connects to the P2P network, handles
incoming messages from other services and implements a reactor pattern
to handle network messages.

Centrifuge is responsible for syncing and initial block download it also
runs non-contextual validation and may ask peers for missing data.
