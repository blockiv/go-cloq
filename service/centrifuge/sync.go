package centrifuge

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"time"

	hashset "github.com/deckarep/golang-set"
	lru "github.com/hashicorp/golang-lru"
	"github.com/perlin-network/noise"
	"go.uber.org/zap"

	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/modules/chain"
	"gitlab.com/cloq/go-cloq/modules/network/wire"
	"gitlab.com/cloq/go-cloq/service"
)

var (
	// maxBlocksToSend or receive in a single message
	maxBlocksToSend = wire.MaxBlocksPerMessage
	// maxStallDuration to wait before disconnecting from a non progressing peer
	maxStallDuration = 3 * time.Minute
	// resyncInterval is the time between doing a status handshake to establish
	// if the node needs to catch up
	resyncInterval = 3 * time.Minute
)

type BlockStatus int

const (
	// UNKNOWN : the header of the block is not yet connected
	UNKNOWN BlockStatus = iota
	// INVALID : the header of the block is invalid
	INVALID
	// CONNECTED : the header of the block is connected to the chain and extends it
	CONNECTED
	// DOWNLOADED : the block body was downloaded its ancestors are downloaded or accepted
	DOWNLOADED
	// ACCEPTED : the block is valid extends the chain and the transition is correct
	ACCEPTED
)

// SyncState represents the current node syncing status, when a node first
// starts it tries to initially sync up to a known tip. At that point once we
// reach the best known peer's tip we start connecting to the gossip network
// accepting new transactions and blocks, the sync state ownership is always
// that of node, using a channel a separate go routine RemainInSync we'll
// periodically query other peers to check if we need to catch up due to
// latency (faulty network conditions, load schedding...)
type SyncState struct {
	candidatePeers      []noise.ID
	bestAdvertisedState *wire.StatusMessage

	headers map[crypto.Hash]BlockStatus
	blocks  map[crypto.Hash]BlockStatus

	invalidBlocks   *lru.Cache
	requestedBlocks hashset.Set
	orphanedBlocks  hashset.Set
}

// TryInitialSync initates the initial sync protocol.
// Step 1: Connect to a random peer from the candidate list.
// Step 2: Send a Status message request
// Step 3: Parse response
// Step 4: Check advertised Status message response
// Step 5: Send a getheaders message starting from our tip
// Step 5: Process header batch response
// Step 6: If last getblocks message length is less than maxBlocksToSend exit
// else go to 5.
// TODO : update block status after each step
func (n *Node) TryInitialSync(candidate string, validate bool) (bool, error) {

	chainView := n.Blockchain.GetChainView()
	currentTip := chainView.GetChainTip()
	n.Logger.Info("current chain tip :", zap.Any("tip", currentTip))
	nodeConfig := n.ChainConfig

	tipHeight := currentTip.MostWorkHeight
	tipHash := currentTip.MostWorkHash
	tipRoot := currentTip.MostWorkRoot
	tipWork := currentTip.TotalChainWork

	message := wire.NewStatusMessage(nodeConfig.NetworkMagic(), nodeConfig.GetBranchID(),
		tipHeight, tipWork.Bytes(), tipRoot.Bytes(), tipHash.Bytes(), nodeConfig.GenesisID())
	n.Logger.Info("message status :", zap.Any("message", message))
	peer, err := n.Network.Dial(candidate)
	if err != nil {
		return false, err
	}

	n.Logger.Info("connected to Dial waiting for client to be ready")

	peer.Client.WaitUntilReady()

	n.Logger.Info("client handshake done")
	if err := n.Network.SendMessageToPeer(peer.Client.ID(), message); err != nil {
		return false, err
	}
	response, err := n.Network.SendRequestToPeer(peer.Client.ID(), message)
	if err != nil {
		n.Logger.Warn("failed to receive response from peer", zap.String("service", "sync"), zap.Error(err))
		return false, err
	}
	n.Logger.Info("succesfully received response from remote peer", zap.String("peer", peer.Client.ID().String()))
	// for the receiving peer we're an inbound connection thus it will responde
	// with a status message of it's owsm.
	msg, err := wire.UnmarshalWireMessage(response)
	if err != nil || msg.Topic() == wire.Reject {
		n.Logger.Warn("failed to unmarshal response from peer", zap.String("service", "sync"), zap.Error(err), zap.Bool("rejected", true))
		return false, err
	}
	peerStatus, ok := msg.(*wire.StatusMessage)
	if !ok {
		n.Logger.Warn("failed to unmarshal response from peer", zap.String("service", "sync"), zap.Error(err))
		return false, err
	}
	n.Logger.Info("peer status :", zap.String("genesis", hex.EncodeToString(peerStatus.GenesisHash)), zap.Uint64("height", peerStatus.TipHeight), zap.Uint32("branchID", peerStatus.BranchID))

	requestedHeaders := make([][]byte, 0, wire.MaxHeadersPerMessage)

	getHeadersMessage := wire.NewGetHeadersMessage()
	getHeadersMessage.StartingPoint = peerStatus.GenesisHash
	getHeadersMessage.MaxHeaders = wire.MaxHeadersPerMessage
	getHeadersMessage.Reverse = false

	if currentTip.MostWorkHeight < peerStatus.TipHeight {

		resp, err := n.Network.SendRequestToPeer(peer.Client.ID(), getHeadersMessage)
		if err != nil {
			return false, err
		}
		n.Logger.Info("succesfully received response from remote peer", zap.String("peer", peer.ID))

		respMessage, err := wire.UnmarshalWireMessage(resp)

		if err != nil || respMessage.Topic() != wire.Headers {
			return false, err
		}

		serializedHeaders, ok := respMessage.(*wire.HeadersMessage)
		if !ok {
			return false, errMessageType
		}
		headerCount := len(serializedHeaders.BlockHeaders)
		n.Logger.Info("received headers from peer", zap.Int("header count", headerCount))
		headers := make([]*types.Header, 0, len(serializedHeaders.BlockHeaders))

	headerSync:
		for _, headerBytes := range serializedHeaders.BlockHeaders {

			newHeader := types.NewEmptyHeader()
			err := newHeader.Unmarshal(headerBytes)

			if err != nil {
				return false, err
			}

			headerHash, _ := newHeader.Hash()

			if err != nil {
				return false, err
			}
			ok, err := chain.VerifyHeaderWithoutContext(newHeader, tipWork.Big(), nodeConfig, currentTip.MedianTimestamp)
			if !ok || err != nil {
				return false, err
			}

			requestedHeaders = append(requestedHeaders, headerHash.Bytes())
			headers = append(headers, newHeader)
		}
		resChan := make(chan service.Result, 1)
		if validate {

			n.AdmissionControl.QueueHeadersBatch(&service.HeaderBatch{Headers: headers, ResultChan: resChan})

			res := <-resChan
			if res.Err != nil || !res.Ok {
				return false, err
			}
		}
		// TODO : Break loop to request actual blocks
		if uint32(headerCount) < wire.MaxHeadersPerMessage {
			break headerSync
		}
		getHeadersMessage.StartingPoint = chainView.GetChainTip().MostWorkHash.Bytes()

		// send getheaders message
		// block and process headers
		// re-send getheaders message or quit
	}

	getBlocksMessage := wire.NewGetBlocksMessage()

	getBlocksMessage.Full = false
	getBlocksMessage.Reverse = false

	totalHeaders := len(requestedHeaders)

	for i := 0; i < totalHeaders; i += 32 {

		getBlocksMessage.BlockHashes = requestedHeaders[i : i+32]
		resp, err := n.Network.SendRequestToPeer(peer.Client.ID(), getBlocksMessage)
		if err != nil {
			return false, err
		}

		respMessage, err := wire.UnmarshalWireMessage(resp)

		if err != nil || respMessage.Topic() != wire.Blocks {
			return false, err
		}

		serializedBlocks := respMessage.(*wire.BlocksMessage)

		receivedBlkCount := len(serializedBlocks.BlockBodies)

		blocks := make([]*types.Block, 0, len(serializedBlocks.BlockBodies))

		for idx, blockBytes := range serializedBlocks.BlockBodies {

			blkHashMessage := serializedBlocks.BlockHashes[idx]

			newBlock := types.NewEmptyBlock()

			err := newBlock.Unmarshal(blockBytes)
			if err != nil {
				return false, err
			}

			blkHash, _ := newBlock.Hash()

			if !bytes.Equal(blkHash.Bytes(), blkHashMessage) {
				return false, fmt.Errorf("hash mismatch expected block hash %s got %s", hex.EncodeToString(blkHashMessage), blkHash.String())
			}

			blocks = append(blocks, newBlock)
		}
		resChan := make(chan service.Result, 1)
		if validate {
			// ac.ProcessBlockBodies should connect headless blocks before appending them
			// to the chain, either trough GetHeader, connectBlock, validate block
			// append header
			currentTip = chainView.GetChainTip()

			n.AdmissionControl.QueueBlockBatch(&service.BlockBatch{Hashes: serializedBlocks.BlockHashes, BlockBodies: blocks, ResultChan: resChan})

			res := <-resChan
			if res.Err != nil || !res.Ok {
				return false, err
			}

		}

		if receivedBlkCount < wire.MaxBlocksPerMessage {
			return true, nil
		}
	}

	n.Logger.Info("Succesfully synced :", zap.Any("Tip", chainView.GetChainTip()))
	return true, nil
}
