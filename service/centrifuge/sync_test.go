package centrifuge

import (
	"context"
	"io/ioutil"
	"os"
	"testing"
)

func TestSyncManager(t *testing.T) {
	aliceDir, err := ioutil.TempDir(os.TempDir(), "test_alice")
	if err != nil {
		t.Fatal("failed to create temp dir")
	}
	bobDir, err := ioutil.TempDir(os.TempDir(), "test_bob")
	if err != nil {
		t.Fatal("failed to create temp dir")
	}
	genesis, genesisHash, alice := createTestNode(t, aliceDir)
	t.Log("Alice tip :", alice.Blockchain.GetChainTip())
	bob := createGenesisNode(t, genesis, genesisHash.Bytes(), bobDir)

	err = bob.AdmissionControl.Start(context.Background())
	if err != nil {
		t.Fatal("failed to start ac with error :", err)
	}
	alice.RegisterHandlers()

	err = alice.Network.Listen()
	if err != nil {
		panic(err)
	}
	aliceCandidate := alice.Network.Host.ID()
	t.Log("alice noise id :", aliceCandidate)

	// If the network.Listen is not called the node won't have proper handshake keys
	err = bob.Network.Listen()
	if err != nil {
		panic(err)
	}

	ok, err := bob.TryInitialSync(alice.Network.Host.Addr(), true)

	if !ok || err != nil {
		t.Fatal("failed to sync with error :", err)
	}
}
