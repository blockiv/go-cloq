package centrifuge

import (
	"context"
	"errors"
	"time"

	"github.com/perlin-network/noise"
	"gitlab.com/cloq/go-cloq/consensus/config"
	"gitlab.com/cloq/go-cloq/modules/network/wire"
	"gitlab.com/cloq/go-cloq/service/ac"

	"gitlab.com/cloq/go-cloq/modules/chain"
	"gitlab.com/cloq/go-cloq/modules/network"
	"gitlab.com/cloq/go-cloq/modules/storage"
	"go.uber.org/zap"
)

// Centrifuge node service errors
var (
	errMessageType      = errors.New("invalid message type")
	errIncompatiblePeer = errors.New("incompatible peer status")
)

// Centrifuge node service timer options
var (
	heartBeatInterval      = 15 * time.Second
	requestTimeoutInterval = 30 * time.Second
)

// Node represents a client node it handles connections to peers
// validating messages and submitting blocks and transaction to
// ac and mempool.
type Node struct {
	ChainConfig      config.ChainParameters
	NodeConfig       config.NodeConfig
	Blockchain       *chain.Blockchain
	Network          *network.Network
	Storage          *storage.Storage
	MessageProcessor *MessageProcessor
	SyncState        *SyncState
	Logger           *zap.Logger
	AdmissionControl *ac.Service
}

// TODO : Node should handle Peer activity at a higher level instead of using
// Network (Segregate network from peer handling)
// Create and administer AC service and Blockchain locally
// NewNode creates a new node instance.
func NewNode(cfg config.ChainConfig, nodeCfg config.NodeConfig, ac *ac.Service, blockchain *chain.Blockchain) *Node {

	logger, err := zap.NewDevelopment()
	if err != nil {
		return nil
	}

	mp := NewMessageProcessor()

	node := &Node{
		ChainConfig:      cfg,
		NodeConfig:       nodeCfg,
		Blockchain:       blockchain,
		Logger:           logger,
		MessageProcessor: mp,
		AdmissionControl: ac,
	}
	peersStore := network.NewPeerStore()
	protocolHandler := network.NewProtocolHandler(node.HandlePeerConnected, nil, nil, nil, node.HandleGossipMessage)
	net, err := network.NewNetwork(protocolHandler, logger, peersStore)
	if err != nil {
		return nil
	}
	node.Network = net

	return node
}

// Start the centrifuge service
func (n *Node) Start() {

	n.RegisterHandlers()
	if err := n.Network.Listen(); err != nil {
		panic(err)
	}

	if n.AdmissionControl == nil {
		return
	}

	if err := n.AdmissionControl.Start(context.Background()); err != nil {
		panic(err)
	}

}

// Shutdown the centrifuge service
func (n *Node) Shutdown() {
	n.Network.Shutdown()
	n.AdmissionControl.Shutdown()
	n.Storage.Close()
}

// ConnectTo establishes a connection to a remote peer
func (n *Node) ConnectTo(addr string) (*network.Peer, error) {
	return n.Network.Dial(addr)
}

// TODO : Register all handlers + improve error handling for node process
// RegisterHandlers register node level handlers.
func (n *Node) RegisterHandlers() {

	n.MessageProcessor.Register(wire.Ping, func(sender noise.ID, message wire.Message, ctx *noise.HandlerContext) error {
		msg, ok := message.(*wire.PingMessage)
		if !ok {
			return errMessageType
		}
		return n.OnMessagePing(sender, msg, ctx)
	})
	n.MessageProcessor.Register(wire.Pong, func(sender noise.ID, message wire.Message, ctx *noise.HandlerContext) error {
		msg, ok := message.(*wire.PongMessage)
		if !ok {
			return errMessageType
		}
		return n.OnMessagePong(sender, msg, ctx)
	})
	n.MessageProcessor.Register(wire.GetHeaders, func(sender noise.ID, message wire.Message, ctx *noise.HandlerContext) error {
		msg, ok := message.(*wire.GetHeadersMessage)
		if !ok {
			return errMessageType
		}
		return n.OnMessageGetHeaders(sender, msg, ctx)
	})
	n.MessageProcessor.Register(wire.GetBlocks, func(sender noise.ID, message wire.Message, ctx *noise.HandlerContext) error {
		msg, ok := message.(*wire.GetBlocksMessage)
		if !ok {
			return errMessageType
		}
		return n.OnMessageGetBlocks(sender, msg, ctx)
	})
	n.MessageProcessor.Register(wire.Status, func(sender noise.ID, message wire.Message, ctx *noise.HandlerContext) error {
		msg, ok := message.(*wire.StatusMessage)
		if !ok {
			return errMessageType
		}
		return n.OnMessageStatus(sender, msg, ctx)
	})
	n.MessageProcessor.Register(wire.Headers, func(sender noise.ID, message wire.Message, ctx *noise.HandlerContext) error {
		msg, ok := message.(*wire.HeadersMessage)
		if !ok {
			return errMessageType
		}
		return n.OnMessageHeaders(sender, msg)
	})
	n.MessageProcessor.Register(wire.Blocks, func(sender noise.ID, message wire.Message, ctx *noise.HandlerContext) error {
		msg, ok := message.(*wire.BlocksMessage)
		if !ok {
			return errMessageType
		}
		return n.OnMessageBlocks(sender, msg)
	})
	n.MessageProcessor.Register(wire.GetData, func(sender noise.ID, message wire.Message, ctx *noise.HandlerContext) error {
		msg, ok := message.(*wire.GetDataMessage)
		if !ok {
			return errMessageType
		}
		return n.OnMessageGetData(sender, msg, ctx)
	})
	n.MessageProcessor.Register(wire.NewBlock, func(sender noise.ID, message wire.Message, ctx *noise.HandlerContext) error {
		msg, ok := message.(*wire.NewBlockMessage)
		if !ok {
			return errMessageType
		}
		return n.OnMessageNewBlock(sender, msg)
	})
	n.MessageProcessor.Register(wire.NewTransaction, func(sender noise.ID, message wire.Message, ctx *noise.HandlerContext) error {
		msg, ok := message.(*wire.TransactionMessage)
		if !ok {
			return errMessageType
		}
		return n.OnMessageNewTransaction(sender, msg)
	})
	n.MessageProcessor.Register(wire.Reject, func(sender noise.ID, message wire.Message, ctx *noise.HandlerContext) error {
		msg, ok := message.(*wire.RejectMessage)
		if !ok {
			return errMessageType
		}
		return n.OnMessageReject(sender, msg, ctx)
	})
	n.Network.Host.Handle(n.HandleMessage)
}

// HandleMessage parses incoming messages and dispatches them to an underlying
// handler.
// HandleMessage is registered as a network handler for incoming peer connections.
func (n *Node) HandleMessage(ctx noise.HandlerContext) error {

	sender := ctx.ID()
	messageBytes := ctx.Data()

	message, err := wire.UnmarshalWireMessage(messageBytes)
	if err != nil {
		return err
	}
	messageTopic := message.Topic()
	if ctx.IsRequest() {
		n.Logger.Debug("Received new request message with topic " + wire.GetTopic(messageTopic) + " from peer :" + sender.String())
		return n.MessageProcessor.HandleRequest(sender, message, &ctx)
	}
	n.Logger.Debug("Received new message with topic " + wire.GetTopic(messageTopic) + " from peer :" + sender.String())
	return n.MessageProcessor.Handle(sender, message)
}

// GetNodeStatus returns the status of the current peer in the form
// of a network message.
func (n *Node) GetNodeStatus() wire.StatusMessage {

	nodeConfig := n.ChainConfig
	currentTip := n.Blockchain.GetChainTip()
	tipHeight := currentTip.MostWorkHeight
	tipHash := currentTip.MostWorkHash
	tipRoot := currentTip.MostWorkRoot
	tipWork := currentTip.TotalChainWork

	return wire.NewStatusMessage(nodeConfig.NetworkMagic(), nodeConfig.GetBranchID(),
		tipHeight, tipWork.Bytes(), tipRoot.Bytes(), tipHash.Bytes(), nodeConfig.GenesisID())
}
