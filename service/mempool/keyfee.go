package mempool

// keyFee implements a pair of hash,int that specify txid and the fee.
type keyFee struct {
	k string
	v uint64
}

// newKeyFee creates a new pair
func newKeyFee(key string, value uint64) keyFee {
	return keyFee{key, value}
}

// Key returns the key
func (p *keyFee) Key() string {
	return p.k
}

// Value returns the value
func (p *keyFee) Value() uint64 {
	return p.v
}

type byKey []keyFee

func (bk byKey) Len() int           { return len(bk) }
func (bk byKey) Swap(i, j int)      { bk[i], bk[j] = bk[j], bk[i] }
func (bk byKey) Less(i, j int) bool { return bk[i].v < bk[j].v }
