package mempool

import (
	"crypto/ed25519"
	"crypto/rand"
	"encoding/hex"
	"testing"

	"gitlab.com/cloq/go-cloq/core/types"

	"gitlab.com/cloq/go-cloq/core/addr"
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/script"
	"gitlab.com/cloq/go-cloq/protocol"
	"gitlab.com/cloq/go-cloq/service"
)

func TestMempoolService(t *testing.T) {

	mempool := NewMempool(1)

	for i := 0; i <= 10; i++ {

		testTX := genRandomTx()

		serviceTx := service.NewIncomingTx(testTX, true, nil)
		err := mempool.Store(serviceTx)
		if err != nil {
			t.Log("failed to add testTX with error")
		}
	}

}

func genRandomTx() *types.Transaction {

	var random = rand.Reader
	var prog = make([]byte, 8)
	var input = make([]byte, 32)
	random.Read(prog)
	random.Read(input)
	var val = script.Script(prog)
	var inputs = []types.Input{
		{
			Origin:    crypto.SHA3Hash(input),
			Validator: val,
		},
	}
	var randomOutputKey, _, _ = ed25519.GenerateKey(random)
	var pubkeyHash = crypto.SHA3(randomOutputKey)
	var encodedPubkeyHash = "0x" + hex.EncodeToString(pubkeyHash[:])

	var defaultScriptSrc = "dup sha3 equal " + encodedPubkeyHash + " verify checksig"
	var defaultScriptBytecode, _ = script.Compile(defaultScriptSrc)
	var defaultScriptAddress, _ = addr.Encode(defaultScriptBytecode, protocol.TestnetAddressPrefix)
	var outputs = []types.Output{
		{
			Address:  defaultScriptAddress,
			Value:    5 * 10e9,
			Locktime: 1025,
		},
	}

	var coinbaseCommitment = types.NewCommitment(inputs, outputs)
	var coinbaseTx = types.NewTransaction(1, protocol.WhirpoolBranchID, 0, 0, coinbaseCommitment, types.NewWitness())

	return coinbaseTx
}
