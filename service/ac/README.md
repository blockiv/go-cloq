# Admission Control

Admission Control is a self-contained service that does data validation
for blocks and transactions.

Admission Control starts in a goroutine whose context parent is centifuge.
It has no access to the networking module, the networking is prehandled
by centrifuge.
