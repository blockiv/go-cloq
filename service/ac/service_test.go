package ac

import (
	"context"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"gitlab.com/cloq/go-cloq/consensus/config"
	"gitlab.com/cloq/go-cloq/consensus/pow"
	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/internal/testgen"
	"gitlab.com/cloq/go-cloq/modules/chain"
	"gitlab.com/cloq/go-cloq/modules/storage"
	"gitlab.com/cloq/go-cloq/protocol"
	"gitlab.com/cloq/go-cloq/service"
)

func TestService(t *testing.T) {
	dir, err := ioutil.TempDir(os.TempDir(), "test_alice")
	if err != nil {
		t.Fatal("failed to create temp dir with error", err)
	}

	storage, err := storage.NewStorage(dir, true)
	if err != nil {
		t.Fatal("failed to create test storage with error", err)
	}
	t.Log("Generating Genesis")
	genesisBlk, genesisHash, err := chain.CreateGenesisBlock([]byte("Test Genesis Record"), time.Now().Unix(), "1cqtestaddress", protocol.GetBlockIssuance(0), protocol.RegressionPoWLimit)
	if err != nil {
		t.Fatal("failed to create genesis block with error", err)
	}
	t.Log("Generated New Genesis Block", genesisHash.String())
	regnet := config.WhirlpoolRegnet(genesisHash)

	blockchain, err := chain.New(regnet, storage)
	if err != nil {
		t.Fatal("failed to create test chain with error", err)
	}

	err = blockchain.LoadGenesis(genesisBlk)
	if err != nil {
		t.Fatal("failed to append genesis block with error", err)
	}
	newTip := blockchain.GetChainTip()
	t.Log(newTip)
	ac := NewService(regnet, blockchain, config.MinerConfig{Active: false})

	err = ac.Start(context.Background())
	if err != nil {
		t.Fatal("failed to start service with error ", err)
	}
	defer ac.Shutdown()

	for i := genesisBlk.GetHeight(); i < 10; i++ {
		coinbase := testgen.GenCoinbase(i + 1)
		nextBlock, err := ac.CreateNextBlock([]*types.Transaction{coinbase})

		if err != nil {
			t.Fatal("failed to create next block with error :")
		}
		t.Log("Created next block successfully", nextBlock.String())

		nextBlock, err = pow.SolveBlock(nextBlock)
		if err != nil {
			t.Fatal("failed to create next block with error :")
		}
		nextBlockHash, _ := nextBlock.Hash()

		resp := make(chan error, 1)

		blockToProcess := service.IncomingBlock{
			Mined:      true,
			Block:      nextBlock,
			Hash:       nextBlockHash,
			Processed:  false,
			ResultChan: resp,
		}
		ac.QueueNewBlock(blockToProcess)

		err = <-resp
		/// we expect timing errors due to latency and how easy to solve
		// regression work
		if err != nil {
			t.Log("failed to process block with error  :", err)
		}
	}

}
