package miner

import (
	"encoding/hex"
	"errors"
	"math/big"
	"sync"
	"time"

	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/types"
	"go.uber.org/zap"

	"gitlab.com/cloq/go-cloq/consensus/pow"
)

// Miner is a separate Go routine that receives blocks does work on
// them and sends them back.
type Miner struct {
	IncomingBlocks chan *types.Block
	SolvedBlocks   chan<- *types.Block
	WorkerCount    int
	Logger         *zap.Logger
	wg             sync.WaitGroup
}

// NewMiner creates a new mining instance given a number of workers
func NewMiner(workers int, outgoignChan chan *types.Block) *Miner {

	logger, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	return &Miner{
		IncomingBlocks: make(chan *types.Block, 5),
		SolvedBlocks:   outgoignChan,
		WorkerCount:    workers,
		Logger:         logger,
	}
}

// Run waits for blocks then spawns threads that work on them
func (miner *Miner) Run() {
	miner.wg.Add(1)
	go miner.run()
}

// run implements the actual mining.
func (miner *Miner) run() {
	defer miner.wg.Done()

	for {
		select {
		case blockToProcess := <-miner.IncomingBlocks:
			solvedBlock := miner.solveBlock(blockToProcess)
			miner.SolvedBlocks <- solvedBlock
		}
	}
}

// Solve passes a block to the worker to find valid work
func (miner *Miner) Solve(blk *types.Block) {
	miner.IncomingBlocks <- blk
}

// solveBlock solves the actual proof of work
func (miner *Miner) solveBlock(blk *types.Block) *types.Block {

	t0 := time.Now()
	h, _ := blk.Hash()
	blk.ChainWork = crypto.SetBig(new(big.Int).Add(blk.ChainWork.Big(), pow.AmountOfWork(blk.Target.Big())))

	for !pow.CheckWork(h, blk.Target.Big()) {
		blk.Nonce++

		h, _ = blk.Hash()
	}
	tn := time.Now()
	if !pow.CheckWork(h, blk.Target.Big()) {
		miner.Logger.Warn("failed to do enough work")
	}

	miner.Logger.Info("Valid Work :", zap.Bool("Valid Work", pow.CheckWork(h, blk.Target.Big())))
	miner.Logger.Info("Target : ", zap.String("New Target", hex.EncodeToString(blk.Target.Bytes())))
	miner.Logger.Info("Nonce : ", zap.Uint64("Nonce", blk.Nonce))
	miner.Logger.Info("Header Hash : ", zap.String("Hash", hex.EncodeToString(h.Bytes())))
	miner.Logger.Info("Header Height :", zap.Uint64("Height", blk.GetHeight()))
	miner.Logger.Info("Block Work :", zap.String("Block Work", blk.Target.Big().String()))
	miner.Logger.Info("Timer Finished : ", zap.String("Time Finished", tn.String()), zap.String("Elapsed", tn.Sub(t0).String()))

	return blk
}

func SolveBlock(blk *types.Block) (*types.Block, error) {

	h, _ := blk.Hash()
	blk.ChainWork = crypto.SetBig(new(big.Int).Add(blk.ChainWork.Big(), pow.AmountOfWork(blk.Target.Big())))

	for !pow.CheckWork(h, blk.Target.Big()) {
		blk.Nonce++

		h, _ = blk.Hash()
	}
	if !pow.CheckWork(h, blk.Target.Big()) {
		return blk, errors.New("failed to do enough work")
	}

	return blk, nil
}
