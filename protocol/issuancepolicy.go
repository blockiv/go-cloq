package protocol

// This file contains constant related to cloq's issuance policy.
// Cloq has a constant deflation rate at each epoch for about 40 years.
// With a steady tail emission of 2*Units per block.

const (
	// UNIT is the unitary denomination of a 1 CLOQ, represented to 9 decimal points.
	// 1 CLOQ = 1,000,000,000 cloqies (1 Billion)
	UNIT uint64 = 1e9
	// MaxUnits is the max number of coins in the system.
	MaxUnits = 23e6 * UNIT
	// CoinbaseMaturity is the number of blocks before a newly created coin is available for spending (250 blocks)
	CoinbaseMaturity uint64 = 256
	// BaseReward is the initial block reward
	BaseReward uint64 = 52 * 10e8
	// DecreaseEpoch is the epoch length after which the base reward is decreased
	DecreaseEpoch uint64 = 100000
	// DeflationRate is the rate at which we cut the Base reward each epoch
	DeflationRate uint64 = 6 * 10e6
	// TailReward represents the reward rate at the end of mining
	TailReward uint64 = 1e9
	// ColdPhaseHeight is when the tail reward activates
	ColdPhaseHeight = 7000001
)

// GetBlockIssuance calculates the expected issued CLOQs at a certain height.
func GetBlockIssuance(height uint64) uint64 {

	// The issuance rate decreases every 100k blocks by DeflationRate.
	// At Height : 7_000_000 the tailreward activates and issuance becomes constant.
	if height < ColdPhaseHeight {
		return BaseReward - (DeflationRate * (height / DecreaseEpoch))
	}
	return TailReward
}
