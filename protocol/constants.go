// Package consensus provides package wide constants and other network fixed parameters.
package protocol

import (
	"math/big"
	"time"

	"gitlab.com/cloq/go-cloq/core/crypto"
)

// The current values are network rules and should be homogeneous across all implementations.
var (
	// MaxUncles allowed to be referenced in a single header
	MaxUncles = 2
	// AllowedFutureTimestamp in block timestamps
	AllowedFutureTimestamp = 10 * time.Minute

	// MaxVMPushData is a threshold on data pushed to the VM stack.
	MaxVMPushData = 256
	// MaxBlockWeight is the maximum allowed size of a serialized block (4 MB).
	MaxBlockWeight = 4000000
	// MaxTxWeight is the maximum allowed size of a serialized transaction (1 MB).
	MaxTxWeight = 1000000
	// MaxPathwayThreshold defines an upper bound on the number of possible inputs and outputs in a transaction.
	// This serves both the clients and the network, they pay a lower a fee and put out less bloat.
	MaxPathwayThreshold = 65535
	// MaxSoftReorg is the how far away we are allowed to re-org before we panic and throw an error.
	MaxSoftReorg = CoinbaseMaturity - 1

	// Coinbase Tx Validation Rules

	// MinCoinbaseValidator is the minimum number of bytes in a coinbase validator
	// this is required to prevent duplicate coinbase transactions.
	MinCoinbaseValidator = 8
	// MaxCoinbaseValidator is the maximum number of bytes in a coinbase validator
	MaxCoinbaseValidator = 128
	// CoinbaseInput is the empty hash
	CoinbaseInput = crypto.EmptyHash

	// StartDiff is the initial difficulty level set at 1
	StartDiff = big.NewInt(1)

	// WhirlpoolPoWLimit is the highest target in the network for Whirlpool
	WhirlpoolPoWLimit, _ = new(big.Int).SetString("0x7fffffffffffffffffffffffffffffffffffffffffffffffffffffffff", 0)
	// RegressionPoWLimit is the target used for regression testing
	RegressionPoWLimit, _ = new(big.Int).SetString("0x7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff", 0)

	// MaxAmountOfWork is essentially the number of hash computations you can do.
	MaxAmountOfWork = new(big.Int).Lsh(big.NewInt(1), 256)
)
