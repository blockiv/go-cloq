package protocol

var (
	// TagInput for computing the tagged hash of an input.
	TagInput = []byte{'i', 'n', 'p', 'u', 't'}
	// TagOutput for computing the tagged hash of an output
	TagOutput = []byte{'o', 'u', 't', 'p', 'u', 't'}
	// TagContract for computing the tagged hash of a contract.
	TagContract = []byte{'c', 'o', 'n', 't', 'r', 'a', 'c', 't'}
	// TagTXID for computing the txid i.e H(tx.Commitment)
	TagTXID = []byte{'t', 'x', 'i', 'd'}
	// TagWTXID for computing the wtxid i.e H(wtxid||txid||H('wit')witness))
	TagWTXID = []byte{'w', 'i', 't', 'i', 'd'}
	// TagOpenContract for computing the tagged hash of an open contract
	TagOpenContract = []byte{'o', 'p', 'e', 'n', 'c', 'o', 'n', 't', 'r', 'a', 'c', 't'}
	// TagHeader for computing blockheader hash
	TagHeader = []byte{'b', 'l', 'k', 'h', 'd', 'r'}
	// TagWitness is for computing witness tags for WTXID
	TagWitness = []byte{'w', 'i', 't'}
	// TagScript is for computing Script hashes.
	TagScript = []byte{'q', 's', 'c', 'r', 'i', 'p', 't'}
)
